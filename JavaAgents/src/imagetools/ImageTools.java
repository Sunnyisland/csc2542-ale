/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package imagetools;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import javax.imageio.ImageIO;

/** Tools for drawing on Atari images.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ImageTools {
    public static void drawCross(BufferedImage img, int x, int y, Color c) {
        int rgb = c.getRGB();

        int w = img.getWidth();
        int h = img.getHeight();

        if (x > 0 && x < w && y > 0 && y < h) {
            img.setRGB(x, y, rgb);
        
            if (x > 0) img.setRGB(x-1, y, rgb);
            if (y > 0) img.setRGB(x, y-1, rgb);
            if (x < w - 1) img.setRGB(x+1, y, rgb);
            if (y < h - 1) img.setRGB(x, y+1, rgb);
        }
    }

    public static void overlayPixel(BufferedImage img, int x, int y, int gridColor) {
                        int imageColor = img.getRGB(x, y);
        double alpha = 1.0 / 2;

        // Interpolate the two colors, channel by channel
        int r = (int) (alpha * (gridColor & 0xFF0000)
                + (1.0 - alpha) * (imageColor & 0xFF0000)) & 0xFF0000;
        int g = (int) (alpha * (gridColor & 0x00FF00)
                + (1.0 - alpha) * (imageColor & 0x00FF00)) & 0x00FF00;
        int b = (int) (alpha * (gridColor & 0x0000FF)
                + (1.0 - alpha) * (imageColor & 0x0000FF)) & 0x0000FF;

        img.setRGB(x, y, r | g | b);
    }

    public static void drawGrid(BufferedImage img, int tileWidth, int tileHeight, int gridColor) {
        for (int cx = 0; cx < img.getWidth(); cx += tileWidth)
            for (int cy = 0; cy < img.getHeight(); cy += tileHeight) {
                // Draw top and left lines
                for (int x = 0; x < tileWidth; x++)
                    ImageTools.overlayPixel(img, x + cx, cy, gridColor);
                for (int y = 0; y < tileHeight; y++)
                    ImageTools.overlayPixel(img, cx, cy + y, gridColor);
            }
    }

    public static void drawGrid(BufferedImage img, int bx, int by, int tileWidth, int tileHeight, int gridColor) {
        for (int cx = bx; cx < img.getWidth()+bx; cx += tileWidth)
            for (int cy = by; cy < img.getHeight()+by; cy += tileHeight) {
                // Draw top and left lines
                for (int x = 0; x < tileWidth; x++) {
                    int xx = x + cx;

                    if (xx >= 0 && xx < img.getWidth() && cy >= 0 && cy < img.getHeight())
                        ImageTools.overlayPixel(img, x + cx, cy, gridColor);
                }
                for (int y = 0; y < tileHeight; y++) {
                    int yy = y + cy;

                    if (cx >= 0 && cx < img.getWidth() && yy >= 0 && yy < img.getHeight())
                        ImageTools.overlayPixel(img, cx, cy + y, gridColor);
                }
            }
    }

    private static int indexDigits = 6;

    public static void saveImage(BufferedImage img, String basename, int index) {
        // Create a formatter to generate 6-digit indices
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setMinimumIntegerDigits(indexDigits);
        formatter.setGroupingUsed(false);

        String indexString = formatter.format(index);

        String filename = basename + indexString + ".png";

        saveImage(img, filename);
    }

    public static void saveImage(BufferedImage img, String filename) {
        try {
            ImageIO.write(img, "png", new File(filename));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
