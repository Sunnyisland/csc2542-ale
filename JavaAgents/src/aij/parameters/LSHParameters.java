/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.parameters;

import nips11.parameters.*;
import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class LSHParameters {
    public static final String PARAM_NUM_RANDOM_VECTORS = "lsh-num-random-vectors";
    public static final String PARAM_RANDOM_VECTOR_SIZE = "lsh-random-vector-size";
    public static final String PARAM_HASH_TABLE_SIZE = "lsh-hash-table-size";
    public static final String PARAM_RANDOM_SEED = "lsh-random-seed";

    public static int numRandomVectors;
    public static int randomVectorSize;
    public static int hashTableSize;
    public static int randomSeed;
    
    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_NUM_RANDOM_VECTORS)) return;

        params.addIntegerParam(PARAM_NUM_RANDOM_VECTORS, 10);
        params.addIntegerParam(PARAM_RANDOM_VECTOR_SIZE, 1000);
        params.addIntegerParam(PARAM_HASH_TABLE_SIZE, 1001);
        params.addIntegerParam(PARAM_RANDOM_SEED, 0);
    }

    public static void parseParameters(ParameterHolder params) {
        numRandomVectors = params.getIntegerParam(PARAM_NUM_RANDOM_VECTORS);
        randomVectorSize = params.getIntegerParam(PARAM_RANDOM_VECTOR_SIZE);
        hashTableSize = params.getIntegerParam(PARAM_HASH_TABLE_SIZE);
        randomSeed = params.getIntegerParam(PARAM_RANDOM_SEED);
    }
}
