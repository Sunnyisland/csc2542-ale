/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents;

import atariagents.io.Actions;
import atariagents.screen.ScreenMatrix;
import java.io.File;
import marcgb.tools.config.Parameters;
import nips11.agents.AbstractAgent;
import nips11.parameters.NIPSParameters;
import nips11.parameters.RLAgentParameters;
import nips11.rl.EnvironmentProvider;
import rlVizLib.general.ParameterHolder;

/** A RL environment + agent, all wrapped up as one.
 *
 * @author marc
 */
public class SingleActionAgent extends AbstractAgent {
    public static final int NUM_RIGHT_DISPLAY_MODES = 0;
    
    protected final int numActions = Actions.numPlayerActions;
    
    protected EnvironmentProvider environment;
    
    protected boolean requestReset;
    protected boolean ignoreNextFrame;
    
    protected int frameNumber;
    protected int episodeNumber;
    protected int maxNumEpisodes;
    protected int episodeStartFrame;

    protected double rewardThisEpisode = 0;

    protected int currentAction;
    protected double epsilon;
    
    protected int maxFramesWithoutReward = 36000;
    protected int numFramesWithoutReward = 0;
    
    protected double minScore = Double.POSITIVE_INFINITY;
    protected double maxScore = Double.NEGATIVE_INFINITY;

    protected boolean firstStep;
    
    public SingleActionAgent(ParameterHolder params) {
        super(params);

        NIPSParameters.parseParameters(params);
        RLAgentParameters.parseParameters(params);
        
        currentAction = NIPSParameters.singleAction;
        epsilon = RLAgentParameters.epsilon;
        maxNumEpisodes = NIPSParameters.maxNumEpisodes;
        
        environment = new EnvironmentProvider(params);
        episodeNumber = 1;
        frameNumber = 0;
        requestReset = true;
        firstStep = true;
    }

    public static void addParameters(ParameterHolder params) {
        NIPSParameters.addParameters(params);
        RLAgentParameters.addParameters(params);
        EnvironmentProvider.addParameters(params);
    }

    @Override
    public boolean shouldTerminate() {
        // Terminate when we are told to do so by the outside world
        boolean shouldTerminate = (comm.wantsTerminate() || episodeNumber > maxNumEpisodes);

        if (shouldTerminate)
            System.err.println ("SCORE "+minScore+" "+maxScore);

        return shouldTerminate;
    }

    @Override
    public long getPauseLength() {
        return 0;
    }

    @Override
    public int selectAction() {
        if (Math.random() < epsilon)
            return (int)(Math.random() * Actions.numPlayerActions);
        else
            return currentAction;
    }

    @Override
    public void observeImage(ScreenMatrix image) {
        if (firstStep) {
            firstStep = false;
            newEpisode();
        }
        
        // To ignore the frame right after a reset
        if (ignoreNextFrame) {
            ignoreNextFrame = false;
            return;
        }

        frameNumber++;
        
        environment.observe(image, comm.getRAM(), comm.getRLData());
        boolean terminal = environment.isTerminal();
        double reward = environment.getReward();

        environment.newStep();

        updateScoreBounds();
        rewardThisEpisode += reward;

        if (reward != 0) numFramesWithoutReward = 0;
        else numFramesWithoutReward++;
        
        // We want to terminate if this action leads nowhere, e.g. noop in seaquest
        // The assumption is that we cannot gain infinite reward by repeating the
        //  same action (we will then die at some point)
        if (terminal || (numFramesWithoutReward >= maxFramesWithoutReward)) {
            endEpisode();
        }
    }

    protected void updateScoreBounds() {
        double score = environment.getScore();
        if (score < minScore) minScore = score;
        if (score > maxScore) maxScore = score;
    }

    protected void newEpisode() {
        environment.reset();
    }

    protected void endEpisode() {
        System.err.println("EPISODE " + episodeNumber + " " + rewardThisEpisode);
        rewardThisEpisode = 0;
        episodeNumber++;

        requestReset = true;
        firstStep = true;
        frameNumber = 0;
    }

    @Override
    public int specialAction() {
        if (requestReset) {
            requestReset = false;
            ignoreNextFrame = true;
            return Actions.map("system_reset");
        }
        else
            return -1;
    }

    public boolean wantsRamData() {
        return false;
    }
    
    public boolean wantsRLData() {
        return true;
    }

    public boolean wantsStandardSize() {
        return true;
    }

    @Override
    protected void keyboardEvents() {
        super.keyboardEvents();
    }

    public static void main(String[] args) {
        ParameterHolder params = new ParameterHolder();
        SingleActionAgent.addParameters(params);

        // Parse arguments first - somewhat awkward
        int argIndex = 0;
        String configFilename = null;

        int action = -1;
        
        while (argIndex < args.length) {
            String a = args[argIndex];

            if (!a.startsWith("-"))
                configFilename = a;
            if (a.startsWith("--action")) {
                String[] tokens = a.split("=");
                action = Integer.parseInt(tokens[1]);
                argIndex++;
            }
            argIndex++;
        }

        if (configFilename != null) {
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }

        // Apply single action choice
        if (action != -1)
            params.setIntegerParam(NIPSParameters.PARAM_SINGLE_ACTION, action);

        SingleActionAgent agent = new SingleActionAgent(params);

        try {
            agent.run();
        }
        catch (Exception e) {
            // make sure the UI dies, or we will wait forever
            agent.ui.die();
            agent.comm.close();
            throw new RuntimeException(e);
        }
    }
}
