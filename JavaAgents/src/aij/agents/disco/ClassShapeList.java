/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/** Encapsulates a set of class shapes.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ClassShapeList {
    protected ArrayList<ClassShape> shapes;
    protected int numClasses;
    
    public ClassShapeList(String filename) throws IOException {
        load(filename);
    }

    public ClassShapeList() {
        shapes = new ArrayList<ClassShape>();
    }

    public int numClasses() {
        return numClasses;
    }

    public int numShapes() {
        return shapes.size();
    }
    
    public ArrayList<ClassShape> getShapes() {
        return shapes;
    }

    public void addShape(ClassShape shape) {
        shapes.add(shape);
    }

    public void save(String filename) {
        // @todo
    }

    public final void load(String filename) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

        String[] tokens = in.readLine().split(",");
        numClasses = Integer.parseInt(tokens[0]);
        int numShapes = Integer.parseInt(tokens[1]);

        shapes = new ArrayList<ClassShape>();
        
        String line;
        while ((line = in.readLine()) != null) {
            ClassShape shape = new ClassShape(line);
            shapes.add(shape);
        }
    }

}
