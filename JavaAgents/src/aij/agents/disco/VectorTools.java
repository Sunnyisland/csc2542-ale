/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package aij.agents.disco;

import atariagents.screen.ScreenMatrix;
import java.util.ArrayList;

/** Ported from vector_matrix_tools.{h,cpp}
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class VectorTools {

    public static int get_l2_distance(ScreenMatrix pm_matrix_a,
            ScreenMatrix pm_matrix_b) {
        int m = pm_matrix_a.height;
        int n = pm_matrix_b.width;
        assert (m == pm_matrix_b.height);
        assert (n == pm_matrix_b.width);
        int total_dist = 0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // MGB inverted
                int diff = pm_matrix_a.matrix[j][i] - pm_matrix_b.matrix[j][i];
                total_dist += diff * diff;
            }
        }
        return total_dist;
    }

    public static void simple_where(ScreenMatrix pm_matrix, int target,
            ArrayList<Integer> pv_row_inds, ArrayList<Integer> pv_col_inds) {

        pv_row_inds.clear();
        pv_col_inds.clear();
        int height = pm_matrix.height;
        if (height == 0) {
            return; // empty matrix, nothing to do
        }
        int width = pm_matrix.width;
        if (width == 0) {
            return; // empty matrix, nothing to do
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                // MGB inverted
                if (pm_matrix.matrix[j][i] == target) {
                    pv_row_inds.add(i);
                    pv_col_inds.add(j);
                }
            }
        }
    }

    public static int vect_min(ArrayList<Integer> pv_vector) {
        assert (pv_vector.size() > 0);
        int min_val = pv_vector.get(0);
        int min_index = 0;

        for (int i = 1; i < pv_vector.size(); i++) {
            int val = pv_vector.get(i);
            if (val < min_val) {
                min_val = val;
                min_index = i;
            }
        }

        return min_index;
    }

    public static int vect_max(ArrayList<Integer> pv_vector) {
        assert (pv_vector.size() > 0);
        int max_val = pv_vector.get(0);
        int max_index = 0;

        for (int i = 1; i < pv_vector.size(); i++) {
            int val = pv_vector.get(i);
            if (val > max_val) {
                max_val = val;
                max_index = i;
            }
        }

        return max_index;
    }
}
