/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

import java.util.Comparator;

/** Ported from blob_object.{h,cpp}.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class BlobObjectComparator implements Comparator {

    public int compare(Object t1, Object t2) {
        RegionObject obj1 = (RegionObject)t1;
        RegionObject obj2 = (RegionObject)t2;

        int obj1_area = obj1.i_width * obj1.i_height;
        int obj2_area = obj2.i_width * obj2.i_height;

        // MGB: although obj1_area - obj2_area would be better; it is not the
        //  original code's functionality
        if (obj1_area > obj2_area) return 1;
        else return -1;
    }

}
