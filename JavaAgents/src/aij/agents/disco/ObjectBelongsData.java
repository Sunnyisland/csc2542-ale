/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

/** Helper class to replace references in ClassDiscovery.java.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ObjectBelongsData {
    public boolean flip_horz;
    public boolean flip_vert;
    public boolean diff_size;

    public ObjectBelongsData(boolean flip_horz, boolean flip_vert, boolean diff_size) {
        this.flip_horz = flip_horz;
        this.flip_vert = flip_vert;
        this.diff_size = diff_size;
    }
}
