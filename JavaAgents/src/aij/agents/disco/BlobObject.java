/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

/** Ported from blob_object.{h,cpp}
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class BlobObject {
        public int i_center_x, i_center_y;
        public int i_velocity_x, i_velocity_y;
        public int i_instance_of_class;

    public BlobObject( int _center_x, int _center_y) {
        this(_center_x, _center_y, 0, 0, 0);
    }

    public BlobObject( int _center_x, int _center_y, int _velocity_x,
                        int _velocity_y, int _instance_of_class) {
    i_center_x = _center_x;
    i_center_y = _center_y;
    i_velocity_x = _velocity_x;
    i_velocity_y = _velocity_y;
    i_instance_of_class = _instance_of_class;
    }

    public int calc_distance_max_xy( BlobObject other_obj) {
    return Math.max( Math.abs(i_center_x - other_obj.i_center_x),
                Math.abs(i_center_y - other_obj.i_center_y) );
    }

}
