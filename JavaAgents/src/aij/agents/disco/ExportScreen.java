/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

import atariagents.screen.BWPalette;
import atariagents.screen.CustomALEPalette;
import atariagents.screen.NTSCPalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/** A class that ports some, but not all, functionality from export_screen.{h,cpp}.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ExportScreen {
    protected static ScreenConverter ntscConverter;
    protected static ScreenConverter customConverter;
    protected static ScreenConverter bwConverter;

    public static void exportNTSC(ScreenMatrix screen, String filename) {
        if (ntscConverter == null) ntscConverter = new ScreenConverter(new NTSCPalette());
        export(screen, filename, ntscConverter);
    }

    public static void exportCustom(ScreenMatrix screen, String filename) {
        if (customConverter == null) customConverter = new ScreenConverter(new CustomALEPalette());
        export(screen, filename, customConverter);
    }

    public static void exportBW(ScreenMatrix screen, String filename) {
        if (bwConverter == null) bwConverter = new ScreenConverter(new BWPalette());
        export(screen, filename, bwConverter);
    }

    public static void export(ScreenMatrix screen, String filename, ScreenConverter converter) {
        try {
            ImageIO.write(converter.convert(screen), "png", new File(filename));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
