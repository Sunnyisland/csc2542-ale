/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package aij.agents.disco;

import atariagents.screen.ScreenMatrix;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import nips11.parameters.BASSParameters;
import nips11.parameters.DISCOParameters;
import nips11.parameters.NIPSParameters;
import rlVizLib.general.ParameterHolder;

/** Ported from class_discovery.{h,cpp}. Minimal modifications were made.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ClassDiscovery {

    protected ScreenMatrix pm_background_matrix;
    protected RegionManager p_region_manager;
    protected ArrayList<RegionObject> pv_curr_screen_objects;
    protected ArrayList<RegionObject> pv_prev_screen_objects;
    protected ArrayList<BlobClass> pv_discovered_classes;
    protected int i_screen_width;
    protected int i_screen_height;
    protected int i_num_screens;
    protected int i_max_num_screens;
    protected float f_max_perc_difference;
    protected int i_max_obj_velocity;
    protected float f_max_shape_area_dif;
    protected float f_min_on_frame_ratio;
    protected int i_min_boundary_length;
    protected int i_max_num_classes;
    protected boolean b_plot_pre_filter_classes;
    protected boolean b_plot_post_filter_classes;

    protected String shapesFilename;
    protected boolean terminated;

    protected boolean noMultiplePasses = false;

    public ClassDiscovery(ParameterHolder params) {
        // @todo set screen_width/height
        DISCOParameters.parseParameters(params);
        BASSParameters.parseParameters(params);
        NIPSParameters.parseParameters(params);
        
        f_max_perc_difference = (float)DISCOParameters.maxPercDifference;
        i_max_obj_velocity = DISCOParameters.maxObjVelocity;
        f_max_shape_area_dif = (float)DISCOParameters.maxShapeAreaDif;
        i_max_num_screens = NIPSParameters.maximumSampleCount;
        f_min_on_frame_ratio = (float)DISCOParameters.minOnFrameRatio;
        i_min_boundary_length = DISCOParameters.minBoundaryLength;
        i_max_num_classes = DISCOParameters.maxNumClasses;
        b_plot_pre_filter_classes = DISCOParameters.plotPreFilterClasses;
        b_plot_post_filter_classes = DISCOParameters.plotPostFilterClasses;

        shapesFilename = DISCOParameters.shapesFilename;
        
        i_num_screens = 0;

        String backgroundFilename = BASSParameters.backgroundFilename;
        loadBackground(backgroundFilename);

        p_region_manager = new RegionManager(params, pm_background_matrix);
        pv_curr_screen_objects = null;
        pv_prev_screen_objects = null;

        pv_discovered_classes = new ArrayList<BlobClass>();

        terminated = false;
    }

    public static void addParameters(ParameterHolder params) {
        DISCOParameters.addParameters(params);
        NIPSParameters.addParameters(params);
        BASSParameters.addParameters(params);
    }

    protected final void loadBackground(String filename) {
        try {
            pm_background_matrix = new ScreenMatrix(filename);
        }
        catch (IOException e) {
            System.err.println ("Cannot load background.");
            throw new RuntimeException(e);
        }
    }

    public ScreenMatrix getBackground() {
        return pm_background_matrix;
    }
    
    public void get_new_screen(ScreenMatrix screen_matrix, int frame_number) {
        // MGB Yavar was ignoring first 1000 frames
        if (i_num_screens > i_max_num_screens || pv_discovered_classes.size() > 200) {
            return; // we are done with class discovery
        }
        pv_prev_screen_objects = pv_curr_screen_objects;
        pv_curr_screen_objects = p_region_manager.extract_objects_from_new_screen(screen_matrix, frame_number);

        for (RegionObject obj : pv_curr_screen_objects) {
            // 1- See if this object belongs to an already discovered class
            boolean obj_in_class_list = false;
            for (int c = 0; c < pv_discovered_classes.size(); c++) {
                BlobClass disc_class = pv_discovered_classes.get(c);
                ObjectBelongsData obd = object_belongs_to_class(disc_class, obj);
                if (obd != null) {
                    obj_in_class_list = true;
                    disc_class.i_num_frames_on_scr++;
                    disc_class.update_screen_boundaries(obj);
                    obj.i_instance_of_class = c;
                    if (obd.flip_horz || obd.flip_vert || obd.diff_size) {
                        // A flipped version of the object or a
                        //version with diferent size also belongs to
                        //class. Add the non-flipped/resized verison
                        disc_class.add_object(obj, frame_number,
                                "Flipped and/or Different Size");
                    } else {
                        // The exact object belongs to this class
                        // add one to its frame counter
                        disc_class.increase_obj_onscreen_count(obj);
                    }
                    break; // we are done with the inner loop
                }
            }
            if (obj_in_class_list) {
                continue;  // We have already assigned a class to this object
            }

            // 2- See if there is a 'similar' object 'close' to this object
            //    in the previous frame
            RegionObject prev_obj = null;
            if (pv_prev_screen_objects != null) {
                prev_obj = obj.get_closest_obj_in_list(
                        pv_prev_screen_objects, pv_prev_screen_objects.size(),
                        f_max_shape_area_dif, f_max_perc_difference,
                        i_max_obj_velocity);
            }
            if (prev_obj != null) {
                // There was a similar object in the previous frame
                // Assign this object to the same class
                obj.i_instance_of_class = prev_obj.i_instance_of_class;
                BlobClass target_class = pv_discovered_classes.get(prev_obj.i_instance_of_class);
                String disc_str = "Sim-Obj on Previous Frame";
                target_class.add_object(obj, frame_number, disc_str);
                target_class.i_num_frames_on_scr++;
                continue;
            }

            // 3- This is a brand new object, belonging to a brand new class
            BlobClass new_class = new BlobClass();
            new_class.i_discovered_on_frame = frame_number;
            new_class.i_num_frames_on_scr = 1;
            String disc_str = "Brand New";
            new_class.add_object(obj, frame_number, disc_str);
            obj.i_instance_of_class = pv_discovered_classes.size();
            pv_discovered_classes.add(new_class);
            if (pv_discovered_classes.size() > 200) {
                System.err.println("Warning: number of discovered classes exceeds 200."
                        + "The remaining classes wil be ignored");
            }
        }

        if (i_num_screens == i_max_num_screens || pv_discovered_classes.size() > 200)
            extract_classes();

        i_num_screens++;
    }

    protected void extract_classes() {
        if (b_plot_pre_filter_classes) {
            System.err.println("Plotting the (pre-filter) discovered classes...");
            plot_classes(false, false);
            System.err.println(" done.");
        }
        System.err.println("Filtering classes...");
        filter_classes();
        System.err.println(" done.");
        System.err.println("Plotting the (post-filter) discovered classes...");
        plot_classes(true, false);
        System.err.println(" done.");
        if (pv_discovered_classes.size() > i_max_num_classes) {
            System.err.println("Merging together classes...");
            merge_classes();
            System.err.println(" done.");
            if (b_plot_post_filter_classes) {
                System.err.println("Plotting the (post-merge) discovered classes...");
                plot_classes(true, true);
                System.err.println(" done.");
            }
        }
        // MGB
        System.err.println("Expanding filled class shapes...");
        expand_class_shapes();
        System.err.println("Exporting discovered classes...");
        export_class_shapes();
        System.err.println(" done.");

        if (noMultiplePasses)
            System.exit(-1);
        
        // MGB Throw away the current objects; their class instances are now invalid
        pv_curr_screen_objects = null;
    }

    public void terminate() {
        extract_classes();
        terminated = true;
    }
    
    public boolean is_class_discovery_complete() {
        return (terminated || i_num_screens >= i_max_num_screens);
    }

    /* *********************************************************************
    filters out 'noise classes' (i.e. classes that do not appear on the
    screen often enough or do not move in a large enough boundary)
     ******************************************************************** */
    public void filter_classes() {
        ArrayList<BlobClass> pv_filtered_classes = new ArrayList<BlobClass>();
        int min_boundary_len_sq = (i_min_boundary_length * i_min_boundary_length);
        int filtered_classes_counter = 0;

        for (BlobClass curr_class : pv_discovered_classes) {
            float on_frame_ratio = (float) curr_class.i_num_frames_on_scr
                    / (float) i_num_screens;
            boolean keep_class = true;
            if (on_frame_ratio < f_min_on_frame_ratio) {
                keep_class = false;
            }
            int delta_x = curr_class.i_screen_pos_x_max
                    - curr_class.i_screen_pos_x_min;
            int delta_y = curr_class.i_screen_pos_x_max
                    - curr_class.i_screen_pos_x_min;
            if (delta_x < i_min_boundary_length
                    && delta_y < i_min_boundary_length
                    && (delta_x * delta_y) < min_boundary_len_sq) {
                keep_class = false;
            }
            if (keep_class) {
                pv_filtered_classes.add(curr_class);
                curr_class.update_class_number(filtered_classes_counter);
                filtered_classes_counter++;
            } else {
                // MGB Nothing to do in Java - garbage collector does it for us!
            }
        }
        pv_discovered_classes = pv_filtered_classes;
    }

    /* *********************************************************************
    merges similar classes (used when we  have too many classes )
    This will continue, until number classes is less than i_max_num_classes
     ******************************************************************** */
    public void merge_classes() {
        int num_merges = 0;
        while (pv_discovered_classes.size() > i_max_num_classes) {
            // 1- Find the two closest classes and merge them
            float closest_dist = -1;
            int merge_ind_a = -1;
            int merge_ind_b = -1;
            for (int i = 0; i < pv_discovered_classes.size(); i++) {
                for (int j = i + 1; j < pv_discovered_classes.size(); j++) {
                    DistanceData dd = pv_discovered_classes.get(i).get_min_distance(pv_discovered_classes.get(j),
                            f_max_shape_area_dif);
                    if (dd == null) {
                        continue;
                    }
                    if (closest_dist < 0 || dd.perc_distance < closest_dist) {
                        closest_dist = dd.perc_distance;
                        merge_ind_a = i;
                        merge_ind_b = j;
                    }
                }
            }
            if (merge_ind_a == -1 || merge_ind_b == -1) {
                System.err.println ("Unable to merge classes down to " + i_max_num_classes +
                        ". The remaining classes are not comparable in distance");
                break;
            }
            // 2- Merge them!

            pv_discovered_classes.get(merge_ind_b).update_class_number(merge_ind_a);
            pv_discovered_classes.get(merge_ind_a).merge_with(pv_discovered_classes.get(merge_ind_b));
            
            pv_discovered_classes.remove(merge_ind_b);

            for (int i = merge_ind_b; i < pv_discovered_classes.size(); i++) {
                pv_discovered_classes.get(i).update_class_number(i);
            }
            num_merges++;
        }
        System.err.println ("Merged "+num_merges+" classes.");
    }

/* *********************************************************************
	Plots the shape_matrix's of each class in a subdirectory
	post_filter should be true if this is called after filtering classes
	post_merger should be true if this is called after merging classes
 ******************************************************************** */
    public void plot_classes(boolean post_filter, boolean post_merge) {
	// Obtain the basename for this shapes file by removing its extension
        String[] tokens = shapesFilename.split("\\.");
        String shapesBasename = tokens[0];

        for (int i = 1; i < tokens.length - 1; i++)
            shapesBasename += tokens[i];

        String base_dir = shapesBasename+"__";
	if (post_merge) {
		base_dir += "post_merge";
	} else if (post_filter) {
		base_dir += "post_filter";
	} else {
		base_dir += "pre_filter";
	}

        File base_dir_fp = new File(base_dir);
        if (!base_dir_fp.isDirectory())
            base_dir_fp.mkdirs();
        else {
            // Clear directory if it exists
            base_dir_fp.delete();
            base_dir_fp.mkdirs();
        }

        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setGroupingUsed(false);

        try {
	for (int c = 0; c < pv_discovered_classes.size(); c++) {
		BlobClass curr_class = pv_discovered_classes.get(c);
                formatter.setMinimumIntegerDigits(3);
                String indexString = formatter.format(c);

		String sub_dir = base_dir + "/" + "class_" + indexString;
                File sub_dir_fp = new File(sub_dir);
		if(!sub_dir_fp.isDirectory())
                    sub_dir_fp.mkdirs();

                // Write the class info
		String info_file = sub_dir + "/class_info.txt";
                PrintStream file = new PrintStream(new FileOutputStream(info_file));
		file.println(curr_class.get_info_text(i_num_screens));
                file.close();
                
                // Plot and write info for each RegionObject in this class
		for (int j = 0; j < curr_class.pv_reg_objects.size(); j++) {
			RegionObject curr_obj = curr_class.pv_reg_objects.get(j);
                        formatter.setMinimumIntegerDigits(4);
                        String classIndexString = formatter.format(j);

                        info_file = sub_dir + "/object_info_" + classIndexString + ".txt";

                        file = new PrintStream(new FileOutputStream(info_file));
			file.println(curr_obj.get_info_text());
                        file.close();

                        String plot_file = sub_dir + "/object_info_" + classIndexString + ".png";
                        curr_obj.plot(plot_file);
		}
	}
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
}

    /** MGB small shapes that are only composed of foreground pixels are dangerous.
     *    We want to enforce that they are surrounded by background pixels.
     */
    public void expand_class_shapes() {
        for (BlobClass curr_class : pv_discovered_classes) {
            for (RegionObject r : curr_class.pv_reg_objects) {
                boolean isFullShape = true;

                ScreenMatrix shape = r.pm_shape_matrix;
                for (int x = 0; x < shape.width; x++)
                    for (int y = 0; y < shape.height; y++)
                        if (shape.matrix[x][y] == 0) {
                            isFullShape = false;
                            break;
                        }

                // Expand this shape to contain a border
                if (isFullShape) {
                    System.err.println ("Expanding shape for class "+r.i_instance_of_class+"...");
                    int newWidth = shape.width+2;
                    int newHeight = shape.height+2;

                    ScreenMatrix newShape = new ScreenMatrix(newWidth, newHeight);
                    // Copy the shape with an empty border around it
                    for (int x = 0; x < shape.width; x++)
                        for (int y = 0; y < shape.height; y++)
                            newShape.matrix[x+1][y+1] = shape.matrix[x][y];

                    r.pm_shape_matrix = newShape;
                    r.i_width = newWidth;
                    r.i_height = newHeight;
                }
            }
        }
    }

    /* *********************************************************************
    Sorts the region-objects of the discovered classes based
    on their size, and exports the results to a txt file, so that
    it can imported by ClassShape::import_shape_list
    File format is:
    number_of_classes,number_of_shapes\n
    shape_matrix_text_1 (see the comments on constructor) \n
    shape_matrix_text_2\n
    ...
    shape_matrix_text_n
     ******************************************************************** */
    public void export_class_shapes() {
        ArrayList<RegionObject> sorted_objects = new ArrayList<RegionObject>();
        for (BlobClass curr_class : pv_discovered_classes) {
            for (RegionObject r : curr_class.pv_reg_objects) {
                sorted_objects.add(r);
            }
        }

        // MGB @todo
        RegionObject[] sorted_objects_array = sorted_objects.toArray(new RegionObject[0]);

        Arrays.sort(sorted_objects_array, new BlobObjectComparator());
        
        int num_shapes = sorted_objects.size();
        int num_classes = pv_discovered_classes.size();

        try {
            PrintStream file = new PrintStream(new FileOutputStream(shapesFilename));
            file.println(num_classes + "," + num_shapes);
            // Arrays.sort sorts in ascending order; so we output them backwards,
            //  starting with the biggest shape
            for (int i = num_shapes - 1; i >= 0; i--) {
                file.println(sorted_objects_array[i].export_object_as_txt());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* *********************************************************************
    Check swhetehr the object (or one of its flipped or resized versions)
    belong to the class
     ******************************************************************** */
    public ObjectBelongsData object_belongs_to_class(BlobClass cls, RegionObject obj) {
        boolean[] booleanValues = new boolean[]{false, true};

        for (boolean do_flip_horz : booleanValues) {
            for (boolean do_flip_vert : booleanValues) {
                for (boolean diff_size : booleanValues) {
                    if (cls.belongs(obj, diff_size, do_flip_horz,
                            do_flip_vert, f_max_shape_area_dif)) {
                        return new ObjectBelongsData(do_flip_horz, do_flip_vert, diff_size);
                    }
                }
            }
        }

        return null;
    }
}
