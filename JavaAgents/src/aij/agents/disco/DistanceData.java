/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

/** Helper class to convert reference-based functions to Java.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class DistanceData {
    public int pixel_distance;
    public float perc_distance;

    public DistanceData(int pixel_distance, float perc_distance) {
        this.pixel_distance = pixel_distance;
        this.perc_distance = perc_distance;
    }
}
