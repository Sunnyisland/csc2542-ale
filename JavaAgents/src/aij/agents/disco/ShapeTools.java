/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

import atariagents.screen.ScreenMatrix;

/** Ported from shape_tools.{h,cpp}.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ShapeTools {
/* *****************************************************************************
	Flips a 2D shape matrix horizentaly or vertically
 **************************************************************************** */
    public static void flip_shape(ScreenMatrix orig_matrix, ScreenMatrix new_matrix,
				boolean do_horizontal, boolean do_vertical) {
	int m = orig_matrix.height;
	int n = orig_matrix.width;
        
	int row_start, row_end, row_update;
	int col_start, col_end, col_update;
	if (do_horizontal) {
		col_start = n - 1;
		col_end = 0;
		col_update = -1;
	} else {
		col_start = 0;
		col_end = n - 1;
		col_update = 1;
	}
	if (do_vertical) {
		row_start = m - 1;
		row_end = 0;
		row_update = -1;
	} else {
		row_start = 0;
		row_end = m - 1;
		row_update = 1;
	}

        // MGB - modified to fit my ScreenMatrix; actually slower than the
        //  original code, which moved pointers around
        int ox = col_start;
        for (int x = 0; x < n; x++) {
            int oy = row_start;
            for (int y = 0; y < m; y++) {
                new_matrix.matrix[x][y] = orig_matrix.matrix[ox][oy];
                oy += row_update;
            }

            ox += col_update;
        }
    }

    public static DistanceData calc_shape_distance(
            ScreenMatrix pm_shape_a, ScreenMatrix pm_shape_b,
            boolean allow_different_size, float max_shape_area_dif) {
	int a_height = pm_shape_a.height;
	int a_width  = pm_shape_a.width;
	int b_height = pm_shape_b.height;
	int b_width  = pm_shape_b.width;

        int pixel_distance;
        float perc_distance;
        
	int a_area = a_width * a_height;
	int b_area = b_width * b_height;
	if (a_width == b_width &&
		a_height == b_height) {
		// Objects have the same shape
		pixel_distance = VectorTools.get_l2_distance(pm_shape_a, pm_shape_b);
	} else {
		// Objects have different shapes
		if (!allow_different_size) {
			return null;
		}

		// When one shape is smaller than the other, we move the smaller
		// object inside the bigger object, and return the smallest distance
		// This only makes sense when the shapes area is within a small range
		if  (	( (float)a_area >  (max_shape_area_dif * b_area) ) ||
				( (float)b_area > (max_shape_area_dif * a_area) )	) {
			// size difference is too big
			return null;
		}

		ScreenMatrix bigger_shape, smaller_shape;
		int width_dif, height_dif, smaller_height, smaller_width;
		if (a_width <= b_width &&
			a_height <= b_height) {
			bigger_shape = pm_shape_b;
			smaller_shape = pm_shape_a;
			smaller_height = a_height;
			smaller_width = a_width;
			width_dif = b_width - a_width;
			height_dif = b_height - a_height;
		} else if (	a_width >= b_width &&
					a_height >= b_height) {
			bigger_shape = pm_shape_a;
			smaller_shape = pm_shape_b;
			smaller_height = b_height;
			smaller_width = b_width;
			width_dif = a_width - b_width;
			height_dif =  a_height - b_height;
		} else {
			// incompatible shapes
			return null;
		}

		assert (width_dif >= 0 &&  height_dif >= 0);
		// TODO: if deemed necessary, bring this back:
		// if shape(smaller_shape) < (3,3):
		//	# This method (hack) doesn't really make sense for tiny shapes
		//	return None

		int smallest_distance = -1;
		for (int i = 0; i <= height_dif; i++) {
			for (int j = 0; j <= width_dif; j++) {
				int l2_dist = 0;
				for (int y = 0; y < smaller_height; y++) {
					for (int x = 0; x < smaller_width; x++) {
                                            // MGB inverted
                                            int dif = bigger_shape.matrix[j+x][i+y] -
                                                    smaller_shape.matrix[x][y];
						l2_dist += dif * dif;
					}
				}
				if (smallest_distance == -1 || l2_dist < smallest_distance) {
					smallest_distance = l2_dist;
				}
			}
		}
		pixel_distance = smallest_distance;
	}
	int num_pixels = a_area + b_area;

	perc_distance = (float)pixel_distance / (float)num_pixels;
        DistanceData dd = new DistanceData(pixel_distance, perc_distance);
	return dd;
}

}
