/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

import atariagents.screen.ScreenMatrix;

/** Ported from class_shape.{h,cpp}
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ClassShape {
    public ScreenMatrix shape;
    public int width;
    public int height;
    public int firstOneX;
    public int firstOneY;
    public int instanceOfClass;

    public ClassShape(String text) {
        String[] lines = text.split("\\|");

        String[] infoTokens = lines[0].split(",");

        // The first |-separated token contains shape information
        instanceOfClass = Integer.parseInt(infoTokens[0]);
        width = Integer.parseInt(infoTokens[1]);
        height = Integer.parseInt(infoTokens[2]);

        shape = new ScreenMatrix(width, height);

        // The following tokens, one per row, contain row information
        for (int y = 0; y < height; y++) {
            String[] bitTokens = lines[y+1].split(",");
            for (int x = 0; x < width; x++)
                shape.matrix[x][y] = Integer.parseInt(bitTokens[x]);
        }

        // first top-leftmost non-zero bit
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                if (shape.matrix[x][y] == 1) {
                    firstOneX = x;
                    firstOneY = y;
                    break;
                }
    }
}
