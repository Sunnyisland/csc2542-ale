/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

/** Encapsulates a foreground point.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class FGPoint {
    public int x;
    public int y;

    public FGPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
