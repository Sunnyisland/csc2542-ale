/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package aij.agents.disco;

import java.util.ArrayList;

/**
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class BlobClass {

    protected ArrayList<RegionObject> pv_reg_objects;
    protected int i_num_frames_on_scr;
    protected int i_screen_pos_x_min;
    protected int i_screen_pos_x_max;
    protected int i_screen_pos_y_min;
    protected int i_screen_pos_y_max;
    protected int i_discovered_on_frame;

    public BlobClass() {
        pv_reg_objects = new ArrayList<RegionObject>();

        i_num_frames_on_scr = 0;
        i_screen_pos_x_min = -1;
        i_screen_pos_x_max = -1;
        i_screen_pos_y_min = -1;
        i_screen_pos_y_max = -1;
        i_discovered_on_frame = -1;
    }

    public void add_object(RegionObject reg_object, int frame_num,
            String discovery_txt) {
        RegionObject new_object = (RegionObject) reg_object.clone();
        new_object.i_disc_frame_num = frame_num;
        new_object.i_num_frames_on = 1;
        new_object.str_disc_text = discovery_txt;
        pv_reg_objects.add(new_object);
        update_screen_boundaries(reg_object);
    }

    public void update_screen_boundaries(RegionObject reg_object) {
        if (i_screen_pos_x_min == -1
                || reg_object.i_center_x < i_screen_pos_x_min) {
            i_screen_pos_x_min = reg_object.i_center_x;
        }
        if (i_screen_pos_x_max == -1
                || reg_object.i_center_x > i_screen_pos_x_max) {
            i_screen_pos_x_max = reg_object.i_center_x;
        }
        if (i_screen_pos_y_min == -1
                || reg_object.i_center_y < i_screen_pos_y_min) {
            i_screen_pos_y_min = reg_object.i_center_y;
        }
        if (i_screen_pos_y_max == -1
                || reg_object.i_center_y > i_screen_pos_y_max) {
            i_screen_pos_y_max = reg_object.i_center_y;
        }
    }

    /* *********************************************************************
     * Returns true if the given object belongs to this class.
     * When check_flipped_horizentally is true, we check if the flipped
     * version of the object belongs to this class. Same deal with
     *  check_flipped_vertically, and allow_different_size
     ******************************************************************** */
    public boolean belongs(RegionObject reg_object,
            boolean allow_different_size,
            boolean check_flipped_horizentally,
            boolean check_flipped_vertically,
            float max_shape_area_dif) {
        RegionObject flipped_obj = null;

        if (check_flipped_vertically || check_flipped_horizentally) {
            flipped_obj = (RegionObject) reg_object.clone();
            ShapeTools.flip_shape(reg_object.pm_shape_matrix, flipped_obj.pm_shape_matrix,
                    check_flipped_horizentally, check_flipped_vertically);
        }
        boolean do_belong = false;
        for (RegionObject curr_object : pv_reg_objects) {
            DistanceData dd;
            if (flipped_obj != null) {
                dd = curr_object.calc_distance(flipped_obj,
                        allow_different_size, max_shape_area_dif);
            } else {
                dd = curr_object.calc_distance(reg_object,
                        allow_different_size, max_shape_area_dif);
            }

            if (dd != null && dd.pixel_distance == 0) {
                do_belong = true;
                break;
            }
        }

        return do_belong;
    }

    /* *********************************************************************
     * Adds one to the i_num_frames_on of the corresponding RegionObject
     *  in our pv_rep_objects
     ******************************************************************** */
    public void increase_obj_onscreen_count(RegionObject reg_object) {
        for (RegionObject curr_object : pv_reg_objects) {
            DistanceData dd = curr_object.calc_distance(reg_object,
                    false, (float) 0.0);
            if (dd != null && dd.pixel_distance == 0) {
                assert (curr_object.i_num_frames_on > 0);
                curr_object.i_num_frames_on++;
                return;
            }
        }

        throw new RuntimeException("BlobClass::increase_obj_onscreen_count() called"
                + " on object that is not a member of this blob-class\n");
    }

    /* *********************************************************************
     * Merges this class with the given class
     ******************************************************************** */
    public void merge_with(BlobClass other_class) {
        ArrayList<RegionObject> other_objects = other_class.get_reg_objects();

        for (RegionObject new_object : other_objects) {
            pv_reg_objects.add((RegionObject) new_object.clone());
        }

        i_screen_pos_x_max = Math.max(i_screen_pos_x_max,
                other_class.i_screen_pos_x_max);
        i_screen_pos_x_min = Math.min(i_screen_pos_x_min,
                other_class.i_screen_pos_x_min);
        i_screen_pos_y_max = Math.max(i_screen_pos_y_max,
                other_class.i_screen_pos_y_max);
        i_screen_pos_y_min = Math.min(i_screen_pos_x_min,
                other_class.i_screen_pos_y_min);
    }

    /* *********************************************************************
     * Goes through all the region-objects assigned to this class, and
     * updates their i_instance_of_class
     ******************************************************************** */
    public void update_class_number(int new_num) {
        for (RegionObject o : pv_reg_objects) {
            o.i_instance_of_class = new_num;
        }
    }

    /* *********************************************************************
     * Returns the minimum (percentage) distance between the objects of two
     * classes
     * Returns false if the two classes donot have a well-defined distance
     ******************************************************************** */
    public DistanceData get_min_distance(BlobClass other_class, float max_shape_area_dif) {
        double min_dist = -1.0;
        int min_pixel_dist = 0;
        for (RegionObject my_obj : pv_reg_objects) {
            for (RegionObject other_obj : other_class.pv_reg_objects) {
                DistanceData dd = my_obj.calc_distance(other_obj, true, max_shape_area_dif);
                if (dd == null) {
                    continue;
                }
                if (min_dist < 0 || dd.perc_distance < min_dist) {
                    min_dist = dd.perc_distance;
                    min_pixel_dist = dd.pixel_distance;
                }
            }
        }

        if (min_dist < 0) {
            return null;
        }

        DistanceData dd = new DistanceData(min_pixel_dist, (float) min_dist);
        return dd;
    }

    /* *********************************************************************
     * Accessor method
     ******************************************************************** */
    public ArrayList<RegionObject> get_reg_objects() {
        return pv_reg_objects;
    }

    public String get_info_text(int num_frames) {
	float on_frame_ratio = (float)i_num_frames_on_scr / (float)num_frames;
	int delta_x = i_screen_pos_x_max - i_screen_pos_x_min;
	int delta_y = i_screen_pos_y_max - i_screen_pos_y_min;
	int boundary_area = delta_x * delta_y;
        return "Number of Frames on Screen: " + i_num_frames_on_scr + "\n"
			 + "On Screen Ratio: " + on_frame_ratio + "\n"
			 + "Screen Positions Bound: (" + i_screen_pos_x_min + ","
			 + i_screen_pos_y_min + ") - (" + i_screen_pos_x_max + ","
			 + i_screen_pos_y_max + ")" + "\n"
			 + "Boundary Area: " + boundary_area + "\n"
			 + "Discovered on frame #" + i_discovered_on_frame + "\n";
}}
