/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.disco;

import atariagents.screen.CustomALEPalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import nips11.parameters.DISCOParameters;
import rlVizLib.general.ParameterHolder;

/**
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class RegionManager {
    public static final int MAX_NUM_OBJECTS = 1000;

    protected ScreenMatrix pm_background_matrix;
    protected ScreenMatrix pm_region_matrix;
    protected int i_curr_num_regions;
    protected int i_prev_num_regions;
    protected ArrayList<RegionObject> pv_curr_objects;
    protected ArrayList<RegionObject> pv_prev_objects;
    protected ArrayList<RegionObject> pv_curr_merged_objects;
    protected ArrayList<RegionObject> pv_prev_merged_objects;
    protected boolean b_plot_region_matrix_pre_merge;
    protected boolean b_plot_region_matrix_post_merge;
    protected int i_screen_width;
    protected int i_screen_height;
    protected float f_max_perc_difference;
    protected int i_max_obj_velocity;
    protected float f_max_shape_area_dif;

    public RegionManager(ParameterHolder params, ScreenMatrix background_matrix) {
        pm_background_matrix = background_matrix;
        i_screen_width = background_matrix.width;
        i_screen_height = background_matrix.height;

        // initilize the region matrix
        pm_region_matrix = new ScreenMatrix(i_screen_width, i_screen_height);

        i_curr_num_regions = 0;
        i_prev_num_regions = 0;

        // initlize the object vectors
        pv_curr_objects = new ArrayList<RegionObject>();
        pv_prev_objects = new ArrayList<RegionObject>();
        for (int i = 0; i < MAX_NUM_OBJECTS; i++) {
            pv_curr_objects.add(null);
            pv_prev_objects.add(null);
        }
        pv_curr_merged_objects = new ArrayList<RegionObject>();
        pv_prev_merged_objects = new ArrayList<RegionObject>();

        DISCOParameters.parseParameters(params);
        
        b_plot_region_matrix_pre_merge = DISCOParameters.plotRegionMatrixPreMerge;
        b_plot_region_matrix_post_merge = DISCOParameters.plotRegionMatrixPostMerge;
        f_max_perc_difference = (float)DISCOParameters.maxPercDifference;
        i_max_obj_velocity = DISCOParameters.maxObjVelocity;
        f_max_shape_area_dif = (float)DISCOParameters.maxShapeAreaDif;
    }

    public static void addParameters(ParameterHolder params) {
        DISCOParameters.addParameters(params);
    }

    public ArrayList<RegionObject> extract_objects_from_new_screen(
            ScreenMatrix screen_matrix, int frame_number) {
        swap_curr_and_prev_region_objects();
        // 1- Use our naive method to extract regions
        i_curr_num_regions = extract_regions(screen_matrix);
        if (i_curr_num_regions > MAX_NUM_OBJECTS) {
            throw new UnsupportedOperationException("RegionManager: number of "
                    + "discovered regions(" + i_curr_num_regions + ") is high than MAX_NUM_OBJECTS");
        }

        if (b_plot_region_matrix_pre_merge) {
            plot_region_matrix("pre_merge", frame_number);
        }

        // 2- Assign a RegionObject to each of the extracted region
        for (int reg_num = 1; reg_num <= i_curr_num_regions; reg_num++) {
            RegionObject new_object = new RegionObject();
            new_object.extract_from_region_matrix(pm_region_matrix, reg_num);
            if (new_object.b_is_valid) {
                if (pv_curr_objects.size() <= reg_num) // Add null objects (MGB)
                    for (int i = pv_curr_objects.size(); i <= reg_num; i++)
                        pv_curr_objects.add(null);

                pv_curr_objects.set(reg_num, new_object);

                // Find the corresponding object in previous frame,
                RegionObject previous_object = new_object.get_closest_obj_in_list(
                        pv_prev_objects, i_prev_num_regions, f_max_shape_area_dif,
                        f_max_perc_difference, i_max_obj_velocity);

                // Calculate object's velocity
                new_object.p_previous_object = previous_object;
                calc_object_velocity(new_object);
            }
        }

        // 3- Merge regions in the region_matrix that are connected and
        // correspond to objects with equal velocity
        merge_equivalent_regions();

        if (b_plot_region_matrix_post_merge) {
            plot_region_matrix("post_merge", frame_number);
        }

        // 4- Generate a new list of merged objects
        for (int reg_num = 1; reg_num <= i_curr_num_regions; reg_num++) {
            RegionObject new_object = new RegionObject();
            new_object.extract_from_region_matrix(pm_region_matrix, reg_num);
            if (!new_object.b_is_valid) {
                continue; // empty region
            }
            if (new_object.i_width == 1
                    && new_object.i_height == 1) {
                continue; // ignore objects consisting of exactly one pixel
            }
            RegionObject premerge_obj = pv_curr_objects.get(reg_num);
            assert (premerge_obj != null);
            new_object.i_velocity_x = premerge_obj.i_velocity_x;
            new_object.i_velocity_y = premerge_obj.i_velocity_y;
            pv_curr_merged_objects.add(new_object);
        }
        return pv_curr_merged_objects;
    }

    /* *********************************************************************
    Uses a simple sequential method to categorize non-background pixels
    in the given screen as belonging to one of many discrete regions.
    The result of this method is a matrix, where the item representing
    each pixel is either 0 (i.e. background) or i, where is the region
    number. Returns the number of regions found (pre merge).


    This sequential approach is taken from Wikipedia
    (http://en.wikipedia.org/wiki/Blob_extraction)

    Scan the image from left to right and from top to bottom:
    For every pixel:
     *   check the north and west pixel (when considering
    4-connectivity) or the northeast, north, northwest, and west
    pixel for 8-connectivity for a given region criterion.
     *   If none of the neighbors fit the criterion then assign to region
    value of the region counter. Increment region counter.
     *   If only one neighbor fits the criterion assign pixel to that
    region.
     *   If multiple neighbors match and are all members of the same
    region, assign pixel to their region.
     *   If multiple neighbors match and are members of different
    regions, assign pixel to one of the regions
    Indicate that all of these regions are the equivalent.

    Note 1: After this method, the image still needs to be rescanned,
    so the equivalent regions are merged

    Note 2: At this point we only find mono-color regions
     ******************************************************************** */
    public int extract_regions(ScreenMatrix screen_matrix) {
        int num_neighbors = 4;
        int neighbors_y[] = {-1, -1, -1, 0};

        int neighbors_x[] = {-1, 0, 1, -1};
        // Reset the region-matrix
        for (int i = 0; i < i_screen_height; i++) {
            for (int j = 0; j < i_screen_width; j++) {
                // MGB inverted
                pm_region_matrix.matrix[j][i] = -1;
            }
        }
        int region_counter = 1; // region 0 is reseved for the background

        // 1- First Scan
        int i, j, y, x, color_ind, neighbors_ind, found_region;
        for (i = 0; i < i_screen_height; i++) {
            for (j = 0; j < i_screen_width; j++) {
                // MGB inverted (x3)
                color_ind = screen_matrix.matrix[j][i];
                if (color_ind == pm_background_matrix.matrix[j][i]) {
                    // This pixel is part of the background
                    pm_region_matrix.matrix[j][i] = 0;
                    continue;
                }
                // find the region of i,j based on west and north neighbors.
                found_region = -1;
                for (neighbors_ind = 0; neighbors_ind < num_neighbors;
                        neighbors_ind++) {
                    y = i + neighbors_y[neighbors_ind];
                    x = j + neighbors_x[neighbors_ind];
                    if (x < 0 || x >= i_screen_width
                            || y < 0 || y >= i_screen_height) {
                        continue;
                    }
                    int v = pm_region_matrix.matrix[x][y];

                    if (v != 0 && v != -1 && screen_matrix.matrix[x][y] == color_ind) {
                        found_region = v;
                        break;
                    }
                }
                if (found_region == -1) {
                    // this pixel is in a new region
                    // MGB inverted
                    pm_region_matrix.matrix[j][i] = region_counter;
                    region_counter++;
                } else {
                    // MGB inverted
                    pm_region_matrix.matrix[j][i] = found_region;
                }
            }
        }
        // 2- Re-scan the region_matrix, and merge equivalent regions
        int my_region, nb_region, my_color_ind, nb_color_ind;
        int nb_x, nb_y, ind_i, ind_j, lower_region, higher_region;
        for (i = 0; i < i_screen_height; i++) {
            for (j = 0; j < i_screen_width; j++) {
                // MGB inverted
                my_region = pm_region_matrix.matrix[j][i];
                if (my_region == 0) {
                    continue; // background pixel
                }
                // MGB inverted
                my_color_ind = screen_matrix.matrix[j][i];
                for (y = -1; y <= 1; y++) {
                    for (x = -1; x <= 1; x++) {
                        nb_y = i + y;
                        nb_x = j + x;
                        if (nb_x < 0 || nb_x >= i_screen_width
                                || nb_y < 0 || nb_y >= i_screen_height) {
                            continue;
                        }
                        // MGB inverted
                        nb_region = pm_region_matrix.matrix[nb_x][nb_y];
                        if (nb_region == 0) {
                            continue;
                        }
                        // MGB inverted
                        nb_color_ind = screen_matrix.matrix[nb_x][nb_y];
                        if (nb_color_ind == my_color_ind
                                && nb_region != my_region) {
                            // These two regions are equivilant
                            if (my_region > nb_region) {
                                higher_region = my_region;
                                lower_region = nb_region;
                            } else {
                                higher_region = nb_region;
                                lower_region = my_region;
                            }
                            // Go through the region matrix and convert
                            // higher_region to lower_region
                            for (ind_i = 0; ind_i < i_screen_height; ind_i++) {
                                for (ind_j = 0; ind_j < i_screen_width; ind_j++) {
                                    // MGB inverted
                                    if (pm_region_matrix.matrix[ind_j][ind_i] == higher_region) {
                                        pm_region_matrix.matrix[ind_j][ind_i] = lower_region;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return region_counter - 1;
    }

    /* *********************************************************************
    Merges regions in the region_matrix that are connected and
    correspond to objects with equal velocity
     ******************************************************************** */
    public void merge_equivalent_regions() {
        int i, j, shift_x, shift_y, y, x, ind_i, ind_j;
        int my_region_num, neighbor_region_num;
        RegionObject my_obj, nb_obj;
        int high_region_num, low_region_num;
        for (i = 0; i < i_screen_height; i++) {
            for (j = 0; j < i_screen_width; j++) {
                my_region_num = pm_region_matrix.matrix[j][i];
                if (my_region_num == 0) {
                    continue;   // ignore the background pixels
                }
                my_obj = pv_curr_objects.get(my_region_num);
                if (my_obj == null) {
                    continue; // invalid object
                }
                for (shift_y = -3; shift_y <= 3; shift_y++) {
                    for (shift_x = -3; shift_x <= -3; shift_x++) {
                        y = i + shift_y;
                        x = j + shift_x;
                        if (x < 0 || x >= i_screen_width
                                || y < 0 || y >= i_screen_height) {
                            continue;
                        }
                        neighbor_region_num = pm_region_matrix.matrix[x][y];
                        if (neighbor_region_num == 0) {
                            continue;   // ignore the background pixels
                        }
                        nb_obj = pv_curr_objects.get(neighbor_region_num);
                        if (nb_obj == null) {
                            continue; // invalid object
                        }
                        if ((my_region_num != neighbor_region_num)
                                && (my_obj.i_velocity_x == nb_obj.i_velocity_x)
                                && (my_obj.i_velocity_y == nb_obj.i_velocity_y)) {
                            // These two regions should be merged
                            if (my_region_num > neighbor_region_num) {
                                high_region_num = my_region_num;
                                low_region_num = neighbor_region_num;
                            } else {
                                high_region_num = neighbor_region_num;
                                low_region_num = my_region_num;
                            }
                            // Go through the region matrix and convert
                            // high_region_num to low_region_num
                            for (ind_i = 0; ind_i < i_screen_height; ind_i++) {
                                for (ind_j = 0; ind_j < i_screen_width; ind_j++) {
                                    if (pm_region_matrix.matrix[ind_j][ind_i] == high_region_num) {
                                        pm_region_matrix.matrix[ind_j][ind_i] = low_region_num;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /* *********************************************************************
    Calculates the object velocity, based on its location, and the
    location of its coressponding  object in previous frame. If there
    is no previous object assigned, will set velocity to 0.
    The velocity is in pixels/frame unit
     ******************************************************************** */
    public void calc_object_velocity(RegionObject obj) {
        if (obj.p_previous_object == null) {
            obj.i_velocity_x = 0;
            obj.i_velocity_y = 0;
        } else {
            int x_dist = obj.i_center_x - obj.p_previous_object.i_center_x;
            int y_dist = obj.i_center_y - obj.p_previous_object.i_center_y;
            obj.i_velocity_x = x_dist;
            obj.i_velocity_y = y_dist;
        }
    }


/* *********************************************************************
    Swaps  pv_curr_objects with pv_prev_objects
	also swaps pv_c
  ******************************************************************** */
    void swap_curr_and_prev_region_objects() {
        // MGB refactored
        pv_prev_objects = pv_curr_objects;
        pv_curr_objects = new ArrayList<RegionObject>();

        i_prev_num_regions = i_curr_num_regions;
        i_curr_num_regions = 0;

        pv_prev_merged_objects = pv_curr_merged_objects;
        pv_curr_merged_objects = new ArrayList<RegionObject>();
    }

/* *********************************************************************
	Plots the region manager.
	pre_post is either "pre_merge" or "post_merge" and used in filename
  ******************************************************************** */
    public void plot_region_matrix(String pre_post, int frame_number) {
	// copy the region_matrix to a new matrix with color indecies
	ScreenMatrix region_matrix_copy = new ScreenMatrix(pm_region_matrix.width,
                pm_region_matrix.height);
	for (int i = 0; i < i_screen_height; i++) {
		for (int j = 0; j < i_screen_width; j++) {
			int region_num = pm_region_matrix.matrix[j][i];
                        // MGB modified to use CustomALEPalette
                        region_matrix_copy.matrix[j][i] = (region_num & 0xFF);
                        if (region_num > 0xFF)
                            System.err.println ("Warning: region color above 0xFF.");
		}
	}
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setMinimumIntegerDigits(9);
        formatter.setGroupingUsed(false);
        
        String indexString = formatter.format(frame_number);
        // MGB refactored
        String filename = "region_matrx__"+pre_post+"__frame_"+indexString+".png";

        ExportScreen.exportCustom(region_matrix_copy, filename);
    }
}
