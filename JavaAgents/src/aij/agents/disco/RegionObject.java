/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package aij.agents.disco;

import atariagents.screen.ScreenMatrix;
import java.util.ArrayList;

/** Ported from blob_object.{h,cpp}.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class RegionObject extends BlobObject implements Cloneable {

    public ScreenMatrix pm_shape_matrix;
    public int i_region_number;
    // @todo public RegionObject p_previous_object;
    public boolean b_is_valid;
    public int i_disc_frame_num;
    public int i_num_frames_on;
    public String str_disc_text;
    public int i_width, i_height;
    public RegionObject p_previous_object;

    RegionObject() {
        super(0, 0);
        b_is_valid = false;
        pm_shape_matrix = null;
        i_region_number = -1;
        i_disc_frame_num = -1;
        i_num_frames_on = -1;
        str_disc_text = "";
        i_width = i_height = -1;

        p_previous_object = null;
    }

    public boolean extract_from_region_matrix(ScreenMatrix region_matrix,
            int region_num) {
        // @todo @clean
        pm_shape_matrix = null;

        // Find the indices in the region, with the given number
        ArrayList<Integer> pv_row_inds = new ArrayList<Integer>();
        ArrayList<Integer> pv_col_inds = new ArrayList<Integer>();

        VectorTools.simple_where(region_matrix, region_num, pv_row_inds, pv_col_inds);
        int num_pixels = pv_row_inds.size();
        if (num_pixels == 0) {
            // This region number doesn't have any actual pixels on screen
            b_is_valid = false;
            return false;
        }

        // find the shape of the region
        int x_min, x_min_ind, x_max, x_max_ind;
        int y_min, y_min_ind, y_max, y_max_ind;
        x_min_ind = VectorTools.vect_min(pv_col_inds);
        x_max_ind = VectorTools.vect_max(pv_col_inds);
        y_min_ind = VectorTools.vect_min(pv_row_inds);
        y_max_ind = VectorTools.vect_max(pv_row_inds);
        x_min = pv_col_inds.get(x_min_ind);
        x_max = pv_col_inds.get(x_max_ind);
        y_min = pv_row_inds.get(y_min_ind);
        y_max = pv_row_inds.get(y_max_ind);

        i_width = x_max - x_min + 1;
        i_height = y_max - y_min + 1;
        assert (i_width > 0 && i_height > 0);

        // Full the shape matrix with region pixels
        pm_shape_matrix = new ScreenMatrix(i_width, i_height);

        for (int c = 0; c < num_pixels; c++) {
            int i = pv_row_inds.get(c) - y_min;
            int j = pv_col_inds.get(c) - x_min;
            assert (i >= 0 && j >= 0 && i < i_height && j < i_width);
            // MGB inverted
            pm_shape_matrix.matrix[j][i] = 1;
        }

        // Set the rest of variables
        i_region_number = region_num;
        i_center_x = x_min + (int) (i_width / 2);
        i_center_y = y_min + (int) (i_height / 2);
        b_is_valid = true;
        return true;
    }

    public DistanceData calc_distance(RegionObject other_object,
            boolean allow_different_size, float max_shape_area_dif) {
        return ShapeTools.calc_shape_distance(pm_shape_matrix,
                other_object.pm_shape_matrix,
                allow_different_size, max_shape_area_dif);
    }

    public RegionObject get_closest_obj_in_list(
            ArrayList<RegionObject> pv_obj_list,
            int list_length,
            float max_shape_area_dif,
            float max_perc_difference,
            int max_obj_velocity) {
        RegionObject closest_prev_obj = null;	// closest object found so far
        int closest_pixel_distance = -1;		// Their pixel differecne
        int closest_sq_distance = -1; // The square of their actual distance

        for (RegionObject prev_obj : pv_obj_list) {
            if (prev_obj == null || prev_obj.b_is_valid == false) {
                continue; // invalid object
            }
            int horizontal_dist = Math.abs(i_center_x
                    - prev_obj.i_center_x);
            int vertical_dist = Math.abs(i_center_y
                    - prev_obj.i_center_y);

            if (horizontal_dist < max_obj_velocity
                    && vertical_dist < max_obj_velocity) {
                DistanceData dd = calc_distance(prev_obj,
                        true, max_shape_area_dif);
                if (dd == null) {
                    continue;
                }
                int pixel_distance = dd.pixel_distance;
                float perc_distance = dd.perc_distance;

                if (perc_distance < max_perc_difference) {
                    int sq_distance = (horizontal_dist * horizontal_dist);
                    sq_distance += (vertical_dist * vertical_dist);
                    if (closest_prev_obj == null
                            || pixel_distance < closest_pixel_distance) {
                        closest_prev_obj = prev_obj;
                        closest_pixel_distance = pixel_distance;
                        closest_sq_distance = sq_distance;
                    } else if (pixel_distance == closest_pixel_distance
                            && closest_sq_distance > sq_distance) {
                        // We have found an object that is as similar as
                        // closest_prev_obj, but it is physicaly closer to
                        // current object
                        closest_prev_obj = prev_obj;
                        closest_pixel_distance = pixel_distance;
                        closest_sq_distance = sq_distance;
                    }
                }
            }
        }
        return closest_prev_obj;
    }

    /* *********************************************************************
    Save the shape_matrix as a PNG file
     ******************************************************************** */
    public void plot(String filename) {
        ScreenMatrix plot_matrix = new ScreenMatrix(i_width, i_height);

        for (int i = 0; i < i_height; i++) {
            for (int j = 0; j < i_width; j++) {
                if (pm_shape_matrix.matrix[j][i] != 0) {
                    plot_matrix.matrix[j][i] = 1;
                } else {
                    plot_matrix.matrix[j][i] = 0;
                }
            }
        }
        ExportScreen.exportBW(plot_matrix, filename);
    }

    /* *********************************************************************
    Returns the string version of this object, so it can be imported
    later. The string format is as follows:
    class_num,width,height|
    M[0,0], M[0,1], ..., M[0,n]|
    M[1,0], M[1,1], ..., M[1,n]|
    ...
    M[m,0], M[m,1], ..., M[m,n]|
     ******************************************************************** */
    String export_object_as_txt() {
        String exp_str = i_instance_of_class + "," + i_width + "," + i_height + "|";

        for (int i = 0; i < i_height; i++) {
            for (int j = 0; j < i_width; j++) {
                exp_str += pm_shape_matrix.matrix[j][i];
                if (j < i_width - 1) {
                    exp_str += ",";
                } else {
                    exp_str += "|";
                }
            }
        }
        return exp_str;
    }

/* *********************************************************************
 * Returns a string containing general info about this object.
 * This is used to save object info, after class-discovery
 ******************************************************************** */
    String get_info_text() {
        String info_str;

        info_str = "Discovered on frame: " + i_disc_frame_num + "\n"
			 + "Number of frames on screen: " + i_num_frames_on + "\n"
			 + "Size: " + i_height + "x" + i_width + "\n"
			 + "Comment: " + str_disc_text + "\n";
	return info_str;
}

    static boolean compare_objects_by_shape_size(RegionObject obj1, RegionObject obj2) {
        int obj1_area = obj1.i_width * obj1.i_height;
        int obj2_area = obj2.i_width * obj2.i_height;
        if (obj1_area > obj2_area) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object clone() {
        try {
            RegionObject ro = (RegionObject) super.clone();
            ro.pm_shape_matrix = (ScreenMatrix) this.pm_shape_matrix.clone();
            ro.str_disc_text = this.str_disc_text;

            return ro;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
