/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents;

import atariagents.io.Actions;
import atariagents.screen.ScreenMatrix;
import java.io.File;
import marcgb.tools.config.Parameters;
import nips11.agents.AbstractAgent;
import nips11.agents.data.Trajectory;
import nips11.agents.data.TrajectoryProvider;
import nips11.parameters.NIPSParameters;
import nips11.rl.EnvironmentProvider;
import rlVizLib.general.ParameterHolder;

/** A RL environment + agent, all wrapped up as one.
 *
 * @author marc
 */
public class ExpertScoreBoundsAgent extends AbstractAgent {
    public static final int NUM_RIGHT_DISPLAY_MODES = 0;
    
    protected final int numActions = Actions.numPlayerActions;
    
    protected EnvironmentProvider environment;
    protected TrajectoryProvider trajectories;

    protected Trajectory currentTrajectory;
    
    protected boolean requestReset;
    protected boolean ignoreNextFrame;
    
    protected int frameNumber;
    protected int episodeNumber;
    protected int episodeStartFrame;

    protected double rewardThisEpisode = 0;

    protected double minScore = Double.POSITIVE_INFINITY;
    protected double maxScore = Double.NEGATIVE_INFINITY;

    protected boolean firstStep;
    
    public ExpertScoreBoundsAgent(ParameterHolder params) {
        super(params);

        NIPSParameters.parseParameters(params);
        
        environment = new EnvironmentProvider(params);
        trajectories = new TrajectoryProvider(NIPSParameters.actionStepSize, numActions, params);
        
        episodeNumber = 1;
        frameNumber = 0;
        requestReset = true;
        firstStep = true;
    }

    public static void addParameters(ParameterHolder params) {
        NIPSParameters.addParameters(params);
        EnvironmentProvider.addParameters(params);
        TrajectoryProvider.addParameters(params);
    }

    @Override
    public boolean shouldTerminate() {
        // Terminate when we are told to do so by the outside world
        boolean shouldTerminate = (currentTrajectory == null || comm.wantsTerminate());

        if (shouldTerminate)
            System.err.println ("SCORE "+minScore+" "+maxScore);
        
        return shouldTerminate;
    }

    @Override
    public long getPauseLength() {
        return 0;
    }

    protected void nextTrajectory() {
        currentTrajectory = trajectories.getTrajectory();
        if (currentTrajectory != null) {
            currentTrajectory.initIteration();
        }
        // When the current trajectory becomes null, we will terminate
    }

    @Override
    public int selectAction() {
        // Assume the first trajectory has one action; otherwise bust
        int action;

        // No more trajectories; we will terminate
        if (currentTrajectory == null)
            action = 0;
        else if(currentTrajectory.hasNext())
            action = currentTrajectory.next();
        else {
            System.err.println ("WARNING: Trajectory does not end episode.");
            // Pad with random action
            action = (int)(Math.random() * numActions);
            endEpisode();
        }

        return action;
    }

    @Override
    public void observeImage(ScreenMatrix image) {
        if (firstStep) {
            firstStep = false;
            newEpisode();
        }

        // To ignore the frame right after a reset
        if (ignoreNextFrame) {
            ignoreNextFrame = false;
            return;
        }

        frameNumber++;
        
        environment.observe(image, comm.getRAM(), comm.getRLData());
        boolean terminal = environment.isTerminal();
        double reward = environment.getReward();

        environment.newStep();

        rewardThisEpisode += reward;

        updateScoreBounds();

        if (terminal)
            endEpisode();
    }

    protected void updateScoreBounds() {
        double score = environment.getScore();
        if (score < minScore) minScore = score;
        if (score > maxScore) maxScore = score;
    }

    protected void newEpisode() {
        // Move on to the next trajectory, done or not - otherwise we will
        //  spill onto the next episode and permanently muck things up
        nextTrajectory();
        environment.reset();
    }

    protected void endEpisode() {
        System.err.println("EPISODE " + episodeNumber + " " + rewardThisEpisode);
        rewardThisEpisode = 0;
        episodeNumber++;

        requestReset = true;
        firstStep = true;
        frameNumber = 0;        
    }

    @Override
    public int specialAction() {
        if (requestReset) {
            requestReset = false;
            ignoreNextFrame = true;
            return Actions.map("system_reset");
        }
        else
            return -1;
    }

    public boolean wantsRamData() {
        return false;
    }
    
    public boolean wantsRLData() {
        return true;
    }

    public boolean wantsStandardSize() {
        return true;
    }

    @Override
    protected void keyboardEvents() {
        super.keyboardEvents();
    }

    public static void main(String[] args) {
        ParameterHolder params = new ParameterHolder();
        ExpertScoreBoundsAgent.addParameters(params);

        // Parse arguments first - somewhat awkward
        int argIndex = 0;
        String configFilename = null;

        int action = -1;
        
        while (argIndex < args.length) {
            String a = args[argIndex];

            if (!a.startsWith("-"))
                configFilename = a;
            if (a.startsWith("--action")) {
                String[] tokens = a.split("=");
                action = Integer.parseInt(tokens[1]);
            }
            argIndex++;
        }

        if (configFilename != null) {
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }

        // Apply single action choice
        if (action != -1)
            params.setIntegerParam(NIPSParameters.PARAM_SINGLE_ACTION, action);

        ExpertScoreBoundsAgent agent = new ExpertScoreBoundsAgent(params);

        try {
            agent.run();
        }
        catch (Exception e) {
            // make sure the UI dies, or we will wait forever
            agent.ui.die();
            agent.comm.close();
            throw new RuntimeException(e);
        }
    }
}
