/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.rl.features;

import atariagents.screen.ColorPalette;
import atariagents.screen.NTSCPalette;
import atariagents.screen.ScreenMatrix;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ListIterator;
import nips11.agents.data.FrameHistory;
import nips11.agents.models.FeatureList;
import nips11.agents.rlfeatures.*;
import nips11.agents.features.colors.FeatureColors;
import nips11.parameters.BASSParameters;
import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class QuantizedFeatureMap extends RLFeatureMap implements Displayable {
    protected final int numColumns;
    protected final int numRows;
    protected final int bitsPerChannel;

    protected final int numColors;
    protected final ScreenMatrix quantizedScreen;
    protected final FeatureList features;
    
    protected final ColorPalette palette = new NTSCPalette();

    public static void addParameters(ParameterHolder params) {
        BASSParameters.addParameters(params);
    }
    
    public QuantizedFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params);

        // Obtain parameters from config
        BASSParameters.parseParameters(params);

        numColumns = BASSParameters.numColumns;
        numRows = BASSParameters.numRows;
        bitsPerChannel = BASSParameters.bitsPerChannel;

        // 3 color channels
        numColors = 1 << (bitsPerChannel*3);

        // Create the matrix that holds our quantized data
        quantizedScreen = new ScreenMatrix(numColumns, numRows);

        // Create the list of features - its size is fixed, so we allocate pairs
        //  right now
        features = new FeatureList();
        for (int i = 0; i < numActiveFeatures(); i++)
            features.addFeature(-1, 1);
    }

    @Override
    public FeatureList getFeatures(FrameHistory history) {
        ScreenMatrix screen = history.getLastFrame(0);

        quantizeScreen(screen);
        setFeatures();

        return features;
    }

    /** Converts a full-size screen to a smaller version. The resulting screen
     *   matrix is in RGB.
     * 
     * @param original
     */
    protected void quantizeScreen(ScreenMatrix original) {
        int blockWidth = original.width / numColumns;
        int blockHeight = original.height / numRows;
        int blockArea = blockWidth * blockHeight;

        int xOffset = 0;

        // for each pixel in the destination (smaller) image
        for (int x = 0; x < numColumns; x++) {
            int yOffset = 0;

            for (int y = 0; y < numRows; y++) {
                // Average over the block of pixels
                int avgRed = 0;
                int avgGreen = 0;
                int avgBlue = 0;

                for (int dx = xOffset; dx < xOffset + blockWidth; dx++)
                    for (int dy = yOffset; dy < yOffset + blockHeight; dy++) {
                        // Map this pixel using the palette
                        // @todo potentially optimize by avoiding a bunch of calls
                        //  e.g. use the palette directly rather than obtaining a Color object
                        Color c = palette.get(original.matrix[dx][dy]);
                        avgRed += c.getRed();
                        avgGreen += c.getGreen();
                        avgBlue += c.getBlue();
                    }

                avgRed /= blockArea;
                avgGreen /= blockArea;
                avgBlue /= blockArea;

                // @dbg
                if (avgRed > 255 || avgGreen > 255 || avgBlue > 255 ||
                        avgRed < 0 || avgGreen < 0 || avgBlue < 0) {
                    System.err.println ("Bad color: "+avgRed+" "+avgGreen+" "+avgBlue);
                }
                
                int rgb = (avgRed << 16) | (avgGreen << 8) | (avgBlue);
                quantizedScreen.matrix[x][y] = rgb;
                
                yOffset += blockHeight;
            }

            xOffset += blockWidth;
        }
    }

    protected void setFeatures() {
        int indexOffset = 0;
        
        ListIterator<FeatureList.Pair> it = features.active.listIterator();

        int shiftCount = 8 - bitsPerChannel;

        // Go through the quantized screen and assign the current color value
        //  to each feature
        for (int x = 0; x < numColumns; x++)
            for (int y = 0; y < numRows; y++) {
                FeatureList.Pair pair = it.next();

                int color = quantizedScreen.matrix[x][y];
                int red = color >> 16;
                int green = (color >> 8) & 0xFF;
                int blue = (color) & 0xFF;

                // Re-encode it as a 9-bit RGB
                int colorIndex = 
                        ((red >> shiftCount) << bitsPerChannel * 2) |
                        ((green >> shiftCount) << bitsPerChannel) |
                        ((blue >> shiftCount));

                pair.index = indexOffset + colorIndex;
                indexOffset += numColors;
            }
    }

    public final int numActiveFeatures() {
        return numColumns * numRows;
    }
    
    @Override
    public int numFeatures() {
        return numColumns * numRows * numColors;
    }

    @Override
    public int historyLength() {
        return 1;
    }

    public int numDisplayModes() {
        return 1;
    }

    public BufferedImage getDisplay(ScreenMatrix screen, int mode) {
        if (quantizedScreen == null) return null;
        
        BufferedImage img = new BufferedImage(screen.width, screen.height, BufferedImage.TYPE_INT_RGB);

        int blockWidth = screen.width / numColumns;
        int blockHeight = screen.height / numRows;
        
        for (int x = 0; x < screen.width; x++) {
            int qx = x / blockWidth;
            if (qx >= numColumns) break;
            for (int y = 0; y < screen.height; y++) {
                int qy = y / blockHeight;

                if (qy >= numRows) break;
                img.setRGB(x, y, quantizedScreen.matrix[qx][qy]);
            }
        }

        return img;
    }

    public static QuantizedFeatureMap dynamicLoad(ParameterHolder params, FeatureColors colors) {
        return new QuantizedFeatureMap(params, colors);
    }
}
