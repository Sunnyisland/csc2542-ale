/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.rl.features;

import aij.agents.disco.BlobObject;
import java.util.ArrayList;

/** Encapsulates a list of class instances, grouped by class.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ClassInstancesMap {
    protected ArrayList<BlobObject>[] instances;

    public ClassInstancesMap(int numClasses) {
        instances = new ArrayList[numClasses];

        for (int i = 0; i < instances.length; i++)
            instances[i] = new ArrayList<BlobObject>();
    }
    public BlobObject get(int classIndex, int objectIndex) {
        return instances[classIndex].get(objectIndex);
    }

    public void add(int classIndex, BlobObject object) {
        instances[classIndex].add(object);
    }

    public ArrayList<BlobObject> getInstancesOf(int classIndex) {
        return instances[classIndex];
    }
}
