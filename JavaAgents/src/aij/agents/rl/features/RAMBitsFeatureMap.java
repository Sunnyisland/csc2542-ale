/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.rl.features;

import atariagents.io.Actions;
import atariagents.io.ConsoleRAM;
import nips11.agents.rlfeatures.*;
import atariagents.screen.ScreenMatrix;
import features.parameters.FeatureParameters;
import java.awt.image.BufferedImage;
import java.util.ListIterator;
import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import rlVizLib.general.ParameterHolder;

/** A RAM-based feature map, as per Yavar Naddaf's thesis, with bells and whistles.
 *
 * @author marc
 */
public class RAMBitsFeatureMap extends RLFeatureMap implements Displayable {
    protected int historyLength;
    protected boolean encodeLastAction;
    protected boolean encodePairwiseFeatures;

    protected static final int ramBitSize = ConsoleRAM.RAM_SIZE * 8;

    protected FeatureList features;

    public static void addParameters(ParameterHolder params) {
        FeatureParameters.addParameters(params);
    }
    
    public RAMBitsFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params);

        FeatureParameters.parseParameters(params);
        historyLength = FeatureParameters.historyLength;
        encodeLastAction = FeatureParameters.encodeLastAction;
        encodePairwiseFeatures = FeatureParameters.jointEncoding;

        features = new FeatureList();
    }

    /** Returns the size of the unhashed screen bit vector */
    protected int vectorSize() {
        return ramBitSize * historyLength;
    }

    /** Returns a quantization of the last screen. See Yavar Naddaf's thesis.
      */
    @Override
    public FeatureList getFeatures(FrameHistory history) {
        binarizeRAM(history);

        if (encodePairwiseFeatures)
            makePairwiseFeatures();

        if (encodeLastAction)
            encodeByAction(history.getLastAction(0));

        return features;
    }

    // @opt
    private int[] singles;
    private int singlesIndex;
    
    /** Binarizes the screen, using numColors bits per pixel.
     * 
     */
    public void binarizeRAM(FrameHistory history) {
        // Go through first screen; set one bit per pixel, corr. to its color
        // @opt features = new FeatureList();
        
        int featureIndex = 0;

        // @opt
        if (singles == null) singles = new int[numSingleFeaturesPerAction()];
        
        // @opt
        singlesIndex = 0;
        for (int t = 0; t < history.maxHistoryLength(); t++) {
            ConsoleRAM ramObject = history.getLastRAM(t);

            // Everything should be fine, provided we do not use bitList
            if (ramObject == null) {
                break;
            }

            int ram[] = ramObject.ram;

            // Encode the RAM as a bit vector
            for (int i = 0; i < ram.length; i++) {
                for (int bitMask = 0x01; bitMask <= 0x80; featureIndex++, bitMask <<= 1) {
                    if ((ram[i] & bitMask) != 0) {
                        // @opt features.addFeature(featureIndex, 1.0);
                        singles[singlesIndex++] = featureIndex;
                    }
                }
            }
        }

        // @opt
        ListIterator<FeatureList.Pair> it = features.active.listIterator();

        // Replace features rather than allocating them
        int fi = 0;
        while (fi < singlesIndex && it.hasNext()) {
            FeatureList.Pair p = it.next();
            p.index = singles[fi++];
        }

        // Pad the remainder with new features
        while (fi < singlesIndex) {
            it.add(features.new Pair(singles[fi++], 1.0));
        }
    }

    // @opt
    private int[] doubles;
    private int doublesIndex;
    
    protected void makePairwiseFeatures() {
        // Start adding features after the basic features
        int basicOffset = numSingleFeaturesPerAction();

        // @opt int numSingles = features.active.size();
        
        // @opt
        if (doubles == null) doubles = new int[numFeaturesPerAction() - numSingleFeaturesPerAction()];
        // @opt
        doublesIndex = 0;

        for (int sf = 0; sf < singlesIndex - 1; sf++) {
            int index1 = singles[sf];

            int offset1 = basicOffset + index1 * basicOffset - (index1 + 2) * (index1 + 1) / 2;

            // Iterate over all the other bits
            for (int sof = sf + 1; sof < singlesIndex; sof++) {
                int index2 = singles[sof];

                // Add the (p1,p2) pair
                int pairIndex = (offset1 + index2);
                // @opt doubles.add(pairIndex);
                doubles[doublesIndex++] = pairIndex;
            }
        }

        // Replace features rather than allocating them
        ListIterator<FeatureList.Pair> it = features.active.listIterator(singlesIndex);

        int fi = 0;
        while (fi < doublesIndex && it.hasNext()) {
            FeatureList.Pair p = it.next();
            p.index = doubles[fi++];
        }

        // Remove the remaining elements :(
        while (it.hasNext()) { // Implicit is fi == doublesIndex
            it.next();
            it.remove();
        }

        // Pad the remainder with new features
        while (fi < doublesIndex) {
            it.add(features.new Pair(doubles[fi++], 1.0));
        }
    }

    /** We block-encode the last action */
    protected void encodeByAction(int lastAction) {
        if (lastAction == -1) // First frame, assume noop
            lastAction = 0;
        
        int blockSize = numFeaturesPerAction();
        int offset = blockSize * lastAction;

        // Bump all actions up to the correct block
        for (FeatureList.Pair p : features.active) {
            p.index += offset;
        }
    }

    public int numDisplayModes() {
        return 0;
    }

    public BufferedImage getDisplay(ScreenMatrix screen, int mode) {
        switch (mode) {
            default: throw new IllegalArgumentException("Invalid display mode.");
        }
    }

    public int numSingleFeaturesPerAction() {
        return ramBitSize * historyLength;
    }

    public int numFeaturesPerAction() {
        int n = numSingleFeaturesPerAction();

        // All pairwise combinations + basic features
        return (n + 1) * n / 2;
    }

    @Override
    public int numFeatures() {
        return numFeaturesPerAction() * (encodeLastAction ? Actions.numPlayerActions : 1);
    }

    @Override
    public int historyLength() {
        return historyLength;
    }

    public static boolean wantsRAM() {
        return true;
    }

    public static boolean wantsScreen() {
        return false;
    }

    public static RAMBitsFeatureMap dynamicLoad(ParameterHolder params, FeatureColors colors) {
        return new RAMBitsFeatureMap(params, colors);
    }
}
