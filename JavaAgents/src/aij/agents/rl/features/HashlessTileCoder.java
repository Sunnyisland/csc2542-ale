/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.rl.features;

/**
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class HashlessTileCoder {
    protected final int numTilings;
    protected final int numTiles;
    protected final int numDims;

    protected final int featuresPerTiling;
    protected final int[] coordinateOffsets;

    /** Working variables */
    protected int[] qvalues;
    
    public HashlessTileCoder(int numTilings, int numTiles, int numDims) {
        this.numTilings = numTilings;
        this.numTiles = numTiles;
        this.numDims = numDims;

        featuresPerTiling = pow(numTiles, numDims);
        coordinateOffsets = new int[numDims];
        qvalues = new int[numDims];
        
        int offset = 1;

        for (int i = 0; i < coordinateOffsets.length; i++) {
            coordinateOffsets[i] = offset;
            offset *= numTiles;
        }
    }
    
    public static int pow(int m, int n) {
        if (n == 0)
            return 1;
        else if (n == 1)
            return m;
        else if ((n & 1) == 0) {
            int halfpow = pow(m, n >> 1);
            return halfpow * halfpow;
        }
        else
            return m * pow(m, n - 1);
    }
    
    public void tile(int[] indices, double[] values) {
        // For each tiling...
        int tilingOffset = 0;

        // Quantize values first
        for (int i = 0; i < values.length; i++)
            qvalues[i] = (int)(values[i] * numTiles * numTilings);

        for (int tiling = 0; tiling < numTilings; tiling++) {
            int index = computeIndex(qvalues, tiling) + tilingOffset;
            indices[tiling] = index;

            tilingOffset += featuresPerTiling;
        }
    }

    public int computeIndex(int[] values, int tiling) {
        int index = 0;

        for (int dim = 0; dim < numDims; dim++) {
            int shiftedValue = (values[dim] - tiling);
            // Tilings beyond the first extend the first tile to left infinity
            if (shiftedValue < 0) shiftedValue = 0;
            shiftedValue /= numTilings;

            index += shiftedValue * coordinateOffsets[dim];
        }

        return index;
    }
}
