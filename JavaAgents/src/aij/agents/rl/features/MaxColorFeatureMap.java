/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.rl.features;

import atariagents.screen.ColorPalette;
import nips11.agents.rlfeatures.*;
import atariagents.screen.NTSCPalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import java.awt.Color;
import java.awt.image.BufferedImage;
import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import nips11.parameters.BASSParameters;
import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class MaxColorFeatureMap extends RLFeatureMap implements Displayable {
    protected FeatureColors colors;

    protected int numColumns;
    protected int numRows;

    protected final ScreenMatrix quantizedScreenPrimary;
    protected final ScreenMatrix quantizedScreenSecondary;

    protected final ColorPalette palette = new NTSCPalette();
    protected final ScreenConverter converter = new ScreenConverter(palette);

    public static void addParameters(ParameterHolder params) {
        BASSParameters.addParameters(params);
    }
    
    public MaxColorFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params);

        this.colors = colors;

        // Obtain parameters from config
        BASSParameters.parseParameters(params);

        numColumns = BASSParameters.numColumns;
        numRows = BASSParameters.numRows;

        quantizedScreenPrimary = new ScreenMatrix(numColumns, numRows);
        quantizedScreenSecondary = new ScreenMatrix(numColumns, numRows);
    }

    /** Returns a quantization of the last screen. See Yavar Naddaf's thesis.
      */
    @Override
    public FeatureList getFeatures(FrameHistory history) {
        FeatureList features = new FeatureList();
        // First find out which colors are present in which blocks
        getSingleFeatures(features, history);

        return features;
    }

    protected void getSingleFeatures(FeatureList features, FrameHistory history) {
        int numColors = colors.numColors();

        ScreenMatrix screen = history.getLastFrame(0);

        int blockWidth = screen.width / numColumns;
        int blockHeight = screen.height / numRows;

        int featuresPerBlock = numColors;
        int blockIndex = 0;

        // For each pixel block
        for (int bx = 0; bx < numColumns; bx++) {
            for (int by = 0; by < numRows; by++) {
                // @optimize
                int[] colorCount = new int[numColors];
                
                int xo = bx * blockWidth;
                int yo = by * blockHeight;

                // Determine which colors are present; ignore background
                for (int x = xo; x < xo + blockWidth; x++)
                    for (int y = yo; y < yo + blockHeight; y++) {
                        int pixelColor = screen.matrix[x][y];
                        int enc = colors.encode(pixelColor);
                        colorCount[enc]++;
                    }

                // Find the most present color
                int maxColor = -1;
                int maxCount = 0;
                int maxColor2 = -1;
                int maxCount2 = 0;

                // @todo handle ties?
                for (int c = 0; c < numColors; c++) {                    
                    int ct = colorCount[c];
                    if (ct > maxCount) {
                        maxCount2 = maxCount;
                        maxColor2 = maxColor;
                        maxCount = ct;
                        maxColor = c;
                    }
                    else if (ct > maxCount2) {
                        maxCount2 = ct;
                        maxColor2 = c;
                    }
                }

                int index1 = maxColor + blockIndex;

                // @optimize?
                quantizedScreenPrimary.matrix[bx][by] = maxColor;
                // Only add this if we have two different colors in the tile
                if (maxCount2 > 0) {
                    int index2 = maxColor2 + blockIndex;

                    // Re-order so that sorting is preserved
                    if (index1 > index2) {
                        int tmp = index1;
                        index1 = index2;
                        index2 = tmp;
                    }

                    features.addFeature(index1, 1.0);
                    features.addFeature(index2, 1.0);
                    quantizedScreenSecondary.matrix[bx][by] = maxColor2;
                }
                else {
                    features.addFeature(index1, 1.0);
                    quantizedScreenSecondary.matrix[bx][by] = maxColor;
                }

                blockIndex += featuresPerBlock;
            }
        }
    }

    public int numDisplayModes() {
        return 1;
    }

    public BufferedImage getDisplay(ScreenMatrix screen, int mode) {
        switch (mode) {
            case 0: return getMaxColorDisplay(screen);
            default: throw new IllegalArgumentException("Invalid display mode.");
        }
    }

    protected BufferedImage getMaxColorDisplay(ScreenMatrix screen) {
        int blockWidth = screen.width / numColumns;
        int blockHeight = screen.height / numRows;

        BufferedImage img = new BufferedImage(screen.width, screen.height, BufferedImage.TYPE_INT_RGB);

        for (int x = 0; x < screen.width; x++) {
            int qx = x / blockWidth;
            if (qx >= numColumns) break;
            for (int y = 0; y < screen.height; y++) {
                int qy = y / blockHeight;

                if (qy >= numRows) break;
                int evenness = (x + y) & 0x1;

                if (evenness == 0)
                    img.setRGB(x, y, palette.get(quantizedScreenPrimary.matrix[qx][qy] << 1).getRGB());
                else
                    img.setRGB(x, y, palette.get(quantizedScreenSecondary.matrix[qx][qy] << 1).getRGB());
            }
        }

        return img;
    }

    @Override
    public int numFeatures() {
        return numColumns * numRows * colors.numColors();
    }

    @Override
    public int historyLength() {
        return 1;
    }

    public static MaxColorFeatureMap dynamicLoad(ParameterHolder params, FeatureColors colors) {
        return new MaxColorFeatureMap(params, colors);
    }
}
