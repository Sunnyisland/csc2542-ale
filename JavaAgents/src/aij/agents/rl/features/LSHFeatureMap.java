/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package aij.agents.rl.features;

import aij.parameters.LSHParameters;
import atariagents.io.Actions;
import nips11.agents.rlfeatures.*;
import atariagents.screen.ScreenMatrix;
import features.lsh.LSHMap;
import features.parameters.FeatureParameters;
import java.awt.image.BufferedImage;
import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class LSHFeatureMap extends RLFeatureMap implements Displayable {
    protected int historyLength;
    protected boolean encodeLastAction;
    
    protected int screenWidth;
    protected int screenHeight;

    protected LSHMap lsh;

    protected FeatureColors colors;

    protected boolean[] colorBits;
    protected int[] bitList;
    protected int bitListSize = 0;

    protected FeatureList features;

    public static void addParameters(ParameterHolder params) {
        LSHParameters.addParameters(params);
    }
    
    public LSHFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params);

        this.colors = colors;
        
        FeatureParameters.parseParameters(params);
        historyLength = FeatureParameters.historyLength;
        encodeLastAction = FeatureParameters.encodeLastAction;

        lsh = new LSHMap(params);
    }

    protected int numPixelsInScreenVector() {
        return screenWidth * screenHeight * historyLength;
    }

    /** Returns the size of the unhashed screen bit vector */
    protected int screenVectorSize() {
        return colors.numColors() * numPixelsInScreenVector();
    }

    protected void initDataStructures(FrameHistory history) {
        ScreenMatrix screen = history.getLastFrame(0);

        screenWidth = screen.width;
        screenHeight = screen.height;

        bitList = new int[numPixelsInScreenVector()];

        int vecSize = screenVectorSize();

        colorBits = new boolean[vecSize];

        lsh.initLSHStructures(vecSize);
    }

    /** Returns a quantization of the last screen. See Yavar Naddaf's thesis.
      */
    @Override
    public FeatureList getFeatures(FrameHistory history) {
        if (bitList == null) initDataStructures(history);

        binarizeHistory(history);
        features = lsh.map(colorBits, bitList, bitListSize);

        if (encodeLastAction)
            encodeByAction(history.getLastAction(0));

        cleanup();

        return features;
    }

    /** Binarizes the screen, using numColors bits per pixel.
     * 
     */
    public void binarizeHistory(FrameHistory history) {
        // Go through first screen; set one bit per pixel, corr. to its color
        int bitListIndex = 0;

        int numColors = colors.numColors();
        int pixelBlockSize = numColors;

        int offset = 0;
        
        for (int t = 0; t < history.maxHistoryLength(); t++) {
            ScreenMatrix screen = history.getLastFrame(t);

            // Everything should be fine, provided we do not use bitList
            if (screen == null) {
                break;
            }
            
            for (int x = 0; x < screenWidth; x++) {
                for (int y = 0; y < screenHeight; y++) {
                    int pixelEncoding = colors.encode(screen.matrix[x][y]);

                    // We encode this pixel using unary encoding
                    int featureIndex = pixelEncoding + offset;

                    // Encode the pixel doubly: as a bit vector and as a list
                    colorBits[featureIndex] = true;

                    bitList[bitListIndex++] = featureIndex;

                    offset += pixelBlockSize;
                }
            }

            bitListSize = bitListIndex;
        }
    }

    /** We block-encode the last action */
    protected void encodeByAction(int lastAction) {
        if (lastAction == -1) // First frame, assume noop
            lastAction = 0;
        
        int blockSize = numFeaturesPerAction();
        int offset = blockSize * lastAction;

        // Bump all actions up to the correct block
        for (FeatureList.Pair p : features.active) {
            p.index += offset;
        }
    }

    /** Cleans up the data structures used in computing features
     * 
     */
    public void cleanup() {
        // Unset the bits in color bits; don't delete the array list - we will reuse
        for (int i = 0; i < bitListSize; i++) {
            colorBits[bitList[i]] = false;
        }
    }

    public int numDisplayModes() {
        return 0;
    }

    public BufferedImage getDisplay(ScreenMatrix screen, int mode) {
        switch (mode) {
            default: throw new IllegalArgumentException("Invalid display mode.");
        }
    }

    public int numActiveFeatures() {
        return lsh.numActiveFeatures();
    }

    public int numFeaturesPerAction() {
        return lsh.numFeatures();
    }

    @Override
    public int numFeatures() {
        return numFeaturesPerAction() * (encodeLastAction ? Actions.numPlayerActions : 1);
    }

    @Override
    public int historyLength() {
        return historyLength;
    }

    public static LSHFeatureMap dynamicLoad(ParameterHolder params, FeatureColors colors) {
        return new LSHFeatureMap(params, colors);
    }
}
