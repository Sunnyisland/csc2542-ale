/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.awt.Color;

/** Palette used by ALE to display region matrices.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class CustomALEPalette extends ColorPalette {
    public CustomALEPalette() {
        super();

        super.set(new Color(0, 0, 0), 0);
        
        // Set the palette by shuffling the NTSC palette around
        for (int index = 1; index < MAX_ENTRIES; index++) {
            if (index == 128) {
                super.set(new Color(0, 0, 0), index);
                continue;
            }
            int sevenBits = (index & 0x7F) - 1;

            int color = (sevenBits & 0xF) << 4;
            int highbits = ((sevenBits & ~0xF) >> 4) + 1;
            int shade = (16 - (highbits << 1));

            int ntscIndex = color + shade;

            int v = NTSCPalette.colorData[ntscIndex & ~0x1];
            int r = (v & 0xFF0000) >> 16;
            int g = (v & 0x00FF00) >> 8;
            int b = v & 0x0000FF;

            super.set(new Color(r, g, b), index);
        }
    }

}
