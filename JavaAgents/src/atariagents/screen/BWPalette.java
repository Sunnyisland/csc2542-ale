/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.awt.Color;

/**
 *
 * @author marc
 */
public class BWPalette extends ColorPalette {
    public BWPalette() {
        super();

        super.set(Color.BLACK, 0);
        super.set(Color.WHITE, 1);
    }
}
