/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.awt.Color;

/**
 *
 * @author marc
 */
public class PALPalette extends ColorPalette {
    // 104 unique colors, with odd indices being mapped to the color below

    protected int[] colorData = new int[]{
        0x000000, 0, 0x2b2b2b, 0, 0x525252, 0, 0x767676, 0,
        0x979797, 0, 0xb6b6b6, 0, 0xd2d2d2, 0, 0xececec, 0,
        0x000000, 0, 0x2b2b2b, 0, 0x525252, 0, 0x767676, 0,
        0x979797, 0, 0xb6b6b6, 0, 0xd2d2d2, 0, 0xececec, 0,
        0x805800, 0, 0x96711a, 0, 0xab8732, 0, 0xbe9c48, 0,
        0xcfaf5c, 0, 0xdfc06f, 0, 0xeed180, 0, 0xfce090, 0,
        0x445c00, 0, 0x5e791a, 0, 0x769332, 0, 0x8cac48, 0,
        0xa0c25c, 0, 0xb3d76f, 0, 0xc4ea80, 0, 0xd4fc90, 0,
        0x703400, 0, 0x89511a, 0, 0xa06b32, 0, 0xb68448, 0,
        0xc99a5c, 0, 0xdcaf6f, 0, 0xecc280, 0, 0xfcd490, 0,
        0x006414, 0, 0x1a8035, 0, 0x329852, 0, 0x48b06e, 0,
        0x5cc587, 0, 0x6fd99e, 0, 0x80ebb4, 0, 0x90fcc8, 0,
        0x700014, 0, 0x891a35, 0, 0xa03252, 0, 0xb6486e, 0,
        0xc95c87, 0, 0xdc6f9e, 0, 0xec80b4, 0, 0xfc90c8, 0,
        0x005c5c, 0, 0x1a7676, 0, 0x328e8e, 0, 0x48a4a4, 0,
        0x5cb8b8, 0, 0x6fcbcb, 0, 0x80dcdc, 0, 0x90ecec, 0,
        0x70005c, 0, 0x841a74, 0, 0x963289, 0, 0xa8489e, 0,
        0xb75cb0, 0, 0xc66fc1, 0, 0xd380d1, 0, 0xe090e0, 0,
        0x003c70, 0, 0x195a89, 0, 0x2f75a0, 0, 0x448eb6, 0,
        0x57a5c9, 0, 0x68badc, 0, 0x79ceec, 0, 0x88e0fc, 0,
        0x580070, 0, 0x6e1a89, 0, 0x8332a0, 0, 0x9648b6, 0,
        0xa75cc9, 0, 0xb76fdc, 0, 0xc680ec, 0, 0xd490fc, 0,
        0x002070, 0, 0x193f89, 0, 0x2f5aa0, 0, 0x4474b6, 0,
        0x578bc9, 0, 0x68a1dc, 0, 0x79b5ec, 0, 0x88c8fc, 0,
        0x340080, 0, 0x4a1a96, 0, 0x5f32ab, 0, 0x7248be, 0,
        0x835ccf, 0, 0x936fdf, 0, 0xa280ee, 0, 0xb090fc, 0,
        0x000088, 0, 0x1a1a9d, 0, 0x3232b0, 0, 0x4848c2, 0,
        0x5c5cd2, 0, 0x6f6fe1, 0, 0x8080ef, 0, 0x9090fc, 0,
        0x000000, 0, 0x2b2b2b, 0, 0x525252, 0, 0x767676, 0,
        0x979797, 0, 0xb6b6b6, 0, 0xd2d2d2, 0, 0xececec, 0,
        0x000000, 0, 0x2b2b2b, 0, 0x525252, 0, 0x767676, 0,
        0x979797, 0, 0xb6b6b6, 0, 0xd2d2d2, 0, 0xececec, 0
    };

    public PALPalette() {
        super();

        // Set the palette as given above
        for (int index = 0; index < colorData.length; index++) {
            int v = colorData[index & ~0x1];
            int r = (v & 0xFF0000) >> 16;
            int g = (v & 0x00FF00) >> 8;
            int b = v & 0x0000FF;

            super.set(new Color(r, g, b), index);
        }
    }
}
