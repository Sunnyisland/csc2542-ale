/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.awt.Color;

/**
 *
 * @author marc
 */
public class GrayscalePalette extends ColorPalette {
    public GrayscalePalette() {
        super();

        // Grayscale from (0,0,0) to (255,255,255)
        for (int i = 0; i < 256; i++) {
            super.set(new Color(i, i, i), i);
        }
    }
}
