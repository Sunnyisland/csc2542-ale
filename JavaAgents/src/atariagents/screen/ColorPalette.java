/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.awt.Color;

/** Defines a palette of colors. Up to 256 entries. 0 is always black.
 *
 * @author Marc G. Bellemare
 */
public class ColorPalette {
    public static final int MAX_ENTRIES = 256;
    
    protected Color[] map;
    protected int numEntries;
    
    public ColorPalette() {
        map = new Color[MAX_ENTRIES];
        // 0 is always black
        set(Color.BLACK, 0);
    }

    public int numEntries() {
        return this.numEntries;
    }

    /** Adds Color c at index i.
     *
     * @param c
     */
    public Color set(Color c, int i) {
        Color oldColor = map[i];

        map[i] = c;
        if (oldColor == null) numEntries++;

        return oldColor;
    }

    /** Returns the color indexed by i, possibly null.
     * 
     * @param i
     * @return
     */
    public Color get(int i) {
        return map[i];
    }
    
    /** Returns whether palette index i has an associated color.
     * 
     * @param i
     * @return
     */
    public boolean hasEntry(int i) {
        return (map[i] != null);
    }
    
    /** Sets the colors of entry i at random.
     *
     * @param i The target entry index.
     * @return
     */
    public Color setRandom(int i) {
        Color oldColor = map[i];
        int colorRes = 256;

        // Generate the three channels at random
        int r = (int)(Math.random() * colorRes);
        int g = (int)(Math.random() * colorRes);
        int b = (int)(Math.random() * colorRes);
        
        Color c = new Color(r, g, b);

        if (oldColor == null) numEntries++;
        map[i] = c;

        return oldColor;
    }
}
