/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.awt.Color;
import java.awt.image.BufferedImage;

/** Converts a ScreenMatrix to a BufferedImage, using a ColorMap.
 *
 * @author marc
 */
public class ScreenConverter {
    public static final int RANDOM_PALETTE = 0;
    public static final int NTSC_PALETTE   = 1;
    public static final int PAL_PALETTE    = 2;
    public static final int SECAM_PALETTE  = 3;

    protected ColorPalette colorMap;

    public ScreenConverter(ColorPalette cMap) {
        colorMap = cMap;
    }

    public ScreenConverter() {
        this(RANDOM_PALETTE);
    }

    public ScreenConverter(int paletteType) {
        colorMap = makePalette(paletteType);
    }

    public ColorPalette getPalette() {
        return colorMap;
    }

    public ColorPalette makePalette(int paletteType) {
    switch (paletteType) {
        case RANDOM_PALETTE:
            return new ColorPalette();
        case NTSC_PALETTE:
            return new NTSCPalette();
        case PAL_PALETTE:
            return new PALPalette();
        case SECAM_PALETTE:
            return new SECAMPalette();
        default:
            throw new IllegalArgumentException("Invalid palette type: "+paletteType);
        }
    }

    /** Updates the color map by creating new random colors for unmapped 
     *   entries.
     * 
     * @param m
     */
    public void updateColorMap(ScreenMatrix m) {
        for (int i = 0; i < m.width; i++)
            for (int j = 0; j < m.height; j++) {
                int v = m.matrix[i][j];

                if (!colorMap.hasEntry(v)) {
                    colorMap.setRandom(v);
                }
            }
    }

    /** Transforms a ScreenMatrix into a BufferedImage.
     * 
     * @param m
     * @return
     */
    public BufferedImage convert(ScreenMatrix m) {
        // Create a new image
        BufferedImage img = new BufferedImage(m.width, m.height, BufferedImage.TYPE_INT_RGB);

        // Map each pixel
        for (int x = 0; x < m.width; x++)
            for (int y = 0; y < m.height; y++) {
                int index = m.matrix[x][y];
                Color c = colorMap.get(index);
                img.setRGB(x, y, c.getRGB());
            }

        return img;
    }
}
