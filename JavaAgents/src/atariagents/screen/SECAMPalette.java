/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.awt.Color;

/**
 *
 * @author marc
 */
public class SECAMPalette extends ColorPalette {
    // 8 unique colors, with odd indices being mapped to the color below

    protected int[] colorData = new int[]{
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0,
        0x000000, 0, 0x2121ff, 0, 0xf03c79, 0, 0xff50ff, 0,
        0x7fff00, 0, 0x7fffff, 0, 0xffff3f, 0, 0xffffff, 0
    };

    public SECAMPalette() {
        super();

        // Set the palette as given above
        for (int index = 0; index < colorData.length; index++) {
            int v = colorData[index & ~0x1];
            int r = (v & 0xFF0000) >> 16;
            int g = (v & 0x00FF00) >> 8;
            int b = v & 0x0000FF;

            super.set(new Color(r, g, b), index);
        }
    }
}
