/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.screen;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 *
 * @author marc
 */
public class ScreenMatrix implements Cloneable {
    public int[][] matrix;
    public int width;
    public int height;

    public ScreenMatrix(int w, int h) {
        matrix = new int[w][h];
        width = w;
        height = h;
    }

    /** Load a screen from a text file, in ALE format. The first line contains
     *   <width>,<height> .
     *  Each subsequent line (210 of them) contains a screen row with comma-separated
     *   values.
     * 
     * @param filename
     */
    public ScreenMatrix(String filename) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

        String line = in.readLine();
        String[] tokens = line.split(",");

        width = Integer.parseInt(tokens[0]);
        height = Integer.parseInt(tokens[1]);

        this.matrix = new int[width][height];

        int rowIndex = 0;

        // Read in each row
        while ((line = in.readLine()) != null) {
            tokens = line.split(",");
            assert (tokens.length == width);

            for (int x = 0; x < tokens.length; x++) {
                this.matrix[x][rowIndex] = Integer.parseInt(tokens[x]);
            }

            rowIndex++;
        }
    }

    /** Saves this screen matrix as a text file. Can then be loaded using the
     *   relevant constructor.
     * 
     * @param filename
     * @throws IOException
     */
    public void saveData(String filename) throws IOException {
        PrintStream out = new PrintStream(new FileOutputStream(filename));

        // Width,height\n
        out.println(width+","+height);

        // Print the matrix, one row per line
        for (int y = 0; y < height; y++) {
            // Data is comma separated
            for (int x = 0; x < width; x++) {
                out.print(matrix[x][y]);
                if (x < width - 1) out.print(",");
            }

            out.println();
        }
    }


    public Object clone() {
        try {
            ScreenMatrix img = (ScreenMatrix)super.clone();

            img.matrix = new int[this.width][this.height];
        
            for (int x = 0; x < this.width; x++) {
                System.arraycopy(this.matrix[x], 0, img.matrix[x], 0, this.height);
            }
            return img;
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
