/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.movie;

import atariagents.gui.ImageSequence;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import javax.imageio.ImageIO;
import nips11.parameters.MovieParameters;
import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class MovieGenerator {
    /** How many times to show the same image sequence before moving on to the next */
    protected int cyclesPerImage;
    protected String baseFilename;

    protected int pngIndex = 0;

    protected final int indexDigits = 6;

    public static void addParameters(ParameterHolder params) {
        MovieParameters.addParameters(params);
    }
    
    public MovieGenerator(ParameterHolder params) {
        MovieParameters.parseParameters(params);
        
        this.cyclesPerImage = MovieParameters.cyclesPerImage;
        this.baseFilename = MovieParameters.baseFilename;
    }

    public void record(ImageSequence left, ImageSequence right) {
        if (right == null) {
            record(left);
            return;
        }

        if (baseFilename == null)
            throw new IllegalArgumentException("Base filename is not defined.");
        
        if (left.numImages() != right.numImages())
            throw new UnsupportedOperationException("Cannot generate movie: different number of images.");

        int width = left.getWidth();
        int height = left.getHeight();

        int numImages = left.numImages();

        ImageSequence combinedSequence = new ImageSequence(0);
        
        for (int i = 0; i < numImages; i++) {
            // Make a single image out of the two
            BufferedImage combinedImage = new BufferedImage(width*2, height, BufferedImage.TYPE_INT_RGB);
            BufferedImage leftImage = left.get(i);
            BufferedImage rightImage = right.get(i);

            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++) {
                    combinedImage.setRGB(x, y, leftImage.getRGB(x, y));
                    combinedImage.setRGB(x+width, y, rightImage.getRGB(x, y));
                }

            combinedSequence.add(combinedImage);
        }

        // Create a formatter to generate 6-digit indices
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setMinimumIntegerDigits(indexDigits);
        formatter.setGroupingUsed(false);

        for (int c = 0; c < cyclesPerImage; c++) {
            for (int n = 0; n < combinedSequence.numImages(); n++) {
                String indexString = formatter.format(pngIndex);

                String filename = baseFilename + indexString + ".png";

                try {
                    ImageIO.write(combinedSequence.get(n), "png", new File(filename));
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }

                pngIndex++;
            }
        }
    }

    public void record(ImageSequence left) {
        record(left, false);
    }
    
    public void record(ImageSequence left, boolean scale) {
        if (baseFilename == null)
            throw new IllegalArgumentException("Base filename is not defined.");

        int width = left.getWidth();
        int height = left.getHeight();

        int numImages = left.numImages();

        // Create a formatter to generate 6-digit indices
        NumberFormat formatter = NumberFormat.getInstance();
        formatter.setMinimumIntegerDigits(indexDigits);
        formatter.setGroupingUsed(false);

        for (int c = 0; c < cyclesPerImage; c++) {
            for (int n = 0; n < numImages; n++) {
                String indexString = formatter.format(pngIndex);

                String filename = baseFilename + indexString + ".png";

                BufferedImage rawImage = left.get(n);
                BufferedImage savedImage;

                // Rescale to the actual displayed image
                if (scale) {
                    savedImage = new BufferedImage(rawImage.getWidth() * 2,
                            rawImage.getHeight(), BufferedImage.TYPE_INT_RGB);
                    for (int x = 0, ox = 0; x < rawImage.getWidth(); x++, ox += 2)
                        for (int y = 0; y < rawImage.getHeight(); y++) {
                            int rgb = rawImage.getRGB(x, y);
                            savedImage.setRGB(ox, y, rgb);
                            savedImage.setRGB(ox+1, y, rgb);
                        }
                }
                else
                    savedImage = rawImage;

                try {
                    ImageIO.write(savedImage, "png", new File(filename));
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }

                pngIndex++;
            }
        }
    }
}
