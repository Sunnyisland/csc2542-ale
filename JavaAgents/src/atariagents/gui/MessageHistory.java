/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import java.util.LinkedList;
import java.util.List;

/** Encapsulates a list of messages. Each message is timestamped.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class MessageHistory {
    public class Message {
        protected String text;
        protected long timeStamp;

        public Message(String text, long timeStamp) {
            this.text = text;
            this.timeStamp = timeStamp;
        }

        public String getText() { return text; }
        public long getTimeStamp() { return timeStamp; }
    }

    /** A list of messages, with the first element being the oldest */
    protected LinkedList<Message> messages;

    public MessageHistory() {
        messages = new LinkedList<Message>();
    }
    
    /** Adds a message to our history. The time at which the message was added
     *   is also recorded.
     *
     * @param text The message to be added
     */
    public void addMessage(String text) {
        long currentTime = System.currentTimeMillis();

        messages.addLast(new Message(text, currentTime));
    }

    /** Returns a list of current messages */
    public List<Message> getMessages() {
        return messages;
    }

    /** Remove any message which is older than 'maxAge'. The age of a message is
     *   found by comparing its timestamp with the current time.
     *
     * @param maxAge The maximum age, in milliseconds, of a message.
     */
    public void update(long maxAge) {
        long currentTime = System.currentTimeMillis();

        while (!messages.isEmpty()) {
            Message m = messages.getFirst();

            // Delete this message if it is old enough
            long age = currentTime - m.timeStamp;
            if (age > maxAge)
                messages.removeFirst();
            else
                break; // Messages are ordered by timestamp so we can stop
        }
    }
}
