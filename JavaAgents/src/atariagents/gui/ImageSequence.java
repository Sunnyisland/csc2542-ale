/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/** Encapsulates a sequence of images, which is iterated at regular intervals.
 *
 * @author marc
 */
public class ImageSequence {
    protected ArrayList<BufferedImage> sequence;
    protected long frameDelay;
    protected long startTime;
    protected int currentIndex;
    
    public ImageSequence(BufferedImage img) {
        this(0);
        sequence.add(img);
    }
    
    public ImageSequence(long delay) {
        sequence = new ArrayList<BufferedImage>();

        frameDelay = delay;
    }

    public ArrayList<BufferedImage> sequence() {
        return sequence;
    }

    public BufferedImage get(int i) {
        return sequence.get(i);
    }
    
    public int getWidth() {
        return (sequence.isEmpty()? 0 : sequence.get(0).getWidth());
    }

    public int getHeight() {
        return (sequence.isEmpty()? 0 : sequence.get(0).getHeight());
    }

    public void add (BufferedImage img) {
        sequence.add(img);
    }

    public int numImages() {
        return sequence.size();
    }

    public BufferedImage currentImage() {
        if (sequence.isEmpty()) return null;

        // Rotate images as necessary
        if (frameDelay > 0) {
            long time = System.currentTimeMillis();

            if (startTime == 0) {
                startTime = time;
                return sequence.get(0);
            } else if (time - startTime >= frameDelay) {
                currentIndex++;
                if (currentIndex >= sequence.size()) {
                    currentIndex = 0;
                }

                startTime = time;
            }
        }

        return sequence.get(currentIndex);
    }
}
