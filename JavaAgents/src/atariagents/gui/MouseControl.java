/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class MouseControl implements MouseListener {
    public int clickX, clickY;
    public boolean leftClick;

    public void mouseClicked(MouseEvent me) {
        if (me.getButton() == me.BUTTON1) {
            leftClick = true;
        }

        clickX = me.getX();
        clickY = me.getY();
    }

    public int[] getLeftClick() {
        if (!leftClick) return null;
        else {
            leftClick = false;
            return new int[] {clickX, clickY};
        }
    }

    public void mousePressed(MouseEvent me) {
    }

    public void mouseReleased(MouseEvent me) {
    }

    public void mouseEntered(MouseEvent me) {
    }

    public void mouseExited(MouseEvent me) {
    }

}
