/* Copyright 2009 Michael Sokolsky and Thomas Degris
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package atariagents.gui;

import java.awt.image.BufferedImage;
import javax.swing.JFrame;

public final class AgentGUI extends JFrame implements AbstractUI {
    protected final ScreenDisplay panel;
    protected final KeyboardControl keyboard;
    protected final MouseControl mouse;
    
    /** How often to refresh the GUI, in millis */
    protected final long refreshRate = 50;
    protected final GUIRefresher refresher;
    
    public AgentGUI(){
        keyboard = new KeyboardControl();
        mouse = new MouseControl();
        
        panel = new ScreenDisplay(keyboard);
        add(panel);

        this.addKeyListener(keyboard);
        this.addMouseListener(mouse);
        this.setSize(panel.getPreferredSize());

        pack();
        setLocationRelativeTo(null);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        if (refreshRate > 0) {
            refresher = new GUIRefresher(this, refreshRate);
            refresher.start();
        }
        else
            refresher = null;
    }

    public void die() {
        this.dispose();

        if (refresher != null)
            refresher.die();
    }

    public void setLeftImage(ImageSequence img) {
        synchronized(panel) {
            panel.setLeftImage(img);
        }
    }

    public void setRightImage(ImageSequence img) {
        synchronized(panel) {
            panel.setRightImage(img);
        }
    }

    public ImageSequence getLeftImage() {
        synchronized(panel) {
            return panel.getLeftImage();
        }
    }

    public ImageSequence getRightImage() {
        synchronized(panel) {
            return panel.getRightImage();
        }
    }

    public void setCenterString(String s) {
        panel.setCenterString(s);
    }

    public void setRightString(String s) {
        panel.setRightString(s);
    }

    public void addMessage(String s) {
        panel.addMessage(s);
    }

    public int getKeyboardAction() {
        return keyboard.toALEAction();
    }

    public void updateFrameCount() {
        panel.updateFrameCount();
    }

    /** Return whether we should change the left display mode */
    public boolean getLeftDisplayChange() {
        return keyboard.getLeftDisplayChange();
    }

    public boolean getRightDisplayChange() {
        return keyboard.getRightDisplayChange();
    }

    public boolean getSaveWeights() {
        return keyboard.getSaveWeights();
    }

    public boolean getLoadWeights() {
        return keyboard.getLoadWeights();
    }

    public boolean getPauseSimulation() {
        return keyboard.getPauseSimulation();
    }

    public boolean getToggleEvaluation() {
        return keyboard.getToggleEvaluation();
    }
    
    public int deltaFPS() {
        return ((keyboard.getIncreaseFPS()? 1 : 0) + (keyboard.getDecreaseFPS()? -1 : 0));
    }

    public int[] getLeftClick() {
        int[] pt = mouse.getLeftClick();
        if (pt != null)
            return panel.pixelAt(pt);
        else
            return null;
    }

    public void refresh() {
        this.repaint();
    }
}
