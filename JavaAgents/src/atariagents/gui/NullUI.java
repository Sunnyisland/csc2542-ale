/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import java.awt.image.BufferedImage;

/** An empty UI for running console experiments.
 *
 * @author marc
 */
public class NullUI implements AbstractUI {
    /** For computing FPS */
    protected int frameCount;
    protected double fps;
    protected double fpsAlpha = 0.9;
    protected int updateRate = 1;
    protected long time;
    
    public void die() {
    }

    public void setLeftImage(ImageSequence img) {
    }

    public void setRightImage(ImageSequence img) {
    }

    public void setCenterString(String s) {
    }

    public void setRightString(String s) {
    }

    public void addMessage(String s) {
    }
    
    public int getKeyboardAction() {
        return 0;
    }

    public int[] getLeftClick() {
        return null;
    }

    public void updateFrameCount() {
        if (time == 0) {
            time = System.currentTimeMillis();
        }

        frameCount++;

        long deltaTime = System.currentTimeMillis() - time;
        if (deltaTime > (1000) / updateRate) {
            if (fps == 0) {
                fps = frameCount;
            } else {
                // Compute the exact number of (fractional) ticks since FPS update
                double ticksSinceUpdate = deltaTime * updateRate / 1000.0;
                double alpha = Math.pow(fpsAlpha, ticksSinceUpdate);

                fps = alpha * fps + (1 - alpha) * (frameCount * updateRate / ticksSinceUpdate);
            }

            frameCount = 0;
            time = System.currentTimeMillis();
        }
    }

    public boolean getLeftDisplayChange() {
        return false;
    }

    public boolean getRightDisplayChange() {
        return false;
    }

    public boolean getSaveWeights() {
        return false;
    }

    public boolean getLoadWeights() {
        return false;
    }

    public boolean getPauseSimulation() {
        return false;
    }

    public boolean getToggleEvaluation() {
        return false;
    }
    
    public int deltaFPS() {
        return 0;
    }

    public void refresh() {
    }
}
