/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.lang.Math;
import javax.swing.JPanel;

/**
 *
 * @author marc
 */
public class ScreenDisplay extends JPanel {
    final KeyboardControl keyboard;

    ImageSequence leftImage;
    ImageSequence rightImage;
    
    int scaleFactor = 3;
    int defaultWidth = 160;
    int defaultHeight = 210;
    int statusBarHeight = 20;

    int statusBarY;
    int windowWidth;
    int windowHeight;
    
    int userImageOffset = 0;
    
    int frameCount = 0;
    double fps = 0;
    long frameTime = 0;
    int updateRate = 5; // How often to update FPS, in hertz
    double fpsAlpha = 0.9;

    boolean userDisplay = true;

    /** Additional user strings to be displayed */
    String centerString;
    String rightString;

    MessageHistory messages;

    long maxMessageAge = 3000;

    public ScreenDisplay(KeyboardControl keyboard) {
        super();

        messages = new MessageHistory();

        this.keyboard = keyboard;
    }

    public Dimension getPreferredSize() {
        int width, height;

        if (leftImage == null) {
            statusBarY = defaultHeight * scaleFactor;
            width = defaultWidth * scaleFactor;
            height = statusBarY + statusBarHeight;
        }
        else {
            width = leftImage.getWidth() * scaleFactor;
            statusBarY = leftImage.getHeight() * scaleFactor;
            height = statusBarY + statusBarHeight;
        }

        if (userDisplay) {
            width *= 2;
        }

        windowWidth = width;
        windowHeight = height;

        return new Dimension(width, height);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawImages(g);
    }

    public void setLeftImage(ImageSequence img) {
        synchronized (this) {
            this.leftImage = img;
        }
    }

    public void setRightImage(ImageSequence img) {
        synchronized (this) {
            userDisplay = true;

            this.rightImage = img;

            userImageOffset = defaultWidth;
        }
    }

    public ImageSequence getLeftImage() {
        return leftImage;
    }

    public ImageSequence getRightImage() {
        return rightImage;
    }
    
    public void setCenterString(String s) {
        synchronized (this) {
            centerString = s;
        }
    }
    
    public void setRightString(String s) {
        synchronized (this) {
            rightString = s;
        }
    }

    public void addMessage(String s) {
        synchronized (this) {
            messages.addMessage(s);
        }
    }

    public void updateFrameCount() {
        synchronized (this) {
            frameCount++;
            long time = System.currentTimeMillis();

            // If one second has elapsed, update FPS
            if (time - frameTime >= 1000 / updateRate) {
                if (fps == 0) {
                    fps = frameCount;
                } else {
                    // Compute the exact number of (fractional) ticks since FPS update
                    double ticksSinceUpdate = (time - frameTime) * updateRate / 1000.0;
                    double alpha = Math.pow(fpsAlpha, ticksSinceUpdate);

                    fps = alpha * fps + (1 - alpha) * (frameCount * updateRate / ticksSinceUpdate);
                }

                frameCount = 0;
                frameTime = time;
            }
        }
  }

  private void rescale(Graphics g, double xfactor, double yfactor) {
      if (g instanceof Graphics2D) {
          Graphics2D g2d = (Graphics2D) g;
          g2d.scale(xfactor, yfactor);
      }
  }

  /** Converts a mouse location to a pixel,image location */
  public int[] pixelAt(int[] location) {
      int[] pixel = new int[3];

      // Translate to Graphics coordinate (terrible hack because I can't be bothered
      //  and I can't find the relevant Java item)
      location[0] -= 1;
      location[1] -= 23;
      
      boolean twoScreens = (rightImage != null);

      int xScale = scaleFactor * (twoScreens? 1 : 2);
      int yScale = scaleFactor;

      if (twoScreens) {
          int scaledWidth = defaultWidth * scaleFactor;
          int mx = location[0];

          // On second screen?
          if (mx >= scaledWidth) {
              mx -= scaledWidth;
              pixel[2] = 1;
          }
          else
              pixel[2] = 0;

          pixel[0] = mx / xScale;
      }
      else {
          pixel[0] = location[0] / xScale;
          pixel[2] = 0;
      }

      pixel[1] = location[1] / yScale;

      return pixel;
    }

  public void drawImages(Graphics g) {
      synchronized(this) {
          // Do some message cleanup if necessary
          messages.update(maxMessageAge);

          rescale(g, scaleFactor, scaleFactor);
          // draw the atari image
          if (leftImage != null) {
              // If we only have one image, draw it on the whole screen
              if (rightImage == null) {
                  rescale(g, 2.0, 1.0);
                  g.drawImage(leftImage.currentImage(), 0, 0, null);
                  rescale(g, 0.5, 1.0);
              }
              else
                  g.drawImage(leftImage.currentImage(), 0, 0, null);
          }
          if (rightImage != null)
              g.drawImage(rightImage.currentImage(), userImageOffset, 0, null);

          double invScale = 1.0/scaleFactor;
          rescale(g, invScale, invScale);

          int statusBarTextOffset = statusBarY + 15;

          // draw extra stuff
          if (fps > 0) {
              g.setColor(Color.BLACK);
              double roundedFPS = (Math.round(fps * 10) / 10.0);
              g.drawString("FPS: "+roundedFPS, 0, statusBarTextOffset);
          }

          // Draw a string center-bottom
          if (centerString != null) {
              int stringLength = g.getFontMetrics().stringWidth(centerString);
              g.drawString(centerString, (windowWidth - stringLength)/2, statusBarTextOffset);
          }
          
          // Draw a string in the bottom right corner
          if (rightString != null) {
              int stringLength = g.getFontMetrics().stringWidth(rightString);
              g.drawString(rightString, windowWidth - stringLength, statusBarTextOffset);
          }

            int textOffset = statusBarY - 4;

            g.setColor(Color.YELLOW);

            // Draw messages in the bottom right corner
            for (MessageHistory.Message m : messages.getMessages()) {
                // Draw one message
                String text = m.getText();
                int stringLength = g.getFontMetrics().stringWidth(text);
                g.drawString(text, windowWidth - stringLength - 2, textOffset);

                // Decrement textOffset so that the next (older) message
                //  is drawn on top of it
                textOffset -= g.getFontMetrics().getHeight();
            }
      }
  }

}
