/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import java.awt.image.BufferedImage;

/** An interface describing a UI. This gets subclassed into a graphical UI,
 *   a dummy UI, etc... as needed.
 *
 * @author marc
 */
public interface AbstractUI {
    public void die();
    public void refresh();

    public void setLeftImage(ImageSequence img);
    public void setRightImage(ImageSequence img);
    
    public void setCenterString(String s);
    public void setRightString(String s);
    public void addMessage(String s);

    public int getKeyboardAction();
    public int[] getLeftClick();
    
    public void updateFrameCount();

    public boolean getLeftDisplayChange();
    public boolean getRightDisplayChange();

    public boolean getSaveWeights();
    public boolean getLoadWeights();

    public boolean getPauseSimulation();
    public boolean getToggleEvaluation();
    
    public int deltaFPS();
}
