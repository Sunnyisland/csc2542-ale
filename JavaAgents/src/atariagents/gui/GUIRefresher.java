/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import java.util.logging.Level;
import java.util.logging.Logger;

/** A primitive class that forces a GUI refresh every so often.
 *
 * @author Marc G. Bellemare
 */
public class GUIRefresher extends Thread {
    protected AgentGUI gui;
    protected long refreshRate;
    protected boolean mustTerminate;

    public static final long minimumRefreshRate = 10;

    public GUIRefresher(AgentGUI gui, long refreshRate) {
        this.gui = gui;
        this.refreshRate = refreshRate;

        if (this.refreshRate < minimumRefreshRate) {
            System.err.println ("GUI refresh rate is lower than 10ms: setting to 10ms.");
            this.refreshRate = minimumRefreshRate;
        }
    }

    public void die() {
        mustTerminate = true;
    }

    public void run() {
        long delta = refreshRate;

        // Better would be to use a timer event
        while (!mustTerminate) {
            long startTime = System.currentTimeMillis();
            
            // Sleep
            if (delta > 0) {
                try {
                    Thread.sleep(delta);
                } catch (InterruptedException ex) {
                }
            }

            long elapsedTime = System.currentTimeMillis() - startTime;
            delta = refreshRate - (elapsedTime - refreshRate);
            if (delta <= 0) delta = 0;
            
            gui.refresh();
        }
    }
}
