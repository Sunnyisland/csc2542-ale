/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.gui;

import atariagents.io.Actions;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/** A crude keyboard controller.
 *
 * @author marc
 */
public class KeyboardControl implements KeyListener {

    public boolean up, down;
    public boolean left, right;
    public boolean fire;

    public boolean reset;

    /** These two flags are set to true by keypresses and set to false by
     *   the GUI. */
    public boolean leftDisplayChange;
    public boolean rightDisplayChange;

    public boolean increaseFPS;
    public boolean decreaseFPS;

    public boolean saveWeights;
    public boolean loadWeights;

    public boolean pauseSimulation;
    public boolean toggleEvaluate;
    
    public KeyboardControl() {
        up = down = left = right = fire = false;
        reset = false;
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
          case KeyEvent.VK_UP:
          case KeyEvent.VK_W:
            up = true;
            break;
          case KeyEvent.VK_DOWN:
          case KeyEvent.VK_S:
            down = true;
            break;
          case KeyEvent.VK_LEFT:
          case KeyEvent.VK_A:
            left = true;
            break;
          case KeyEvent.VK_RIGHT:
          case KeyEvent.VK_D:
            right = true;
            break;
          case KeyEvent.VK_SPACE:
            fire = true;
            break;
          case KeyEvent.VK_R:
            reset = true;
            break;
        }
    }

    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
          case KeyEvent.VK_UP:
          case KeyEvent.VK_W:
            up = false;
            break;
          case KeyEvent.VK_DOWN:
          case KeyEvent.VK_S:
            down = false;
            break;
          case KeyEvent.VK_LEFT:
          case KeyEvent.VK_A:
            left = false;
            break;
          case KeyEvent.VK_RIGHT:
          case KeyEvent.VK_D:
            right = false;
            break;
          case KeyEvent.VK_SPACE:
            fire = false;
            break;
          case KeyEvent.VK_R:
            reset = false;
            break;
            
            // Keys that are set on release
          case KeyEvent.VK_EQUALS:
            increaseFPS = true;
            break;
          case KeyEvent.VK_MINUS:
            decreaseFPS = true;
            break;
          case KeyEvent.VK_1:
            leftDisplayChange = true;
            break;
          case KeyEvent.VK_2:
            rightDisplayChange = true;
            break;
          case KeyEvent.VK_F1:
            saveWeights = true;
            break;
          case KeyEvent.VK_F2:
            loadWeights = true;
            break;
          case KeyEvent.VK_P:
            pauseSimulation = true;
            break;
          case KeyEvent.VK_E:
            toggleEvaluate = true;
            break;
        }
    }

    /** An array to map a bit-wise representation of the keypresses to ALE actions.
      * 1 = fire, 2 = up, 4 = right, 8 = left, 16 = down
      *
      * -1 indicate an invalid combination, e.g. left/right or up/down. These should
      * be filtered out in toALEAction.
      */
    private int[] bitKeysMap = new int[] {
        0, 1, 2, 10, 3, 11, 6, 14, 4, 12, 7, 15, -1, -1, -1, -1,
        5, 13, -1, -1, 8, 16, -1, -1, 9, 17, -1, -1, -1, -1, -1, -1
    };
    
    /** Converts the current keypresses to an ALE action (for player A).
     * 
     * @return
     */
    public int toALEAction() {
        int bitfield = 0;

        // Reset overrides everything
        if (reset) return Actions.map("reset");

        // Cancel out left/right, up/down, then map
        if (left == right) bitfield |= 0;
        else if (left) bitfield |= 0x08;
        else if (right) bitfield |= 0x04;

        if (up == down) bitfield |= 0;
        else if (up) bitfield |= 0x02;
        else if (down) bitfield |= 0x10;

        if (fire) bitfield |= 0x01;

        return bitKeysMap[bitfield];
    }

    public boolean getIncreaseFPS() {
        boolean r = increaseFPS;
        increaseFPS = false;
        return r;
    }

    public boolean getDecreaseFPS() {
        boolean r = decreaseFPS;
        decreaseFPS = false;
        return r;
    }

    public boolean getLeftDisplayChange() {
        boolean r = leftDisplayChange;
        leftDisplayChange = false;
        return r;
    }

    public boolean getRightDisplayChange() {
        boolean r = rightDisplayChange;
        rightDisplayChange = false;
        return r;
    }

    public boolean getSaveWeights() {
        boolean r = saveWeights;
        saveWeights = false;
        return r;
    }

    public boolean getLoadWeights() {
        boolean r = loadWeights;
        loadWeights = false;
        return r;
    }

    public boolean getPauseSimulation() {
        boolean r = pauseSimulation;
        pauseSimulation = false;
        return r;
    }

    public boolean getToggleEvaluation() {
        boolean r = toggleEvaluate;
        toggleEvaluate = false;
        return r;
    }
}
