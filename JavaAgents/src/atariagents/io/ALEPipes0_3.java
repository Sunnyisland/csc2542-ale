/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.io;

import java.io.IOException;

/**
 * Class that communicates with ALE via pipes. 0.3 protocol.
 * 
 * @author Marc G. Bellemare
 */
public class ALEPipes0_3 extends ALEPipes {
    public boolean useRLE;
    
    /** Uses stdin/stdout for communication */
    public ALEPipes0_3() throws IOException {
        super();

        useRLE = false;
    }

    /** Uses named pipes */
    public ALEPipes0_3(String pInfile, String pOutfile) throws IOException {
        super(pInfile, pOutfile);

        useRLE = false;
    }

    public void setUseRLE(boolean rle) {
        useRLE = rle;
    }
    
    @Override
    public void readScreenMatrix(String line) {
        if (useRLE)
            readScreenRLE(line);
        else
            readScreenFull(line);
    }

    /** Reads the screen matrix update from a string. The string only contains the
     *   pixels that differ from the previous frame. For ALE 0.3.
     *
     * @param line The screen part of the string sent by ALE.
     */
    public void readScreenFull(String line) {
        int ptr = 0;

        // 0.3 protocol - send everything
        for (int y = 0; y < screen.height; y++)
            for (int x = 0; x < screen.width; x++) {
                int ld = line.charAt(ptr+1);
                int hd = line.charAt(ptr);

                if (ld >= 'A') ld -= 'A' - 10;
                else ld -= '0';
                if (hd >= 'A') hd -= 'A' - 10;
                else hd -= '0';

                int v = (hd << 4) + ld;
                ptr += 2;

                screen.matrix[x][y] = v;
            }
    }

    /** Parses a hex byte in the given String, at position 'ptr'. */
    private int byteAt(String line, int ptr) {
        int ld = line.charAt(ptr+1);
        int hd = line.charAt(ptr);

        if (ld >= 'A') ld -= 'A' - 10;
        else ld -= '0';
        if (hd >= 'A') hd -= 'A' - 10;
        else hd -= '0';

        return (hd << 4) + ld;
    }

    /** Read in a run-length encoded screen. ALE 0.3 */
    public void readScreenRLE(String line) {
        int ptr = 0;

        // 0.3 protocol - send everything
        int y = 0;
        int x = 0;

        while (ptr < line.length()) {
            // Read in the next run
            int v = byteAt(line, ptr);
            int l = byteAt(line, ptr + 2);
            ptr += 4;

            for (int i = 0; i < l; i++) {
                screen.matrix[x][y] = v;
                if (++x >= screen.width) {
                    x = 0;
                    y++;

                    if (y >= screen.height && i < l - 1)
                        throw new RuntimeException ("Invalid run length data.");
                }
            }
        }
    }
}
