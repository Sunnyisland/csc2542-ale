/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.io;

/**
 *
 * @author marc
 */
public class ConsoleRAM implements Cloneable {
    public static final int RAM_SIZE = 128;

    public int[] ram;

    public ConsoleRAM() {
        ram = new int[RAM_SIZE];
    }

    public Object clone() {
        try {
            ConsoleRAM cram = (ConsoleRAM)super.clone();
            cram.ram = this.ram.clone();

            return cram;
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
