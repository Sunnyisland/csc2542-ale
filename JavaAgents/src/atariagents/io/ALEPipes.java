/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package atariagents.io;

import atariagents.screen.ScreenMatrix;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * Class that communicates with ALE via pipes.
 * 
 * @author Marc G. Bellemare
 */
public class ALEPipes {
    protected ScreenMatrix screen;
    protected ConsoleRAM ram;
    protected RLData rlData;
    protected boolean terminateRequested;
    
    protected BufferedReader in;
    protected PrintStream out;

    protected boolean updateScreen, updateRam, updateRLData, standardSize;
    protected int frameskip;

    protected int playerBAction = Actions.map("player_b_noop");

    protected boolean hasObserved;

    ScreenMatrix previousScreen;
    ScreenMatrix currentScreen;

    public final int standardWidth = 160;
    public final int standardHeight = 210;
    
    /** Uses stdin/stdout for communication */
    public ALEPipes() throws IOException {
        updateScreen = true;
        updateRam = false;
        updateRLData = true;
        frameskip = 0;

        in = new BufferedReader(new InputStreamReader(System.in));
        out = System.out;
    }

    /** Uses named pipes */
    public ALEPipes(String pInfile, String pOutfile) throws IOException {
        updateScreen = true;
        updateRam = false;
        updateRLData = true;
        frameskip = 0;

        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(pInfile)));
            out = new PrintStream(new FileOutputStream(pOutfile));
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    public void close() {
        try {
            in.close();
            out.close();
        }
        catch (IOException e) {
            // Not sure what to do if we can't close streams...
        }
    }
    
    public void setUpdateScreen(boolean updateScreen) {
        this.updateScreen = updateScreen;
    }

    public void setUpdateRam(boolean updateRam) {
        this.updateRam = updateRam;
    }

    public void setUpdateRL(boolean updateRL) {
        this.updateRLData = updateRL;
    }

    /** Asks the pipes to provide us with a standard 160x210 screen. This is done
     *    by tossing out rows below the 210th.
     *
     * @param standardSize
     */
    public void setStandardSize(boolean standardSize) {
        this.standardSize = standardSize;
    }
    
    /** A blocking method that sends initial information to ALE.
     * 
     */
    public void initPipes() throws IOException {
        // Read in the width and height of the screen
        String line = in.readLine();
        String[] tokens = line.split("-");
        int width = Integer.parseInt(tokens[0]);
        int height = Integer.parseInt(tokens[1]);

        if (width <= 0 || height <= 0) {
            throw new RuntimeException("Invalid width/height: "+width+"x"+height);
        }

        if (standardSize && (width != standardWidth || height != standardHeight)) {
            screen = new ScreenMatrix(standardWidth, standardHeight);
        }
        else
            // Create the screen matrix
            screen = new ScreenMatrix(width, height);

        ram = new ConsoleRAM();
        rlData = new RLData();

        // Now send back our preferences
        out.printf("%d,%d,%d,%d\n", updateScreen? 1:0, updateRam? 1:0, frameskip,
                updateRLData? 1:0);
        out.flush();
    }

    public int getFrameSkip() {
        return frameskip;
    }

    public void setFrameSkip(int frameskip) {
        this.frameskip = frameskip;
    }
    
    /** Returns the screen matrix from ALE.
     * 
     * @return
     */
    public ScreenMatrix getScreen() {
        return screen;
    }

    /** Returns the RAM from ALE.
     * 
     * @return
     */
    public ConsoleRAM getRAM() {
        return ram;
    }

    public RLData getRLData() {
        return rlData;
    }

    public boolean wantsTerminate() {
        return terminateRequested;
    }

    /** A blocking method which will get the next time step from ALE.
     *
     */
    public boolean observe() {
        if (hasObserved) {
            throw new RuntimeException("observe() called without subsequent act().");
        }
        else
            hasObserved = true;

        String line = null;

        // First read in a new line from ALE
        try {
            line = in.readLine();
            if (line == null) return true;
        }
        catch (IOException e) {
            return true;
        }

        // Catch the special keyword 'DIE'
        if (line.equals("DIE")) {
            terminateRequested = true;
            return false;
        }

        // Ignore blank lines, still send an action
        if (line.length() > 0) {
            String[] tokens = line.split(":");

            int tokenIndex = 0;

            // If necessary, first read the RAM data
            if (updateRam)
                readRam(tokens[tokenIndex++]);

            // Then update the screen
            if (updateScreen) {
                readScreenMatrix(tokens[tokenIndex++]);
            }

            if (updateRLData) {
                readRLData(tokens[tokenIndex++]);
            }
        }

        return false;
    }

    /** After a call to observe(), send back the necessary action.
     * 
     * @param act
     * @return
     */
    public boolean act(int act) {
        if (!hasObserved) {
            throw new RuntimeException("act() called before observe().");
        }
        else
            hasObserved = false;

        sendAction(act);

        return false;
    }

    /** Sends an action without looking at the current observation. Equivalent
     *   to calling observe() then act().
     *
     * @param act
     * @return
     */
    public boolean blindAct(int act) {
        if (observe() == true) return true;
        if (act(act) == true) return true;
        return false;
    }

    public void sendAction(int act) {
        // Send player A's action, as well as the NOOP for player B
        out.printf("%d,%d\n", act, 18);
        out.flush();
    }

    public void readRLData(String line) {
        String[] tokens = line.split(",");

        // Parse the terminal bit
        rlData.isTerminal = (Integer.parseInt(tokens[0]) == 1);
        rlData.reward = Integer.parseInt(tokens[1]);
    }

    /** Reads the console RAM from a string 
      * @param line The RAM-part of the string sent by ALE.
      */
    public void readRam(String line) {
        int offset = 0;

        // The RAM string is 128 bytes, each taking 3 characters
        for (int ptr = 0; ptr < ConsoleRAM.RAM_SIZE; ptr++) {
            int v = Integer.parseInt(line.substring(offset, offset + 2), 16);
            ram.ram[ptr] = v;
            
            offset += 2;
        }
    }

    /** Reads the screen matrix update from a string. The string only contains the
     *   pixels that differ from the previous frame.
     *
     * @param line The screen part of the string sent by ALE.
     */
    public void readScreenMatrix(String line) {
        // No pixels differed; we're done
        if (line.equals("NADA")) return;
        else {
            // For each pixel (6 characters), parse its location and value
            int length = line.length();
            int ptr = 0;

            char[] buffer = line.toCharArray();

            while (ptr < length) {
                int i = parseHex(buffer, ptr);
                int j = parseHex(buffer, ptr+2);
                int v = parseHex(buffer, ptr+4);
                /* int i = Integer.parseInt(line.substring(ptr, ptr+2), 16);
                int j = Integer.parseInt(line.substring(ptr+2, ptr+4), 16);
                int v = Integer.parseInt(line.substring(ptr+4, ptr+6), 16); */

                // Drop out of bounds points (in case we are using the standard screen size)
                if (i < screen.width && j < screen.height)
                    screen.matrix[i][j] = v;

                ptr += 6;
            }
        }
    }

    public int parseHex(char[] buffer, int ptr) {
        int ld = buffer[ptr + 1];
        int hd = buffer[ptr];

        if (ld >= 'A') {
            ld -= 'A' - 10;
        } else {
            ld -= '0';
        }
        if (hd >= 'A') {
            hd -= 'A' - 10;
        } else {
            hd -= '0';
        }

        return (hd << 4) + ld;
    }
}
