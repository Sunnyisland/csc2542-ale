/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package atariagents.io;

import java.util.HashMap;

/** A static container for Atari actions.
 *
 * @author marc
 */
public class Actions {

    public static final int numPlayerActions = 18;
    
    public static String[] actionNames = {
        "player_a_noop",        // 0
        "player_a_fire",        // 1
        "player_a_up",          // 2
        "player_a_right",       // 3
        "player_a_left",        // 4
        "player_a_down",        // 5
        "player_a_upright",     // 6
        "player_a_upleft",      // 7
        "player_a_downright",   // 8
        "player_a_downleft",    // 9
        "player_a_upfire",      // 10
        "player_a_rightfire",   // 11
        "player_a_leftfire",    // 12
        "player_a_downfire",    // 13
        "player_a_uprightfire", // 14
        "player_a_upleftfire",  // 15
        "player_a_downrightfire", // 16
        "player_a_downleftfire",  // 17
        "player_b_noop",
        "player_b_fire",
        "player_b_up",
        "player_b_right",
        "player_b_left",
        "player_b_down",
        "player_b_upright",
        "player_b_upleft",
        "player_b_downright",
        "player_b_downleft",
        "player_b_upfire",
        "player_b_rightfire",
        "player_b_leftfire",
        "player_b_downfire",
        "player_b_uprightfire",
        "player_b_upleftfire",
        "player_b_downrightfire",
        "player_b_downleftfire",
        "reset",
        "undefined",
        "random",
        // MGB additions
        "save_state",
        "load_state",
        "system_reset"
    };

    public static HashMap<String,Integer> actionsMap;

    public static int map(String actionName) {
        if (actionsMap == null) makeMap();

        return actionsMap.get(actionName).intValue();
    }

    public static void makeMap() {
        actionsMap = new HashMap<String,Integer>();
        
        for (int i = 0; i < actionNames.length; i++) {
            int v;

            if (i < numPlayerActions * 2) v = i;
            // Reset, undefined, random have special action #s (ranging 40-42)
            else {
                v = i + 4;
            }
            actionsMap.put(actionNames[i], new Integer(v));
        }
    }
}
