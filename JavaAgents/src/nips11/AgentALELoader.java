/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11;

import java.io.File;
import java.io.PrintStream;
import marcgb.tools.config.Parameters;
import marcgb.tools.loader.DynamicLoader;
import rlVizLib.general.ParameterHolder;

/** Loader to run one of the Java agents, as well as ALE.
 *
 * @author Marc G. Bellemare
 */
public class AgentALELoader {
    public static String PARAM_INTERFACE_CLASSNAME = "interface-classname";
    public static String PARAM_ALE_COMMAND_LINE = "ale-command-line";
    public static String PARAM_ALE_WORKING_DIR = "ale-working-dir";
    public static String PARAM_ALE_ROM_NAME = "ale-rom-name";
    public static String PARAM_ALE_MAX_FRAMES = "ale-max-frames";

    public static String PARAM_ALE_RLE = "ale-run-length-encoding";
    
    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_INTERFACE_CLASSNAME)) return;
        
        params.addStringParam(PARAM_INTERFACE_CLASSNAME, "nips11.agents.HumanAgent");

        params.addStringParam(PARAM_ALE_COMMAND_LINE, null);
        params.addStringParam(PARAM_ALE_WORKING_DIR, "../ale_0_2");
        params.addStringParam(PARAM_ALE_ROM_NAME, null);
        params.addIntegerParam(PARAM_ALE_MAX_FRAMES, 0);
        params.addBooleanParam(PARAM_ALE_RLE, false);
    }

    public static String makeCommandLine(ParameterHolder params) {
        String romName = params.getStringParam(PARAM_ALE_ROM_NAME);
        int maxFrames = params.getIntegerParam(PARAM_ALE_MAX_FRAMES);
        boolean useRLE = params.getBooleanParam(PARAM_ALE_RLE);

        String cmd = "./ale -game_controller fifo ";
        if (maxFrames > 0) cmd += "-max_num_frames_per_episode "+maxFrames+" ";
        cmd += "-run_length_encoding "+useRLE+" ";
        cmd += "roms/"+romName+".bin 2>/dev/null";

        return cmd;
    }

    public static void main(String[] args) throws Throwable {
        // @dbg
        // MemoryWatch.report("init");
        ParameterHolder params = new ParameterHolder();
        AgentALELoader.addParameters(params);

        String configFilename = null;
        boolean cpuThrottle = false;

        // Parse arguments
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("--cpu")) {
                cpuThrottle = true;
            }
        }
         // Find the config file name as the first argument that does not start with a -
        for (int i = 0; i < args.length; i++) {
            if (!args[i].startsWith("-")) {
                configFilename = args[i];
                break;
            }
        }

        if (configFilename != null) {
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }

        String mainClassname = params.getStringParam(PARAM_INTERFACE_CLASSNAME);

        String aleCommandLine = params.getStringParam(PARAM_ALE_COMMAND_LINE);
        if (aleCommandLine == null)
            aleCommandLine = makeCommandLine(params);

        String aleWorkingDir = params.getStringParam(PARAM_ALE_WORKING_DIR);

        if (aleCommandLine == null) {
            throw new IllegalArgumentException("Must provide ale-command-line or parameters.");
        }
        
        Process aleProcess =
                Runtime.getRuntime().exec(aleCommandLine, null, new File(aleWorkingDir));

        // Very confusing notation, but this should work
        System.setIn(aleProcess.getInputStream());
        System.setOut(new PrintStream(aleProcess.getOutputStream()));
        
        if (cpuThrottle) {
            CPUThrottle throttle = new CPUThrottle(Thread.currentThread());
            throttle.start();
        }

        DynamicLoader.invoke("main", mainClassname, new Object[]{args});
    }

}
