/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class NIPSParameters {
    /** Model parameters */
    public static final String PARAM_ALPHA = "model-alpha";

    /** Filenames */
    public static final String PARAM_WEIGHTS_FILE = "weights-filename";
    public static final String PARAM_TRAJECTORIES_FILE = "trajectories-filename";
    public static final String PARAM_VALUE_FUNCTION_FILE = "value-function-filename";
    public static final String PARAM_TRANSITION_MODEL_FILE = "transition-model-filename";
    public static final String PARAM_NAMED_PIPES_FILE = "named-pipes-basename";
    public static final String PARAM_OUTPUT_FILE = "output-file";

    /** Classnames */
    public static final String PARAM_FEATURE_MAP_CLASSNAME = "features-map";
    public static final String PARAM_FEATURE_COLORS_CLASSNAME = "feature-colors";
    public static final String PARAM_RL_FEATURE_COLORS_CLASSNAME = "rl-feature-colors";
    public static final String PARAM_RL_LEARNER_CLASSNAME = "rl-learner-classname";

    /** IO parameters */
    public static final String PARAM_ALE_PROTOCOL = "ale-protocol";

    /** Trajectories */
    public static final String PARAM_RANDOM_TRAJECTORY_RANGE = "random-trajectory-range";
    public static final String PARAM_TRAJECTORY_SAMPLING_INTERVAL = "trajectory-sampling-interval";
    public static final String PARAM_TRAJECTORY_EVALUATION_INTERVAL = "trajectory-evaluation-interval";
    public static final String PARAM_RANDOM_SAMPLE_LENGTH = "trajectory-random-sample-length";
    public static final String PARAM_TRAJECTORY_HEAD_IGNORE = "trajectory-head-ignore-length";
    public static final String PARAM_MAX_EXPERT_TRAJECTORY_LENGTH = "trajectory-max-expert-length";
    public static final String PARAM_MAXIMUM_SAMPLE_COUNT = "maximum-sample-count";
    public static final String PARAM_ALL_TRAJECTORIES_ONCE = "all-trajectories-once";
    public static final String PARAM_EXPERT_RANDOM_TRAJECTORIES = "expert-random-trajectories";

    public static final String PARAM_AUTOSAVE_FREQUENCY = "autosave-frequency";
    public static final String PARAM_AUTOSAVE_ON_TIME = "autosave-on-time";
    public static final String PARAM_MAX_RUN_TIME ="max-run-time";
    public static final String PARAM_RESUME_LEARNING = "resume-learning";
    
    public static final String PARAM_ACTION_STEP_SIZE = "action-step-size";
    
    /** GUI parameters */
    public static final String PARAM_USE_GUI = "gui-display";
    public static final String PARAM_PALETTE = "color-palette";

    /** Evaluation parameters */
    public static final String PARAM_EVAL_MOVIE = "eval-make-movie";
    public static final String PARAM_EVAL_MODE = "eval-mode";

    /** Used internally for resuming episodes via --resume */
    public static final String PARAM_RESUME_AT = "resume-at";

    public static final String PARAM_LEFT_DISPLAY_MODE = "gui-left-display-mode";
    public static final String PARAM_RIGHT_DISPLAY_MODE = "gui-right-display-mode";

    public static final String PARAM_MAX_EPISODES = "max-num-episodes";
    public static final String PARAM_EVALUATION_INTERVAL = "evaluation-interval";
    public static final String PARAM_NUM_EVAL_EPISODES = "num-eval-episodes";
    
    public static final String PARAM_RECORD_TRAJECTORY_INTERVAL = "record-trajectory-interval";

    /** Single action results - JAIRParameters */
    public static final String PARAM_SINGLE_ACTION = "single-action";

    public static double modelAlpha;

    public static int aleProtocol;
    
    public static String outputFilename;
    public static String weightsFilename;
    public static String trajectoriesFilename;
    public static String valueFunctionFilename;
    public static String transitionModelFilename;
    public static String namedPipesBasename;

    public static String featureMapClassname;
    public static String featureColorsClassname;
    public static String rlFeatureColorsClassname;
    public static String rlLearnerClassname;
    
    public static int minTrajectoryLength;
    public static int maxTrajectoryLength;
    public static int maxExpertTrajectoryLength;
    public static int actionStepSize;
    public static boolean expertRandomTrajectories;

    public static int trajectoryRandomSampleLength;
    public static int trajectorySamplingInterval;
    public static int trajectoryEvaluationInterval;
    public static int trajectoryHeadIgnoreLength;
    public static int autosaveFrequency;
    public static boolean autosaveOnTime;
    public static boolean resumeLearning;
    public static int maximumSampleCount;
    public static boolean allTrajectoriesOnce;
    
    public static boolean useGui;
    public static String colorPalette;
    
    public static boolean evalMakeMovie;
    public static boolean evalMode;

    public static int maxRunTime;
    public static int resumeAt;
    public static int maxNumEpisodes;
    public static int evaluationInterval;
    public static int numEvalEpisodes;

    public static int recordTrajectoryInterval;
    
    public static int leftDisplayMode;
    public static int rightDisplayMode;

    public static int singleAction;
    
    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_ALPHA)) return;

        // Model parameters
        params.addDoubleParam(PARAM_ALPHA, 0.1);

        params.addIntegerParam(PARAM_ALE_PROTOCOL, 2);
        
        params.addStringParam(PARAM_WEIGHTS_FILE, null);
        params.addStringParam(PARAM_TRAJECTORIES_FILE, null);
        params.addStringParam(PARAM_VALUE_FUNCTION_FILE, null);
        params.addStringParam(PARAM_TRANSITION_MODEL_FILE, null);
        params.addStringParam(PARAM_NAMED_PIPES_FILE, null);
        
        params.addStringParam(PARAM_FEATURE_MAP_CLASSNAME, "nips11.agents.rlfeatures.BASSFeatureMap");
        params.addStringParam(PARAM_FEATURE_COLORS_CLASSNAME, "nips11.agents.features.colors.SECAMColors");
        params.addStringParam(PARAM_RL_FEATURE_COLORS_CLASSNAME, null);
        params.addStringParam(PARAM_RL_LEARNER_CLASSNAME, "nips11.rl.SarsaLearner");
        params.addStringParam(PARAM_OUTPUT_FILE, null);
        
        // Trajectory generation parameters
        params.addStringParam(PARAM_RANDOM_TRAJECTORY_RANGE, "100:1000");
        params.addIntegerParam(PARAM_ACTION_STEP_SIZE, 15);
        params.addIntegerParam(PARAM_TRAJECTORY_SAMPLING_INTERVAL, 0);
        params.addIntegerParam(PARAM_TRAJECTORY_EVALUATION_INTERVAL, 0);
        params.addIntegerParam(PARAM_RANDOM_SAMPLE_LENGTH, 1);
        params.addIntegerParam(PARAM_TRAJECTORY_HEAD_IGNORE, 0);
        params.addIntegerParam(PARAM_AUTOSAVE_FREQUENCY, 0);
        params.addBooleanParam(PARAM_AUTOSAVE_ON_TIME, false);
        params.addIntegerParam(PARAM_MAXIMUM_SAMPLE_COUNT, 0);
        params.addBooleanParam(PARAM_ALL_TRAJECTORIES_ONCE, false);
        params.addIntegerParam(PARAM_MAX_EXPERT_TRAJECTORY_LENGTH, 0);
        params.addBooleanParam(PARAM_EXPERT_RANDOM_TRAJECTORIES, false);
        params.addBooleanParam(PARAM_RESUME_LEARNING, false);
        
        // GUI parameters
        params.addBooleanParam(PARAM_USE_GUI, true);
        params.addStringParam(PARAM_PALETTE, "NTSC");

        // Evaluation parameters
        params.addBooleanParam(PARAM_EVAL_MOVIE, false);
        params.addBooleanParam(PARAM_EVAL_MODE, false);

        params.addIntegerParam(PARAM_MAX_RUN_TIME, 0);
        params.addIntegerParam(PARAM_RESUME_AT, 0);
        params.addIntegerParam(PARAM_MAX_EPISODES, 0);
        params.addIntegerParam(PARAM_EVALUATION_INTERVAL, 0);
        params.addIntegerParam(PARAM_NUM_EVAL_EPISODES, 0);
        
        params.addIntegerParam(PARAM_LEFT_DISPLAY_MODE, -1);
        params.addIntegerParam(PARAM_RIGHT_DISPLAY_MODE, -1);

        params.addIntegerParam(PARAM_RECORD_TRAJECTORY_INTERVAL, 0);
        
        params.addIntegerParam(PARAM_SINGLE_ACTION, 0);
    }

    public static void parseParameters(ParameterHolder params) {
        modelAlpha = params.getDoubleParam(PARAM_ALPHA);
        aleProtocol = params.getIntegerParam(PARAM_ALE_PROTOCOL);
        
        weightsFilename = params.getStringParam(PARAM_WEIGHTS_FILE);
        trajectoriesFilename = params.getStringParam(PARAM_TRAJECTORIES_FILE);
        valueFunctionFilename = params.getStringParam(PARAM_VALUE_FUNCTION_FILE);
        transitionModelFilename = params.getStringParam(PARAM_TRANSITION_MODEL_FILE);
        namedPipesBasename = params.getStringParam(PARAM_NAMED_PIPES_FILE);
        outputFilename = params.getStringParam(PARAM_OUTPUT_FILE);
        
        featureMapClassname = params.getStringParam(PARAM_FEATURE_MAP_CLASSNAME);
        featureColorsClassname = params.getStringParam(PARAM_FEATURE_COLORS_CLASSNAME);
        rlFeatureColorsClassname = params.getStringParam(PARAM_RL_FEATURE_COLORS_CLASSNAME);
        rlLearnerClassname = params.getStringParam(PARAM_RL_LEARNER_CLASSNAME);

        if (rlFeatureColorsClassname == null)
            rlFeatureColorsClassname = featureColorsClassname;
        
        useGui = params.getBooleanParam(PARAM_USE_GUI);
        colorPalette = params.getStringParam(PARAM_PALETTE).toUpperCase();

        // Parse trajectory range
        String[] tokens = params.getStringParam(PARAM_RANDOM_TRAJECTORY_RANGE).split(":");

        minTrajectoryLength = Integer.parseInt(tokens[0]);
        maxTrajectoryLength = Integer.parseInt(tokens[1]);

        maxExpertTrajectoryLength = params.getIntegerParam(PARAM_MAX_EXPERT_TRAJECTORY_LENGTH);
        
        actionStepSize = params.getIntegerParam(PARAM_ACTION_STEP_SIZE);
        trajectorySamplingInterval = params.getIntegerParam(PARAM_TRAJECTORY_SAMPLING_INTERVAL);
        trajectoryEvaluationInterval = params.getIntegerParam(PARAM_TRAJECTORY_EVALUATION_INTERVAL);
        trajectoryRandomSampleLength = params.getIntegerParam(PARAM_RANDOM_SAMPLE_LENGTH);
        trajectoryHeadIgnoreLength = params.getIntegerParam(PARAM_TRAJECTORY_HEAD_IGNORE);
        autosaveFrequency = params.getIntegerParam(PARAM_AUTOSAVE_FREQUENCY);
        autosaveOnTime = params.getBooleanParam(PARAM_AUTOSAVE_ON_TIME);
        maximumSampleCount = params.getIntegerParam(PARAM_MAXIMUM_SAMPLE_COUNT);
        allTrajectoriesOnce = params.getBooleanParam(PARAM_ALL_TRAJECTORIES_ONCE);
        expertRandomTrajectories = params.getBooleanParam(PARAM_EXPERT_RANDOM_TRAJECTORIES);
        resumeLearning = params.getBooleanParam(PARAM_RESUME_LEARNING);
        
        evalMakeMovie = params.getBooleanParam(PARAM_EVAL_MOVIE);
        evalMode = params.getBooleanParam(PARAM_EVAL_MODE);

        maxRunTime = params.getIntegerParam(PARAM_MAX_RUN_TIME);

        resumeAt = params.getIntegerParam(PARAM_RESUME_AT);
        maxNumEpisodes = params.getIntegerParam(PARAM_MAX_EPISODES);
        evaluationInterval = params.getIntegerParam(PARAM_EVALUATION_INTERVAL);
        numEvalEpisodes = params.getIntegerParam(PARAM_NUM_EVAL_EPISODES);
        
        leftDisplayMode = params.getIntegerParam(PARAM_LEFT_DISPLAY_MODE);
        rightDisplayMode = params.getIntegerParam(PARAM_RIGHT_DISPLAY_MODE);

        recordTrajectoryInterval = params.getIntegerParam(PARAM_RECORD_TRAJECTORY_INTERVAL);
        
        singleAction = params.getIntegerParam(PARAM_SINGLE_ACTION);
    }
}
