/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class DISCOParameters {
    public static final String PARAM_PLOT_REGION_PRE_MERGE = "disco-plot-pre-merge";
    public static final String PARAM_PLOT_REGION_POST_MERGE = "disco-plot-post-merge";
    public static final String PARAM_MAX_PERC_DIFFERENCE = "disco-max-percent-difference";
    public static final String PARAM_MAX_OBJ_VELOCITY = "disco-max-object-velocity";
    public static final String PARAM_MAX_NUM_INSTANCES = "disco-max-instances-on-screen";
    public static final String PARAM_MAX_SHAPE_AREA_DIF = "disco-max-shape-area-difference";
    public static final String PARAM_MIN_ON_FRAME_RATIO = "disco-min-on-frame-ratio";
    public static final String PARAM_MIN_BOUNDARY_LENGTH = "disco-min-boundary-length";
    public static final String PARAM_MAX_NUM_CLASSES = "disco-max-num-classes";
    public static final String PARAM_PLOT_PRE_FILTER = "disco-plot-pre-filter";
    public static final String PARAM_PLOT_POST_FILTER = "disco-plot-post-filter";
    
    public static final String PARAM_SHAPES_FILENAME = "disco-shapes-filename";
    public static final String PARAM_NUM_TILINGS = "disco-num-tilings";
    public static final String PARAM_NUM_TILES = "disco-num-tiles";
    
    public static boolean plotRegionMatrixPreMerge;
    public static boolean plotRegionMatrixPostMerge;
    public static double maxPercDifference;
    public static int maxObjVelocity;
    public static int maxInstancesOnScreen;
    public static double maxShapeAreaDif;
    public static double minOnFrameRatio;
    public static int minBoundaryLength;
    public static int maxNumClasses;
    public static boolean plotPreFilterClasses;
    public static boolean plotPostFilterClasses;

    public static String shapesFilename;
    public static int numTilings;
    public static int numTiles;
    
    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_PLOT_REGION_PRE_MERGE)) return;
        
        // Values taken from Settings.cxx in ALE v0.1, class_disc_params.txt; and Yavar Naddaf's thesis
        params.addBooleanParam(PARAM_PLOT_REGION_PRE_MERGE, false);
        params.addBooleanParam(PARAM_PLOT_REGION_POST_MERGE, false);
        // As reported in Yavar Naddaf's thesis
        params.addDoubleParam(PARAM_MAX_PERC_DIFFERENCE, 0.1);
        params.addIntegerParam(PARAM_MAX_OBJ_VELOCITY, 8);
        params.addDoubleParam(PARAM_MAX_SHAPE_AREA_DIF, 1.2);
        params.addDoubleParam(PARAM_MIN_ON_FRAME_RATIO, 0.2);
        // MGB: was 0.3 in class_disc_params.txt; but declared as an int
        params.addIntegerParam(PARAM_MIN_BOUNDARY_LENGTH, 0);
        // As reported in Yavar Naddaf's thesis
        params.addIntegerParam(PARAM_MAX_NUM_CLASSES, 10);
        params.addBooleanParam(PARAM_PLOT_PRE_FILTER, false);
        params.addBooleanParam(PARAM_PLOT_POST_FILTER, true);

        params.addIntegerParam(PARAM_MAX_NUM_INSTANCES, 0);
        
        params.addStringParam(PARAM_SHAPES_FILENAME, "/tmp/agent.shapes");
        params.addIntegerParam(PARAM_NUM_TILINGS, 8);
        params.addIntegerParam(PARAM_NUM_TILES, 10);
    }

    public static void parseParameters(ParameterHolder params) {
        plotRegionMatrixPreMerge = params.getBooleanParam(PARAM_PLOT_REGION_PRE_MERGE);
        plotRegionMatrixPostMerge = params.getBooleanParam(PARAM_PLOT_REGION_POST_MERGE);
        maxPercDifference = params.getDoubleParam(PARAM_MAX_PERC_DIFFERENCE);
        maxObjVelocity = params.getIntegerParam(PARAM_MAX_OBJ_VELOCITY);
        maxShapeAreaDif = params.getDoubleParam(PARAM_MAX_SHAPE_AREA_DIF);
        minOnFrameRatio = params.getDoubleParam(PARAM_MIN_ON_FRAME_RATIO);
        minBoundaryLength = params.getIntegerParam(PARAM_MIN_BOUNDARY_LENGTH);
        maxNumClasses = params.getIntegerParam(PARAM_MAX_NUM_CLASSES);
        plotPreFilterClasses = params.getBooleanParam(PARAM_PLOT_PRE_FILTER);
        plotPostFilterClasses = params.getBooleanParam(PARAM_PLOT_POST_FILTER);

        maxInstancesOnScreen = params.getIntegerParam(PARAM_MAX_NUM_INSTANCES);
        
        shapesFilename = params.getStringParam(PARAM_SHAPES_FILENAME);
        numTilings = params.getIntegerParam(PARAM_NUM_TILINGS);
        numTiles = params.getIntegerParam(PARAM_NUM_TILES);
    }
}
