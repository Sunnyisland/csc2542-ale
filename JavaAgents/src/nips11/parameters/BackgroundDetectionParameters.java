/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class BackgroundDetectionParameters {
    public static final String PARAM_BACKGROUND_BASENAME = "background-basename";

    public static String backgroundBasename;

    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_BACKGROUND_BASENAME)) return;

        params.addStringParam(PARAM_BACKGROUND_BASENAME, null);
    }

    public static void parseParameters(ParameterHolder params) {
        backgroundBasename = params.getStringParam(PARAM_BACKGROUND_BASENAME);
    }
}
