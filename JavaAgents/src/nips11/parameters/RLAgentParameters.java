/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class RLAgentParameters {
    public static final String PARAM_ALPHA       = "value-learning-alpha";
    public static final String PARAM_GQ_BETA     = "gq-beta";
    public static final String PARAM_GAMMA       = "agent-gamma";
    public static final String PARAM_LAMBDA      = "agent-lambda";
    public static final String PARAM_EPSILON     = "agent-epsilon";
    public static final String ACCUMULATING_TRACES = "agent-accumulating-traces";

    public static final String PARAM_ON_POLICY   = "agent-on-policy-update";
    public static final String PARAM_EXPECTED_SARSA   = "agent-expected-sarsa";
    
    public static final String PARAM_INITIAL_WEIGHTS = "value-initial-weights";
    public static final String PARAM_MAX_NORM_ALPHA = "alpha-max-norm";

    public static final String PARAM_GQ_EGREEDY_SUBGRADIENT = "gq-egreedy-subgradient";
    
    public static double alpha;
    public static double beta;
    public static double gamma;
    public static double lambda;
    public static double epsilon;
    public static boolean accumulatingTraces;

    public static boolean onPolicyUpdate;
    public static boolean expectedSarsa;
    public static boolean eGreedySubgradient;
    
    public static double initialWeights;
    public static boolean alphaMaxNorm;

    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_ALPHA)) return;

        params.addDoubleParam(PARAM_ALPHA, 0.1);
        params.addDoubleParam(PARAM_GQ_BETA, 1.0);
        params.addDoubleParam(PARAM_GAMMA, 0.9);
        params.addDoubleParam(PARAM_LAMBDA, 0.8);
        params.addDoubleParam(PARAM_EPSILON, 0.05);
        params.addBooleanParam(ACCUMULATING_TRACES, false);

        params.addBooleanParam(PARAM_ON_POLICY, false);
        params.addBooleanParam(PARAM_EXPECTED_SARSA, false);
        params.addBooleanParam(PARAM_GQ_EGREEDY_SUBGRADIENT, false);
        
        params.addDoubleParam(PARAM_INITIAL_WEIGHTS, 0.0);
        params.addBooleanParam(PARAM_MAX_NORM_ALPHA, false);
    }

    public static void parseParameters(ParameterHolder params) {
        alpha = params.getDoubleParam(PARAM_ALPHA);
        beta = params.getDoubleParam(PARAM_GQ_BETA);
        gamma = params.getDoubleParam(PARAM_GAMMA);
        lambda = params.getDoubleParam(PARAM_LAMBDA);
        epsilon = params.getDoubleParam(PARAM_EPSILON);
        accumulatingTraces = params.getBooleanParam(ACCUMULATING_TRACES);
        
        onPolicyUpdate = params.getBooleanParam(PARAM_ON_POLICY);
        expectedSarsa = params.getBooleanParam(PARAM_EXPECTED_SARSA);
        eGreedySubgradient = params.getBooleanParam(PARAM_GQ_EGREEDY_SUBGRADIENT);
        
        initialWeights = params.getDoubleParam(PARAM_INITIAL_WEIGHTS);
        alphaMaxNorm = params.getBooleanParam(PARAM_MAX_NORM_ALPHA);
    }
}
