/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class MovieParameters {
    public static final String PARAM_NUM_CYCLES = "movie-cycles-per-image";
    public static final String PARAM_BASE_FILENAME = "movie-base-filename";
    public static final String PARAM_MOVIE_MODE = "movie-mode";
    
    public static int cyclesPerImage;
    public static String baseFilename;
    public static int movieMode;

    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_NUM_CYCLES)) return;

        params.addIntegerParam(PARAM_NUM_CYCLES, 1);
        params.addStringParam(PARAM_BASE_FILENAME, null);
        params.addIntegerParam(PARAM_MOVIE_MODE, 0);
    }

    public static void parseParameters(ParameterHolder params) {
        cyclesPerImage = params.getIntegerParam(PARAM_NUM_CYCLES);
        baseFilename = params.getStringParam(PARAM_BASE_FILENAME);
        movieMode = params.getIntegerParam(PARAM_MOVIE_MODE);
    }
}
