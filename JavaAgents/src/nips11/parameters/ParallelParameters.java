/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class ParallelParameters {
    public static final String PARAM_NUM_THREADS = "parallel-num-threads";
    
    public static int numThreads;

    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_NUM_THREADS)) return;

        params.addIntegerParam(PARAM_NUM_THREADS, 1);
    }

    public static void parseParameters(ParameterHolder params) {
        numThreads = params.getIntegerParam(PARAM_NUM_THREADS);
    }
}
