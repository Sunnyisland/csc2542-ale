/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class RLFeatureMapParameters {
    public static final String PARAM_MEMORY_SIZE = "rl-memory-size";
    public static final String PARAM_INNER_MAP_CLASSNAME = "features-map-hashed";
    
    public static int memorySize;
    public static String featuresMapClassname;

    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_MEMORY_SIZE)) return;

        params.addIntegerParam(PARAM_MEMORY_SIZE, 50000);
        params.addStringParam(PARAM_INNER_MAP_CLASSNAME, null);
    }

    public static void parseParameters(ParameterHolder params) {
        memorySize = params.getIntegerParam(PARAM_MEMORY_SIZE);
        featuresMapClassname = params.getStringParam(PARAM_INNER_MAP_CLASSNAME);
    }
}
