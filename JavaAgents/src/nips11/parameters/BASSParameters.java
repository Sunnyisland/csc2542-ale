/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class BASSParameters {
    public static final String PARAM_BACKGROUND_FILENAME = "bass-background-filename";
    public static final String PARAM_NUM_COLUMNS = "bass-num-columns";
    public static final String PARAM_NUM_ROWS = "bass-num-rows";

    public static final String PARAM_NUM_FOVEA_COLUMNS = "bass-num-fovea-columns";
    public static final String PARAM_NUM_FOVEA_ROWS = "bass-num-fovea-rows";
    public static final String PARAM_FOVEA_RADIUS = "bass-fovea-radius";

    public static final String PARAM_BITS_PER_CHANNEL = "bass-bits-per-channel";
    
    public static int numColumns;
    public static int numRows;
    public static int numFoveaColumns;
    public static int numFoveaRows;
    public static int foveaRadius;
    public static String backgroundFilename;

    public static int bitsPerChannel;
    
    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_BACKGROUND_FILENAME)) return;

        params.addIntegerParam(PARAM_NUM_COLUMNS, 16);
        params.addIntegerParam(PARAM_NUM_ROWS, 14);
        params.addIntegerParam(PARAM_NUM_FOVEA_COLUMNS, 5);
        params.addIntegerParam(PARAM_NUM_FOVEA_ROWS, 5);
        params.addIntegerParam(PARAM_FOVEA_RADIUS, 12);
        params.addStringParam(PARAM_BACKGROUND_FILENAME, null);

        params.addIntegerParam(PARAM_BITS_PER_CHANNEL, 3);
    }

    public static void parseParameters(ParameterHolder params) {
        numColumns = params.getIntegerParam(PARAM_NUM_COLUMNS);
        numRows = params.getIntegerParam(PARAM_NUM_ROWS);
        numFoveaColumns = params.getIntegerParam(PARAM_NUM_FOVEA_COLUMNS);
        numFoveaRows = params.getIntegerParam(PARAM_NUM_FOVEA_ROWS);
        foveaRadius = params.getIntegerParam(PARAM_FOVEA_RADIUS);
        
        backgroundFilename = params.getStringParam(PARAM_BACKGROUND_FILENAME);
        bitsPerChannel = params.getIntegerParam(PARAM_BITS_PER_CHANNEL);
    }
}
