/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class RLEnvironmentParameters {
    public static final String PARAM_EPISODE_END_REWARD = "reward-episode-end";
    public static final String PARAM_SCALE_REWARDS      = "environment-scale-rewards";
    
    public static double episodeEndReward;
    public static boolean scaleRewards;

    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_EPISODE_END_REWARD)) return;

        params.addDoubleParam(PARAM_EPISODE_END_REWARD, 0.0);
        params.addBooleanParam(PARAM_SCALE_REWARDS, false);
    }

    public static void parseParameters(ParameterHolder params) {
        episodeEndReward = params.getDoubleParam(PARAM_EPISODE_END_REWARD);
        scaleRewards = params.getBooleanParam(PARAM_SCALE_REWARDS);
    }
}
