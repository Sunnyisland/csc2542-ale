/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.parameters;

import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class WindowFeatureMapParameters {
    public static final String PARAM_HISTORY_LENGTH = "history-length";
    public static final String PARAM_RADIUS = "window-radius";
    public static final String PARAM_RADIUS_INCREMENT = "window-radius-increment";

    public static int historyLength;
    public static int windowRadius;
    public static int windowRadiusIncrement;

    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_HISTORY_LENGTH)) return;

        params.addIntegerParam(PARAM_HISTORY_LENGTH, 2);
        params.addIntegerParam(PARAM_RADIUS, 1);
        params.addIntegerParam(PARAM_RADIUS_INCREMENT, 1);
    }

    public static void parseParameters(ParameterHolder params) {
        historyLength = params.getIntegerParam(PARAM_HISTORY_LENGTH);
        windowRadius = params.getIntegerParam(PARAM_RADIUS);
        windowRadiusIncrement = params.getIntegerParam(PARAM_RADIUS_INCREMENT);
    }
}
