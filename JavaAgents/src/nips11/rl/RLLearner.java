/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.rl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import nips11.agents.models.FeatureList;

/**
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public interface RLLearner {

    void agent_end(double pReward);

    int agent_start(FeatureList state);

    int agent_step(double pReward, FeatureList state);

    // @deprecated
    void loadLearner(String filename);

    // @deprecated
    void saveLearner(String filename);

    void loadLearner(ObjectInputStream in) throws IOException, ClassNotFoundException;

    void saveLearner(ObjectOutputStream out) throws IOException;
    
    void setEvalMode(boolean pEval);

    double getLastValue();

    double[] getValues();

    double stateActionValue(FeatureList pState, int action);
    double stateValue(FeatureList pState);
}
