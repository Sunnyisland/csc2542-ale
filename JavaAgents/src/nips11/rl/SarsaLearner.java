package nips11.rl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import marcgb.tools.print.Numbers;
import nips11.agents.models.AbstractSparseLinearFamilyModelSO;
import nips11.agents.models.FeatureList;
import nips11.agents.models.SparseLinearModelSO;
import nips11.agents.rlfeatures.RLFeatureMap;
import nips11.parameters.ParallelParameters;
import nips11.parameters.RLAgentParameters;
import rlVizLib.general.ParameterHolder;

/**
 *  A class that acts accordingly to an epsilon-greedy policy learns using Sarsa.
 *
 * @author Marc G. Bellemare
 */
public class SarsaLearner implements RLLearner {
    protected int numActions;

    protected boolean evalMode = false;
    protected ParameterHolder params;

    protected double alpha;
    protected double gamma = 1.0;
    protected double lambda = 0.9;
    protected double epsilon = 0.05;

    protected boolean maxNormAlpha;
    
    protected double lastValue;
    protected double[] values;
    protected int lastAction;
    protected int action;
    protected FeatureList lastState;
    protected FeatureList state;
    
    /** Eligibility traces; need separate traces for each action */
    protected FeatureList[] traces;

    protected SparseLinearModelSO[] valueFunction;

    public static boolean debugMode = false;

    public static void addParameters(ParameterHolder params) {
        RLAgentParameters.addParameters(params);
        ParallelParameters.addParameters(params);
    }

    /** Added to support learners that rely on the feature map properties */
    public SarsaLearner(ParameterHolder pParams, RLFeatureMap featureMap, int numActions) {
        this(pParams, featureMap.numFeatures(), numActions);
    }

    public SarsaLearner(ParameterHolder pParams, int numFeatures, int numActions) {
        this.params = pParams;

        RLAgentParameters.parseParameters(pParams);

        alpha = RLAgentParameters.alpha;
        gamma = RLAgentParameters.gamma;
        lambda = RLAgentParameters.lambda;
        epsilon = RLAgentParameters.epsilon;
        this.maxNormAlpha = RLAgentParameters.alphaMaxNorm;

        AbstractSparseLinearFamilyModelSO.setUseMaximumNorm(this.maxNormAlpha);

        this.numActions = numActions;

        createModels(numActions, numFeatures, RLAgentParameters.initialWeights);
        parallelize(params);
        
        values = new double[numActions];
    }

    protected void parallelize(ParameterHolder params) {
        ParallelParameters.parseParameters(params);

        int numThreads = ParallelParameters.numThreads;

        for (int i = 0; i < valueFunction.length; i++)
            valueFunction[i].parallelize(numThreads);
    }

    public void setEvalMode(boolean pEval) {
        evalMode = pEval;
    }

    public void setEpsilon(double e) {
        epsilon = e;
    }

    public double getLastValue() {
        return lastValue;
    }

    public double[] getValues() {
        return values;
    }

    /** Copies the given state to this.state. More efficiently than clone():
     *   reuses objects.
     */
    protected void updateState (FeatureList state) {
        FeatureList reuseList = lastState;

        // Roll state into last state
        lastState = this.state;

        if (reuseList == null)
            reuseList = (FeatureList)state.clone();
        else {
            int numExtraElements = reuseList.active.size() - state.active.size();
            
            ListIterator<FeatureList.Pair> srcIt = state.active.listIterator();
            ListIterator<FeatureList.Pair> dstIt = reuseList.active.listIterator();

            // Reuse existing elements
            while (srcIt.hasNext() && dstIt.hasNext()) {
                FeatureList.Pair srcp = srcIt.next();
                FeatureList.Pair dstp = dstIt.next();

                dstp.index = srcp.index;
                dstp.value = srcp.value;
            }

            // Remove any extra elements
            if (numExtraElements > 0) {
                reuseList.active.subList(state.active.size(), reuseList.active.size()).clear();
            }
            // Add any leftover elements
            else while(srcIt.hasNext()) {
                FeatureList.Pair srcp = srcIt.next();
                reuseList.addFeature(srcp.index, srcp.value);
            }
        }

        this.state = reuseList;
    }
    
    public int agent_start(FeatureList state) {
        lastState = null;
        this.state = null;
        traces = null;

        updateState(state);
        
        return actAndLearn(this.state, 0.0);
    }

    public int agent_step(double pReward, FeatureList state) {
        updateState(state);

        return actAndLearn(this.state, pReward);
    }

    public void agent_end(double pReward) {
        learn(state, action, pReward, null, 0);
    }

   /** Take an action and learn from the option given the current state and
     *   current observation.
     *
     * @param pState
     * @param pObservation
     * @return
     */
    public int actAndLearn(FeatureList pState, double pReward) {
        lastAction = action;

        // Get the next action
        action = selectAction(pState);

        if (lastState != null) {
            // Perform a SARSA update
            learn(lastState, lastAction, pReward, pState, action);
        }

        return action;
    }

    public void learn(FeatureList pLastState, int pLastAction,
            double pReward, FeatureList pState, int pAction) {
        // No learning in evaluation mode
        if (evalMode) return;

        double oldValue = valueFunction[pLastAction].predict(pLastState);
        double newValue;

        if (pState != null) {
            // @optimized newValue = valueFunction[pAction].predict(pState);
            newValue = lastValue;
        }
        else
            newValue = 0;

        // Early exit for diverging agents
        if (Double.isNaN(newValue) || newValue >= 10E7)
            throw new RuntimeException("Diverged.");

        double delta = pReward + gamma * newValue - oldValue;

        updateTraces(pLastState, pLastAction);
        
        // With traces, we update *all* models
        for (int a = 0; a < numActions; a++) {
            AbstractSparseLinearFamilyModelSO model = valueFunction[a];
            FeatureList traces = this.traces[a];

            // Ignore 0-traces
            if (traces.active.isEmpty()) continue;

            model.updateWeightsDelta(traces, delta);
        }
    }

    public void updateTraces(FeatureList pState, int pLastAction) {
        if (traces == null) {
            traces = new FeatureList[numActions];

            for (int a = 0; a < numActions; a++)
                // Make a linked-list backed feature set
                traces[a] = new FeatureList(true);

            // Also copy the contents of the last action's trace
            for (FeatureList.Pair p : pState.active) {
                traces[pLastAction].addFeature(p.index, p.value);
            }
        }
        else {
            FeatureList emptyFeatureSet = new FeatureList();

            for (int a = 0; a < numActions; a++) {
                FeatureList traces = this.traces[a];
                // Be lazy and clear the traces now
                if (lambda == 0.0)
                    traces.clearList();
                
                // For the selected action, decay its trace and add the new
                //  state vector
                if (a == pLastAction) {
                    traces.updateTrace(pState, gamma*lambda, 0.01);
                }
                else
                    traces.updateTrace(emptyFeatureSet, gamma*lambda, 0.01);
            }
        }
    }

    public double stateActionValue(FeatureList pState, int action) {
        return valueFunction[action].predict(pState);
    }

    /** Returns the value of a given state, assuming a greedy policy
      *  @todo - use epsilon
      */
    public double stateValue(FeatureList pState) {
        double best = Double.NEGATIVE_INFINITY;

        double sumValue = 0;

        for (int a = 0; a < numActions; a++) {
            double v = valueFunction[a].predict(pState);
            sumValue += v;
            if (v > best)
                best = v;
        }

        // Estimate the epsilon-greedy value
        double value = sumValue * epsilon / numActions + best * (1.0 - epsilon);

        return value;
    }

    /** Epsilon-greedy action selection.
     *
     * @param pState
     * @return
     */
    public int selectAction(FeatureList pState) {
        double bestValue = Double.NEGATIVE_INFINITY;
        double worstValue = Double.POSITIVE_INFINITY;

        int bestAction = -1;
        ArrayList<Integer> ties = new ArrayList<Integer>();

        // E-greedy
        if (!debugMode && Math.random() < epsilon) {
            int r = (int)(Math.random() * numActions);
            lastValue = valueFunction[r].predict(pState);
            return r;
        }
       
        // Greedy selection, with random tie-breaking
        for (int a = 0; a < numActions; a++) {
            double v = valueFunction[a].predict(pState);

            values[a] = v;

            if (v > bestValue) {
                bestValue = v;
                bestAction = a;
                ties.clear();
                ties.add(bestAction);
            }
            else if (v == bestValue) {
                ties.add(a);
            }

            if (v < worstValue)
                worstValue = v;
        }

        // Tie-breaker
        if (ties.size() > 1) {
            int r = (int)(Math.random() * ties.size());
            bestAction = ties.get(r);
        }

        // In debug mode, we e-greedy AFTER computing the values
        if (debugMode && Math.random() < epsilon) {
            int r = (int)(Math.random() * numActions);
            lastValue = values[r];
            return r;
        }
        
        lastValue = bestValue;
        
        return bestAction;
    }

   /** This method should construct the set of models used by this agent.
     *
     * @param pNumActions The number of actions available to the agent.
     * @param pObservationDim The dimension of the observation vector.
     */
    protected void createModels(int pNumActions, int pNumFeatures, double initialWeights) {
        valueFunction = new SparseLinearModelSO[pNumActions];

        // @dbg adding to see if this fixes the too-much-memory-needed issue on
        //  mammouth
        System.gc();

        for (int a = 0; a < pNumActions; a++) {
            valueFunction[a] = new SparseLinearModelSO(pNumFeatures, true);
            valueFunction[a].setAlpha(alpha);
            // Set all weights to the given initial value
            valueFunction[a].setWeights(initialWeights);
        }
    }

    public void saveLearner(String filename) {
        synchronized(this) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(filename)));

                oos.writeObject(valueFunction);
                oos.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void saveLearner(ObjectOutputStream out) throws IOException {
        out.writeObject(valueFunction);
    }
    
    public void loadLearner(String filename) {
        synchronized(this) {
            try {
                // Trash the current value function to avoid memory issues
                valueFunction = null;
                
                ObjectInputStream ois = new ObjectInputStream(new GZIPInputStream(new FileInputStream(filename)));
                valueFunction = (SparseLinearModelSO[]) ois.readObject();

                ois.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void loadLearner(ObjectInputStream in) throws IOException, ClassNotFoundException {
        valueFunction = null;
        valueFunction = (SparseLinearModelSO[]) in.readObject();
    }
}
