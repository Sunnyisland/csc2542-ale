package nips11.rl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import nips11.agents.models.AbstractSparseLinearFamilyModelSO;
import nips11.agents.models.FeatureList;
import nips11.agents.models.SparseLinearModelSO;
import nips11.agents.rlfeatures.RLFeatureMap;
import nips11.parameters.ParallelParameters;
import nips11.parameters.RLAgentParameters;
import rlVizLib.general.ParameterHolder;

/**
 *  A class that acts accordingly to an epsilon-greedy policy learns using Sarsa.
 *
 * @author Marc G. Bellemare
 */
public class GQLearner implements RLLearner {
    protected int numActions;

    protected boolean evalMode = false;
    protected ParameterHolder params;

    protected double alpha;
    protected double beta;
    protected double gamma = 1.0;
    protected double epsilon = 0.05;

    protected boolean maxNormAlpha;
    
    protected double lastValue;
    protected double expectedValue;
    protected double[] values;
    protected int lastAction;
    protected int action;
    protected FeatureList lastState;
    protected FeatureList state;
    
    protected SparseLinearModelSO[] valueFunction;
    protected SparseLinearModelSO[] gqWeights;

    public static boolean debugMode = false;

    public static void addParameters(ParameterHolder params) {
        RLAgentParameters.addParameters(params);
        ParallelParameters.addParameters(params);
    }

    /** Added to support learners that rely on the feature map properties */
    public GQLearner(ParameterHolder pParams, RLFeatureMap featureMap, int numActions) {
        this(pParams, featureMap.numFeatures(), numActions);
    }

    public GQLearner(ParameterHolder pParams, int numFeatures, int numActions) {
        this.params = pParams;

        RLAgentParameters.parseParameters(pParams);

        alpha = RLAgentParameters.alpha;
        beta = RLAgentParameters.beta;
        gamma = RLAgentParameters.gamma;
        epsilon = RLAgentParameters.epsilon;
        this.maxNormAlpha = RLAgentParameters.alphaMaxNorm;

        AbstractSparseLinearFamilyModelSO.setUseMaximumNorm(this.maxNormAlpha);

        this.numActions = numActions;

        createModels(numActions, numFeatures, RLAgentParameters.initialWeights);
        parallelize(params);
        
        values = new double[numActions];
    }

    protected void parallelize(ParameterHolder params) {
        ParallelParameters.parseParameters(params);

        int numThreads = ParallelParameters.numThreads;

        for (int i = 0; i < valueFunction.length; i++)
            valueFunction[i].parallelize(numThreads);
    }

    public void setEvalMode(boolean pEval) {
        evalMode = pEval;
    }

    public void setEpsilon(double e) {
        epsilon = e;
    }

    public double getLastValue() {
        return lastValue;
    }

    public double[] getValues() {
        return values;
    }

    /** Copies the given state to this.state. More efficiently than clone():
     *   reuses objects.
     */
    protected void updateState (FeatureList state) {
        FeatureList reuseList = lastState;

        // Roll state into last state
        lastState = this.state;

        if (reuseList == null)
            reuseList = (FeatureList)state.clone();
        else {
            int numExtraElements = reuseList.active.size() - state.active.size();
            
            ListIterator<FeatureList.Pair> srcIt = state.active.listIterator();
            ListIterator<FeatureList.Pair> dstIt = reuseList.active.listIterator();

            // Reuse existing elements
            while (srcIt.hasNext() && dstIt.hasNext()) {
                FeatureList.Pair srcp = srcIt.next();
                FeatureList.Pair dstp = dstIt.next();

                dstp.index = srcp.index;
                dstp.value = srcp.value;
            }

            // Remove any extra elements
            if (numExtraElements > 0) {
                reuseList.active.subList(state.active.size(), reuseList.active.size()).clear();
            }
            // Add any leftover elements
            else while(srcIt.hasNext()) {
                FeatureList.Pair srcp = srcIt.next();
                reuseList.addFeature(srcp.index, srcp.value);
            }
        }

        this.state = reuseList;
    }
    
    public int agent_start(FeatureList state) {
        lastState = null;
        this.state = null;

        updateState(state);
        
        return actAndLearn(this.state, 0.0);
    }

    public int agent_step(double pReward, FeatureList state) {
        updateState(state);

        return actAndLearn(this.state, pReward);
    }

    public void agent_end(double pReward) {
        learn(state, action, pReward, null, 0);
    }

   /** Take an action and learn from the option given the current state and
     *   current observation.
     *
     * @param pState
     * @param pObservation
     * @return
     */
    public int actAndLearn(FeatureList pState, double pReward) {
        lastAction = action;

        // Get the next action
        action = selectAction(pState);
        int bestAction = getBestAction();

        if (lastState != null) {
            // Perform a GQ update - use the max action!
            learn(lastState, lastAction, pReward, pState, bestAction);
        }

        return action;
    }

    private int getBestAction() {
        return bestActionTies.get((int)(Math.random() * bestActionTies.size()));
    }

    public void learn(FeatureList pLastState, int pLastAction,
            double pReward, FeatureList pState, int pBestAction) {
        // No learning in evaluation mode
        if (evalMode) return;

        double oldValue = valueFunction[pLastAction].predict(pLastState);
        double newValue;

        if (pState != null) {
            newValue = expectedValue;
        }
        else
            newValue = 0;

        double expectedDelta = gqWeights[pLastAction].predict(pLastState);
        
        // Early exit for diverging agents
        if (Double.isNaN(newValue) || newValue >= 10E7)
            throw new RuntimeException("Diverged.");

        double delta = pReward + gamma * newValue - oldValue;

        // Regular update
        valueFunction[pLastAction].updateWeightsDelta(pLastState, delta);
        // Extra term in GQ
        if (pState != null)
            valueFunction[pBestAction].updateWeightsDelta(pState, -gamma * expectedDelta);

        // Now update the expected delta
        double gqWeightUpdate = delta - expectedDelta;
        gqWeights[pLastAction].updateWeightsDelta(pLastState, gqWeightUpdate);
    }

    public double stateActionValue(FeatureList pState, int action) {
        return valueFunction[action].predict(pState);
    }

    /** Returns the value of a given state, assuming a greedy policy
      *  @todo - use epsilon
      */
    public double stateValue(FeatureList pState) {
        double best = Double.NEGATIVE_INFINITY;

        double sumValue = 0;

        for (int a = 0; a < numActions; a++) {
            double v = valueFunction[a].predict(pState);
            sumValue += v;
            if (v > best)
                best = v;
        }

        // Estimate the epsilon-greedy value
        double value = sumValue * epsilon / numActions + best * (1.0 - epsilon);

        return value;
    }

    private ArrayList<Integer> bestActionTies;
    /** Epsilon-greedy action selection.
     *
     * @param pState
     * @return
     */
    public int selectAction(FeatureList pState) {
        double bestValue = Double.NEGATIVE_INFINITY;
        double worstValue = Double.POSITIVE_INFINITY;

        int bestAction = -1;
        if (bestActionTies == null) bestActionTies = new ArrayList<Integer>();
        else bestActionTies.clear();

        expectedValue = 0;

        // Greedy selection, with random tie-breaking
        for (int a = 0; a < numActions; a++) {
            double v = valueFunction[a].predict(pState);

            values[a] = v;
            // Everyone contributes epsilon to the expected next value
            expectedValue += v * epsilon / numActions;

            if (v > bestValue) {
                bestValue = v;
                bestAction = a;
                bestActionTies.clear();
                bestActionTies.add(bestAction);
            }
            else if (v == bestValue) {
                bestActionTies.add(a);
            }

            if (v < worstValue)
                worstValue = v;
        }

        for (int a : bestActionTies) {
            expectedValue += values[a] * (1.0 - epsilon) / bestActionTies.size();
        }
        
        // Tie-breaker
        if (bestActionTies.size() > 1) {
            int r = (int)(Math.random() * bestActionTies.size());
            bestAction = bestActionTies.get(r);
        }

        // In debug mode, we e-greedy AFTER computing the values
        if (Math.random() < epsilon) {
            int r = (int)(Math.random() * numActions);
            lastValue = values[r];
            return r;
        }
        
        lastValue = bestValue;
        
        return bestAction;
    }

   /** This method should construct the set of models used by this agent.
     *
     * @param pNumActions The number of actions available to the agent.
     * @param pObservationDim The dimension of the observation vector.
     */
    protected void createModels(int pNumActions, int pNumFeatures, double initialWeights) {
        valueFunction = new SparseLinearModelSO[pNumActions];
        gqWeights = new SparseLinearModelSO[pNumActions];

        // @dbg adding to see if this fixes the too-much-memory-needed issue on
        //  mammouth
        System.gc();

        for (int a = 0; a < pNumActions; a++) {
            valueFunction[a] = new SparseLinearModelSO(pNumFeatures, true);
            valueFunction[a].setAlpha(alpha);
            // Set all weights to the given initial value
            valueFunction[a].setWeights(initialWeights);
        }

        // Create the GQ weights
        for (int a = 0; a < pNumActions; a++) {
            gqWeights[a] = new SparseLinearModelSO(pNumFeatures, true);
            gqWeights[a].setAlpha(beta);
        }
    }

    public void saveLearner(String filename) {
        synchronized(this) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(filename)));

                oos.writeObject(valueFunction);
                oos.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void saveLearner(ObjectOutputStream out) throws IOException {
        out.writeObject(valueFunction);
        out.writeObject(gqWeights);
    }
    
    public void loadLearner(String filename) {
        synchronized(this) {
            try {
                // Trash the current value function to avoid memory issues
                valueFunction = null;
                
                ObjectInputStream ois = new ObjectInputStream(new GZIPInputStream(new FileInputStream(filename)));
                valueFunction = (SparseLinearModelSO[]) ois.readObject();

                ois.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void loadLearner(ObjectInputStream in) throws IOException, ClassNotFoundException {
        valueFunction = null;
        gqWeights = null;
        System.gc();
        valueFunction = (SparseLinearModelSO[]) in.readObject();
        gqWeights = (SparseLinearModelSO[]) in.readObject();
    }
}
