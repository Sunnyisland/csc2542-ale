/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package explore.rl;

import nips11.agents.data.FrameHistory;

/** An interface that allows learners to receive the current history.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public interface NeedsHistory {
    public void observeHistory(FrameHistory history);
}
