/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.rl;

import atariagents.io.ConsoleRAM;
import atariagents.io.RLData;
import atariagents.screen.ScreenMatrix;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import nips11.parameters.RLEnvironmentParameters;
import rlVizLib.general.ParameterHolder;

/** Provides environment-like capabilities by reading the screen and RAM.
 *
 *
 * @author Marc G. Bellemare
 */
public class EnvironmentProvider {
    /** For shaping purposes; optional reward at episode end */
    protected double episodeEndReward;

    protected double rewardMagnitude;
    protected boolean baseRewardObserved;

    // @todo make this a parameter
    protected boolean costPerStepReward = false;
    protected boolean scaleRewards;
    
    protected ScreenMatrix screen;
    protected ConsoleRAM ram;
    protected RLData rlData;

    protected double accumulatedReward;
    protected boolean isTerminal;
    protected double score;

    public EnvironmentProvider(ParameterHolder params) {
        RLEnvironmentParameters.parseParameters(params);
        episodeEndReward = RLEnvironmentParameters.episodeEndReward;
        scaleRewards = RLEnvironmentParameters.scaleRewards;
        
        reset();
    }

    public static void addParameters(ParameterHolder params) {
        RLEnvironmentParameters.addParameters(params);
    }

    /** Notify the environment that we are beginning a new time step (which may
     *   be composed of many ALE frames).
     */
    public void newStep() {
        accumulatedReward = 0;
        isTerminal = false;
    }

    public double getReward() {
        double reward = accumulatedReward;

        if (scaleRewards && baseRewardObserved)
            reward /= rewardMagnitude;
        
        if (costPerStepReward)
            reward = (reward > 0? 0.0 : -1.0);

        if (isTerminal())
            reward += episodeEndReward;

        return reward;
    }

    public double getRewardMagnitude() {
        return rewardMagnitude;
    }

    public double getScore() {
        return score;
    }

    public boolean isTerminal() {
        return isTerminal;
    }

    public void observe(ScreenMatrix screen, ConsoleRAM ram, RLData rlData) {
        this.screen = screen;
        this.ram = ram;
        this.rlData = rlData;

        // If necessary, update our reward scale
        updateRewardScale();

        // Add reward/score, but only if we are still running the episode
        //  This was added to ignore the case when we restart a new game because
        //  of macro-actions
        isTerminal |= this.rlData.isTerminal;

        catchSuspiciousRewards(rlData);

        if (!isTerminal) {
            accumulatedReward += this.rlData.reward;
            score += this.rlData.reward;
        }
    }

    /** Used to find a 'base reward' scale so that 1.0 reward corresponds to
     *   whatever base amount the agent gets, e.g. 44 points in Beam Rider.
     */
    protected void updateRewardScale() {
        if (scaleRewards) {
            if (!baseRewardObserved && this.rlData.reward != 0) {
                rewardMagnitude = Math.abs(this.rlData.reward);
                baseRewardObserved = true;
            }
        }
    }

    private void catchSuspiciousRewards(RLData rlData) {
        // Ignore the small rewards case, which will actually happen in games like hockey
        if (!isTerminal && score == -rlData.reward && Math.abs(rlData.reward) >= 10) {
            System.err.println ("WARNING: Possible game reset without terminal.");
        }
    }

    /** Resets the environment's internal variables, such as score.
     * 
     */
    public void reset() {
        score = 0;
        newStep();
    }

    public void loadEnvironment(ObjectInputStream in) throws IOException, ClassNotFoundException {
        rewardMagnitude = in.readDouble();
        baseRewardObserved = in.readBoolean();
    }

    public void saveEnvironment(ObjectOutputStream out) throws IOException {
        out.writeDouble(rewardMagnitude);
        out.writeBoolean(baseRewardObserved);
    }

    /** Save/restore mechanism */
    
    protected double s_accumulatedReward;
    protected boolean s_isTerminal;
    protected double s_score;

    /** Saves the environment state for a later restore */
    public void saveState() {
        s_accumulatedReward = accumulatedReward;
        s_isTerminal = isTerminal;
        s_score = score;
    }

    public void restoreState() {
        accumulatedReward = s_accumulatedReward;
        isTerminal = s_isTerminal;
        score = s_score;
    }
}
