/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import marcgb.tools.config.Parameters;
import marcgb.tools.loader.DynamicLoader;
import nips11.agents.AbstractAgent;
import rlVizLib.general.ParameterHolder;

/** Loader to run one of the three agents.
 *
 * @author Marc G. Bellemare
 */
public class AgentLoader {
    public static String PARAM_INTERFACE_CLASSNAME = "interface-classname";

    public static void addParameters(ParameterHolder params) {
        params.addStringParam(PARAM_INTERFACE_CLASSNAME, "nips11.agents.HumanAgent");
    }

    public static void main(String[] args) throws Throwable {
        ParameterHolder params = new ParameterHolder();
        AgentLoader.addParameters(params);

        String configFilename = null;

        // Find the config file name as the first argument that does not start with a -
        for (int i = 0; i < args.length; i++) {
            if (!args[i].startsWith("-")) {
                configFilename = args[i];
                break;
            }
        }

        if (configFilename != null) {
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }

        String mainClassname = params.getStringParam(PARAM_INTERFACE_CLASSNAME);

        DynamicLoader.invoke("main", mainClassname, new Object[] {args});
    }

}
