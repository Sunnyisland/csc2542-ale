/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/** A class written to slow down the java agent so that the combined Java+ALE
 *   speed does not exceed exactly one CPU.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class CPUThrottle extends Thread {
    protected String javaPID;
    protected String alePID;

    protected final Thread parent;
    
    protected volatile boolean terminated;
    
    public CPUThrottle(Thread parent) {
        this.parent = parent;
        
        try {
            // Get our own PID
            Process bash = Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", "echo $PPID"});
            BufferedReader bashIn = new BufferedReader(new InputStreamReader(bash.getInputStream()));
            javaPID = bashIn.readLine();
            // @dbg System.err.println("My PID: " + javaPID);
            bashIn.close();

            bash = Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", "ps --ppid "+javaPID+" | grep ale | awk '{print $1}'"});
            bashIn = new BufferedReader(new InputStreamReader(bash.getInputStream()));
            alePID = bashIn.readLine();
            // @dbg System.err.println("Ale PID: " + alePID);
            bashIn.close();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        int javaUTime = 0;
        int aleUTime = 0;
        int totalTime = 0;

        long sleepCycle = 1000;
        long estimateCycle = sleepCycle * 30;
        long cycleStart = System.currentTimeMillis();

        double cpuRatio = 1.0;
        long deadTime = 0;
        
        try {
            while (!terminated) {
                long time = System.currentTimeMillis();

                if (time - cycleStart >= estimateCycle) {
                    cycleStart = time;
                    // Read in the number of cycles spent in Java
                    BufferedReader in =
                            new BufferedReader(new InputStreamReader(new FileInputStream("/proc/" + javaPID + "/stat")));
                    String[] tokens = in.readLine().split(" ");
                    int newJavaTime = Integer.parseInt(tokens[13]);
                    in.close();

                    // Now the cycles spent in ALE
                    in = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/" + alePID + "/stat")));
                    tokens = in.readLine().split(" ");
                    int newALETime = Integer.parseInt(tokens[13]);
                    in.close();

                    // Now the CPU time
                    in = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/stat")));
                    String line;
                    do {
                        line = in.readLine();
                        // This will probably fail if we only have one cpu... but then don't use this
                    } while (!line.startsWith("cpu0"));

                    tokens = line.split(" ");
                    int newSystemTime = 0;
                    for (int i = 1; i < tokens.length; i++) {
                        newSystemTime += Integer.parseInt(tokens[i]);
                    }

                    in.close();

                    double newRatio;

                    if (totalTime != 0) {
                        newRatio = (newALETime + newJavaTime - aleUTime - javaUTime + 0.0)
                                / (newSystemTime - totalTime);
                        // @dbg System.err.println("CPU " + newRatio+" " + cpuRatio);
                    } else {
                        newRatio = 1.0;
                    }

                    // The effective ratio at which we are running is a combination
                    //  of the speed at which we run (prior to throttling) and
                    //  the speed we actually run at. So that if are throttling and
                    //  get a ratio of 1.0 ... don't change anything!
                    // Also avoid dropping cpuRatio too far below 1, in case we are
                    //  running slow
                    if (cpuRatio * newRatio >= 0.5)
                        cpuRatio *= newRatio;
                    
                    if (cpuRatio <= 1.0) {
                        deadTime = 0;
                    } else {
                        deadTime = (long) (sleepCycle * (1.0 - 1.0 / cpuRatio));
                    }
                    javaUTime = newJavaTime;
                    aleUTime = newALETime;
                    totalTime = newSystemTime;
                }

                try {
                    // First we stop the Java agent for a bit (yes, I know, this is
                    //  bad)
                    parent.suspend();
                    Thread.sleep(deadTime);
                    parent.resume();
                    Thread.sleep(sleepCycle - deadTime);
                } catch (InterruptedException ex) {
                    parent.resume();
                }

            if (!parent.isAlive())
                terminate();
            }
        }
        catch (Exception e) {
            System.err.println ("Throttle failed.");
        }
    }

    public void terminate() {
        terminated = true;
    }
}
