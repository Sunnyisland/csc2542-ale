/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents;

import atariagents.io.Actions;
import atariagents.gui.ImageSequence;
import atariagents.screen.GrayscalePalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import atariagents.movie.MovieGenerator;
import java.awt.image.BufferedImage;
import java.io.File;
import marcgb.tools.config.Parameters;
import nips11.agents.data.Trajectory;
import nips11.agents.data.TrajectoryProvider;
import nips11.parameters.NIPSParameters;
import rlVizLib.general.ParameterHolder;

/**
 * This agent replays a trajectory and saves the resulting PNGs.
 *
 * @author Marc G. Bellemare
 */
public class TrajectoryToMovieAgent extends AbstractAgent {
    protected int numActions = Actions.numPlayerActions;
    
    protected int controlCounter;
    /** The action used to find the control region (we try each in turn) */
    protected int controlAction;
    protected Trajectory trajectory;
    protected TrajectoryProvider trajectoryProvider;

    /** Tools to display the control region */
    protected ScreenConverter CRConverter = new ScreenConverter(new GrayscalePalette());
    /** The image we want to display on the left side, if not using default display */
    protected ImageSequence leftImage;
    /** The image we want to display on the right side */
    protected ImageSequence rightImage;

    protected boolean makeMovie;
    protected MovieGenerator movieGenerator;

    protected int actionStepSize;
    
    protected int frameNumber = 0;
    /** To keep track of how long a training (or evaluation) cycle takes */
    protected long cycleStartTime;

    double score = 0;
    
    public TrajectoryToMovieAgent(ParameterHolder params) {
        super(params);

        // Parse parameters
        NIPSParameters.parseParameters(params);

        actionStepSize = NIPSParameters.actionStepSize;
        
        trajectoryProvider = new TrajectoryProvider(actionStepSize, numActions, params);
        trajectory = trajectoryProvider.getTrajectory();
        trajectory.initIteration();

        makeMovie = NIPSParameters.evalMakeMovie;
        if (makeMovie)
            movieGenerator = new MovieGenerator(params);
        else
            movieGenerator = null;
    }
    
    public static void addParameters(ParameterHolder params) {
        // Parent parameters
        AbstractAgent.addParameters(params);
        // Our parameters
        NIPSParameters.addParameters(params);
        TrajectoryProvider.addParameters(params);
        MovieGenerator.addParameters(params);
    }

    @Override
    public boolean wantsScreenData() {
        return true;
    }

    public boolean wantsRealtime() { return false; }
    
    public boolean wantsRamData() {
        return true;
    }

    public boolean wantsRLData() {
        return true;
    }

    public boolean wantsStandardSize() {
        return true;
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    protected void keyboardEvents() {
        super.keyboardEvents();
    }

    public boolean shouldTerminate() {
        if (!trajectory.hasNext())
            System.err.println ("SCORE "+score);
        return !trajectory.hasNext();
    }

    @Override
    public int selectAction() {
        int action = 0;

        if (!trajectory.hasNext())
            System.err.println ("WARNING: selectAction() called with no action left.");
        else {
            action = trajectory.next();
        }

        return action;
    }

    @Override
    public void observeImage(ScreenMatrix image) {
        frameNumber++;
        
        score += comm.getRLData().reward;
        
        saveFrame(image);
    }

    protected void saveFrame(ScreenMatrix frame) {
        BufferedImage img = converter.convert(frame);

        if (makeMovie)
            movieGenerator.record(new ImageSequence(img), true);
    }

    @Override
    public long getPauseLength() {
        return 0;
    }

    @Override
    public int specialAction() {
        return -1;
    }

    public static void main(String[] args) {
        ParameterHolder params = new ParameterHolder();
        TrajectoryToMovieAgent.addParameters(params);

        int argIndex = 0;
        String configFilename = null;

        int resumeNumber = 0;
        
        while (argIndex < args.length) {
            String a = args[argIndex];

            // Go into evaluation mode
            if(a.startsWith("--resume")) {
                String[] tokens = a.split("=");
                resumeNumber = Integer.parseInt(tokens[1]);
            }
            else if (!a.startsWith("-"))
                configFilename = a;

            argIndex++;
        }

        if (configFilename != null) {
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }

        params.setIntegerParam(NIPSParameters.PARAM_RESUME_AT, resumeNumber);
        
        TrajectoryToMovieAgent agent = new TrajectoryToMovieAgent(params);

        agent.run();
    }
}
