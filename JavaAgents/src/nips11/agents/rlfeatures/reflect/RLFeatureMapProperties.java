/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.rlfeatures.reflect;

import java.lang.reflect.Method;
import nips11.agents.rlfeatures.RLFeatureMap;

/** Uses reflection to determine properties of a given RLFeatureMap.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class RLFeatureMapProperties {

    /** Determines whether the given feature map wants RAM data by asking its
     *    wantsRAM() function. Defaults to false.
     * 
     * @param featureMap
     * @return
     */
    public static boolean wantsRAM(String featureMapClassname) {
        Class targetClass;
        try {
            targetClass = Class.forName(featureMapClassname);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        
        Method loaderMethod;

        try {
            loaderMethod = targetClass.getMethod("wantsRAM");
        }
        catch (NoSuchMethodException e) {
            return false;
        }

        try {
            Object returnValue = loaderMethod.invoke(null);

            return ((Boolean)returnValue).booleanValue();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /** Determines whether the given feature map wants RAM data by asking its
     *    wantsRAM() function. Defaults to true.
     *
     * @param featureMap
     * @return
     */
    public static boolean wantsScreen(String featureMapClassname) {
        Class targetClass;
        try {
            targetClass = Class.forName(featureMapClassname);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }

        Method loaderMethod;

        try {
            loaderMethod = targetClass.getMethod("wantsScreen");
        }
        catch (NoSuchMethodException e) {
            return true;
        }

        try {
            Object returnValue = loaderMethod.invoke(null);

            return ((Boolean)returnValue).booleanValue();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
