/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.rlfeatures;

import atariagents.screen.NTSCPalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import imagetools.ImageTools;
import java.awt.Color;
import java.awt.image.BufferedImage;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import rlVizLib.general.ParameterHolder;

/** A simplified version of the BASS feature set.
 *
 * @author marc
 */
public class BasicBASSFeatureMap extends BASSFeatureMap implements Displayable {
    protected final ScreenConverter converter = new ScreenConverter(new NTSCPalette());

    public BasicBASSFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params, colors);
    }

    @Override
    public int numPairwiseFeatures() {
        return 0;
    }

    @Override
    public void addPairwiseFeatures(FeatureList features) {
        return;
    }

    public static BasicBASSFeatureMap dynamicLoad(ParameterHolder params, FeatureColors colors) {
        return new BasicBASSFeatureMap(params, colors);
    }

    public int numDisplayModes() {
        return 1;
    }

    public BufferedImage getDisplay(ScreenMatrix screen, int mode) {
        switch (mode) {
            case 0:
                return getDisplayWithBackground(screen);
            case 1:
                return getDisplayWithoutBackground(screen);
            default:
                throw new IllegalArgumentException("Invalid screen display mode.");
        }
    }

    public BufferedImage getDisplayWithBackground(ScreenMatrix screen) {
        int tileWidth = screen.width / numColumns;
        int tileHeight = screen.height / numRows;

        BufferedImage img = converter.convert(screen);

        int gridColor = Color.WHITE.getRGB();

        ImageTools.drawGrid(img, tileWidth, tileHeight, gridColor);

        return img;
    }

    public BufferedImage getDisplayWithoutBackground(ScreenMatrix screen) {
        int tileWidth = screen.width / numColumns;
        int tileHeight = screen.height / numRows;

        BufferedImage img = converter.convert(screen);

        for (int x = 0; x < screen.width; x++)
            for (int y = 0; y < screen.height; y++)
                if (screen.matrix[x][y] == background.matrix[x][y])
                    img.setRGB(x, y, Color.BLACK.getRGB());

        int gridColor = Color.WHITE.getRGB();

        ImageTools.drawGrid(img, tileWidth, tileHeight, gridColor);

        return img;
    }
}
