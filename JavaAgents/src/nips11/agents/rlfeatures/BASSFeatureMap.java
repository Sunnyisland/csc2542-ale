/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.rlfeatures;

import atariagents.screen.NTSCPalette;
import atariagents.screen.SECAMPalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import imagetools.ImageTools;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import nips11.parameters.BASSParameters;
import rlVizLib.general.ParameterHolder;

/**
 *
 * @author marc
 */
public class BASSFeatureMap extends RLFeatureMap implements Displayable {
    protected FeatureColors colors;
    protected ScreenMatrix background;

    protected String backgroundFilename;

    protected int numColumns;
    protected int numRows;

    protected final ScreenConverter converter;

    public static void addParameters(ParameterHolder params) {
        BASSParameters.addParameters(params);
    }
    
    public BASSFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params);

        this.colors = colors;
        if (this.colors.numColors() <= 9) // SECAM
            converter = new ScreenConverter(new SECAMPalette());
        else
            converter = new ScreenConverter(new NTSCPalette());

        // Obtain parameters from config
        BASSParameters.parseParameters(params);

        backgroundFilename = BASSParameters.backgroundFilename;
        numColumns = BASSParameters.numColumns;
        numRows = BASSParameters.numRows;
        
        loadBackground();
    }

    protected final void loadBackground() {
        try {
            background = new ScreenMatrix(backgroundFilename);
        }
        catch (IOException e) {
            System.err.println ("Cannot load background.");
            throw new RuntimeException(e);
        }
    }

    public ScreenMatrix getBackground() {
        return background;
    }

    public int numSingleFeatures() {
        return numColumns * numRows * colors.numColors();
    }

    public int numPairwiseFeatures() {
        int s = numSingleFeatures();
        return s * (s-1) / 2;
    }
    
    /** Returns a quantization of the last screen. See Yavar Naddaf's thesis.
      */
    @Override
    public FeatureList getFeatures(FrameHistory history) {
        FeatureList features = new FeatureList();
        // First find out which colors are present in which blocks
        getSingleFeatures(features, history);

        addPairwiseFeatures(features);

        return features;
    }

    protected void getSingleFeatures(FeatureList features, FrameHistory history) {
        int numColors = colors.numColors();

        ScreenMatrix screen = history.getLastFrame(0);

        int blockWidth = screen.width / numColumns;
        int blockHeight = screen.height / numRows;

        int featuresPerBlock = numColors;
        int blockIndex = 0;
        
        // For each pixel block
        for (int by = 0; by < numRows; by++) {
            for (int bx = 0; bx < numColumns; bx++) {
                boolean[] hasColor = new boolean[numColors];
                int xo = bx * blockWidth;
                int yo = by * blockHeight;

                // Determine which colors are present; ignore background
                for (int x = xo; x < xo + blockWidth; x++)
                    for (int y = yo; y < yo + blockHeight; y++) {
                        int pixelColor = screen.matrix[x][y];
                        if (pixelColor != background.matrix[x][y])
                            hasColor[colors.encode(pixelColor)] = true;
                    }

                // Add all colors present to our feature set
                for (int c = 0; c < numColors; c++)
                    if (hasColor[c])
                        features.addFeature(c + blockIndex, 1.0);

                blockIndex += featuresPerBlock;
            }
        }
    }


    public void addPairwiseFeatures(FeatureList features) {
        // Now, we will add the pairwise combinations
        int activeFeatures = features.active.size();
        int totalFeatures = numSingleFeatures();

        LinkedList<Integer> doubles = new LinkedList<Integer>();
        
        // Iterate over all (index1,index2) pairs such that index1 < index2
        int listIndex = 0;
        for (FeatureList.Pair p1 : features.active) {
            int index1 = p1.index;

            if (listIndex == activeFeatures - 1) break;
            
            ListIterator<FeatureList.Pair> it2 = features.active.listIterator(listIndex+1);

            int offset1 = index1 * totalFeatures - index1 * (index1 + 1) / 2;

            while (it2.hasNext()) {
                FeatureList.Pair p2 = it2.next();
                
                // Add the (p1,p2) pair
                int pairIndex = (offset1 + p2.index - (index1 + 1));
                doubles.add(pairIndex);
            }

            listIndex++;
        }

        for (int index : doubles)
            features.addFeature(index + totalFeatures, 1.0);
    }

    @Override
    public int numFeatures() {
        return numSingleFeatures() + numPairwiseFeatures();
    }

    @Override
    public int historyLength() {
        return 1;
    }

    public static BASSFeatureMap dynamicLoad(ParameterHolder params, FeatureColors colors) {
        return new BASSFeatureMap(params, colors);
    }

    public int numDisplayModes() {
        return 1;
    }

    public BufferedImage getDisplay(ScreenMatrix screen, int mode) {
        int blockWidth = screen.width / numColumns;
        int blockHeight = screen.height / numRows;

        BufferedImage img = new BufferedImage(screen.width, screen.height, BufferedImage.TYPE_INT_RGB);

        int gridColor = Color.WHITE.getRGB();

        ImageTools.drawGrid(img, blockWidth, blockHeight, gridColor);

        int numColors = colors.numColors();

        // For each pixel block
        for (int by = 0; by < numRows; by++) {
            for (int bx = 0; bx < numColumns; bx++) {
                // First determine the colors that are present
                boolean[] hasColor = new boolean[256];

                int xo = bx * blockWidth;
                int yo = by * blockHeight;

                // Determine which colors are present; ignore background
                for (int x = xo; x < xo + blockWidth; x++)
                    for (int y = yo; y < yo + blockHeight; y++) {
                        int pixelColor = screen.matrix[x][y];
                        if (pixelColor != background.matrix[x][y])
                            hasColor[pixelColor] = true;
                    }

                // Generate an ordered list of colors
                ArrayList<Integer> colorList = new ArrayList<Integer>();
                
                for (int c = 0; c < hasColor.length; c++)
                    if (hasColor[c]) colorList.add(c);
                
                // Now fill in the block with colors
                if (!colorList.isEmpty()) {
                    int index = 0;
                    
                    for (int x = xo+1; x < xo + blockWidth; x++) {
                        for (int y = yo+1; y < yo + blockHeight; y++) {
                            int indexedColor = colorList.get(index);
                            // Convert indexed color to RGB
                            img.setRGB(x, y, converter.getPalette().get(indexedColor).getRGB());

                            index++;
                            if (index == colorList.size()) index = 0;
                        }
                    }
                }
            }
        }

        return img;
    }
}
