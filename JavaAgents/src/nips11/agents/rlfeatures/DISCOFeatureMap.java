/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.rlfeatures;

import aij.agents.disco.BlobObject;
import aij.agents.disco.ClassShape;
import aij.agents.disco.ClassShapeList;
import aij.agents.disco.FGPoint;
import aij.agents.rl.features.ClassInstancesMap;
import aij.agents.rl.features.HashlessTileCoder;
import atariagents.screen.NTSCPalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;
import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import nips11.parameters.BASSParameters;
import nips11.parameters.DISCOParameters;
import rlVizLib.general.ParameterHolder;

/** Computes DISCO features.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class DISCOFeatureMap extends RLFeatureMap implements Displayable {
    public static final int DISPLAY_FOREGROUND = 0;
    public static final int DISPLAY_OBJECTS = 1;
    public static final int NUM_DISPLAY_MODES = 2;

    /** Parameters */
    protected String backgroundFilename;
    protected String shapesFilename;

    protected int maxObjectVelocity;
    protected int maxNumInstances;
    protected int numTilings;
    protected int numTiles;

    protected final int screenWidth;
    protected final int screenHeight;
    
    protected ScreenMatrix background;
    protected ClassShapeList sortedShapes;

    protected ClassInstancesMap currentClassInstances;

    /** Working variables */
    protected ScreenMatrix foreground;
    protected LinkedList<FGPoint> foregroundPoints;

    /** For displaying stuff */
    protected ScreenMatrix labelledForeground;

    protected int tileCode2DSize;
    protected int tileCode4DSize;

    protected final HashlessTileCoder tileCoder2D, tileCoder4D;
    
    public final ScreenConverter converter = new ScreenConverter(new NTSCPalette());

    public DISCOFeatureMap(ParameterHolder params) {
        super(params);

        DISCOParameters.parseParameters(params);
        BASSParameters.parseParameters(params);

        backgroundFilename = BASSParameters.backgroundFilename;
        shapesFilename = DISCOParameters.shapesFilename;

        maxObjectVelocity = DISCOParameters.maxObjVelocity;
        maxNumInstances = DISCOParameters.maxInstancesOnScreen;
        numTilings = DISCOParameters.numTilings;
        numTiles = DISCOParameters.numTiles;

        loadBackground();
        loadShapes();

        screenWidth = background.width;
        screenHeight = background.height;
        
        tileCoder2D = new HashlessTileCoder(numTilings, numTiles, 2);
        tileCoder4D = new HashlessTileCoder(numTilings, numTiles, 4);
    }

    public static void addParameters(ParameterHolder params) {
        RLFeatureMap.addParameters(params);
        DISCOParameters.addParameters(params);
        BASSParameters.addParameters(params);
    }

    protected final void loadBackground() {
        try {
            background = new ScreenMatrix(backgroundFilename);
        }
        catch (IOException e) {
            System.err.println ("Cannot load background.");
            throw new RuntimeException(e);
        }
    }

    public final void loadShapes() {
        try {
            sortedShapes = new ClassShapeList(shapesFilename);
        }
        catch (IOException e) {
            System.err.println ("Cannot load shapes.");
            throw new RuntimeException(e);
        }
    }

    @Override
    public FeatureList getFeatures(FrameHistory history) {
        ScreenMatrix currentScreen = history.getLastFrame(0);
        // @todo optimize: keep instances around and test for pointer equality
        ScreenMatrix previousScreen = history.getLastFrame(1);

        currentClassInstances = getClassInstances(currentScreen);
        ClassInstancesMap previousClassInstances;
        if (previousScreen != null) {
            previousClassInstances = getClassInstances(previousScreen);
        }
        else
            previousClassInstances = null;

        if (previousClassInstances != null)
            calculateVelocities(currentClassInstances, previousClassInstances);
        else
            zeroVelocities(currentClassInstances);
        
        return getFeatures(currentClassInstances);
    }

    /** Based on ClassAgent::get_class_instances_on_screen() from ALE v0.1.
     * 
     * @param screen
     * @return
     */
    protected ClassInstancesMap getClassInstances(ScreenMatrix screen) {
        int numClasses = sortedShapes.numClasses();

        ClassInstancesMap classInstances = new ClassInstancesMap(numClasses);
        extractForeground(screen);

        int numInstances = 0;

        for (ClassShape cs : sortedShapes.getShapes()) {
            ScreenMatrix shape = cs.shape;

            int rowRangeStart = 0 - cs.firstOneY;
            int rowRangeEnd = shape.height - cs.firstOneY;
            int colRangeStart = 0 - cs.firstOneX;
            int colRangeEnd = shape.width - cs.firstOneX;
            int halfHeight = shape.height / 2;
            int halfWidth = shape.width / 2;

            for (ListIterator<FGPoint> it = foregroundPoints.listIterator(); it.hasNext(); ) {
                FGPoint p = it.next();
                int x = p.x;
                int y = p.y;

                // This point has been removed by another match; remove it from the list
                if (foreground.matrix[x][y] == 0) {
                    it.remove();
                    continue;
                }

                if (x + colRangeStart < 0 || x + colRangeEnd >= screen.width ||
                    y + rowRangeStart < 0 || y + rowRangeEnd >= screen.height)
                    continue;

                boolean shapeMatches = true;

                for (int c = 0; shapeMatches && c < shape.width; c++)
                    for (int r = 0; r < shape.height; r++) {
                        if (foreground.matrix[c + x + colRangeStart][r + y + rowRangeStart] !=
                                shape.matrix[c][r]) {
                            shapeMatches = false;
                            break;
                        }
                    }

                if (shapeMatches) {
                    numInstances++;
                    
                    int centerX = x + colRangeStart + halfWidth;
                    int centerY = y + rowRangeStart + halfHeight;

                    BlobObject newObject = new BlobObject(centerX, centerY);
                    newObject.i_instance_of_class = cs.instanceOfClass;

                    classInstances.add(cs.instanceOfClass, newObject);

                    // Remove the found shape from the foreground
                    for (int c = 0; c < shape.width; c++)
                        for (int r = 0; r < shape.height; r++) {
                            int xx = c + x + colRangeStart;
                            int yy = r + y + rowRangeStart;

                            if (foreground.matrix[xx][yy] == 1)
                                labelledForeground.matrix[xx][yy] = (cs.instanceOfClass+1);
                            
                            foreground.matrix[xx][yy] = 0;
                        }
                }

                if (maxNumInstances > 0 && numInstances >= maxNumInstances)
                    break;
            }

            if (maxNumInstances > 0 && numInstances >= maxNumInstances) {
                break;
            }
        }

        return classInstances;
    }

    protected void extractForeground(ScreenMatrix screen) {
        if (foregroundPoints == null) {
            foregroundPoints = new LinkedList<FGPoint>();
            foreground = new ScreenMatrix(screen.width, screen.height);
            labelledForeground = new ScreenMatrix(screen.width, screen.height);
        }
        else
            // @todo could optimize by reusing leftover data structures
            foregroundPoints.clear();

        for (int x = 0; x < screen.width; x++)
            for (int y = 0; y < screen.height; y++) {
                labelledForeground.matrix[x][y] = 0;
                
                if (screen.matrix[x][y] != background.matrix[x][y]) {
                    foreground.matrix[x][y] = 1;
                    foregroundPoints.add(new FGPoint(x, y));
                }
                else {
                    foreground.matrix[x][y] = 0;
                }
            }
    }

    protected void calculateVelocities(ClassInstancesMap current, ClassInstancesMap previous) {
        int numClasses = sortedShapes.numClasses();

        for (int classIndex = 0; classIndex < numClasses; classIndex++) {
            for (BlobObject currentObject : current.getInstancesOf(classIndex)) {
                int closestDistance = Integer.MAX_VALUE;

                for (BlobObject previousObject : previous.getInstancesOf(classIndex)) {
                    int dist = currentObject.calc_distance_max_xy(previousObject);

                    assert(dist >= 0);
                    if (dist <= maxObjectVelocity && dist < closestDistance) {
                        // @todo can optimize by waiting till the end to compute
                        currentObject.i_velocity_x = currentObject.i_center_x - previousObject.i_center_x;
                        currentObject.i_velocity_y = currentObject.i_center_y - previousObject.i_center_y;
                        closestDistance = dist;
                    }
                }
            }
        }
    }

    /** Sets all velocities to 0; used on the first step.
     * 
     * @param current
     */
    protected void zeroVelocities(ClassInstancesMap current) {
        int numClasses = sortedShapes.numClasses();

        for (int classIndex = 0; classIndex < numClasses; classIndex++) {
            for (BlobObject currentObject : current.getInstancesOf(classIndex)) {
                currentObject.i_velocity_x = 0;
                currentObject.i_velocity_y = 0;
            }
        }
    }

    protected FeatureList getFeatures(ClassInstancesMap instancesMap) {
        // @todo usual optimization trick of reusing features
        ArrayList<Integer> unsortedFeatures = new ArrayList<Integer>();
        FeatureList features = new FeatureList();
        
        int numClasses = sortedShapes.numClasses();

        int featureOffset = 0;
        // First indicate the presence of each class
        for (int c = 0; c < numClasses; c++) {
            if (instancesMap.getInstancesOf(c).size() > 0) {
                features.addFeature(c, 1.0);
            }
        }

        featureOffset += numClasses;

        int[] indices = new int[numTilings];
        double[] values = new double[2];
        
        // Now add the absolute position of instances
        for (int c = 0; c < numClasses; c++) {
            // @todo optimize
            boolean[] hasTile2d = new boolean[tileCode2DSize()];
            
            ArrayList<BlobObject> instances = instancesMap.getInstancesOf(c);

            // Obtain a tile code for each instance's position
            for (BlobObject object : instances) {
                values[0] = ((double)object.i_center_x) / screenWidth;
                values[1] = ((double)object.i_center_y) / screenHeight;
                
                tileCoder2D.tile(indices, values);

                // OR the resulting feature vector with our big vector
                for (int i = 0; i < indices.length; i++) {
                    int index = indices[i];
                    // Here we ignore duplicates; this is unlike Yavar Naddaf's
                    //  original implementation
                    if (!hasTile2d[index]) {
                        int featureIndex = index + featureOffset;

                        unsortedFeatures.add(featureIndex);
                        hasTile2d[index] = true;
                    }
                }
            }

            featureOffset += tileCode2DSize();
        }

        values = new double[4];
        // Add relative positions and velocities
        for (int c = 0; c < numClasses; c++) {
            // @todo optimize
            boolean[] hasTile4d = new boolean[tileCode4DSize()];

            ArrayList<BlobObject> instances = instancesMap.getInstancesOf(c);

            // Obtain a tile code for each instance's position
            int objectIndex = 0;
            for (BlobObject object : instances) {
                if (objectIndex == instances.size() - 1) break;
                
                for (ListIterator<BlobObject> it = instances.listIterator(objectIndex+1);
                    it.hasNext(); ) {
                    BlobObject otherObject = it.next();

                    values[0] = (object.i_center_x - otherObject.i_center_x) / ((double)screenWidth);
                    values[1] = (object.i_center_y - otherObject.i_center_y) / ((double)screenHeight);
                    values[2] = (object.i_velocity_x - otherObject.i_velocity_x) / ((double)screenWidth);
                    values[3] = (object.i_velocity_y - otherObject.i_velocity_y) / ((double)screenHeight);

                    tileCoder4D.tile(indices, values);

                    // OR the resulting feature vector with our big vector
                    for (int i = 0; i < indices.length; i++) {
                        int index = indices[i];
                        // Here we ignore duplicates; this is unlike Yavar Naddaf's
                        //  original implementation
                        if (!hasTile4d[index]) {
                            int featureIndex = index + featureOffset;

                            unsortedFeatures.add(featureIndex);
                            hasTile4d[index] = true;
                        }
                    }
                }

                objectIndex++;
            }

            featureOffset += tileCode4DSize();
        }

        // Sort the indices so that we return a sorted feature set
        Collections.sort(unsortedFeatures);

        for (int index : unsortedFeatures) {
            features.addFeature(index, 1.0);
        }
        
        return features;
    }

    public int tileCode2DSize() {
        if (tileCode2DSize == 0)
            tileCode2DSize = numTilings * numTiles * numTiles;

        return tileCode2DSize;
    }

    public int tileCode4DSize() {
        if (tileCode4DSize == 0)
            tileCode4DSize = numTilings * numTiles * numTiles * numTiles * numTiles;

        return tileCode4DSize;
    }

    public int numClasses() {
        return sortedShapes.numClasses();
    }

    @Override
    public int numFeatures() {
        int nc = numClasses();

        return (1 + tileCode2DSize()) * nc + tileCode4DSize() * (nc * (nc + 1)) / 2;
    }

    @Override
    public int historyLength() {
        return 2;
    }
    
    public int numDisplayModes() {
        return NUM_DISPLAY_MODES;
    }

    public BufferedImage getDisplay(ScreenMatrix screen, int mode) {
        switch (mode) {
            case DISPLAY_FOREGROUND:
                return getDisplayForeground(screen);
            case DISPLAY_OBJECTS:
                return getDisplayObjects(screen);
            default:
                throw new IllegalArgumentException("Invalid display mode: "+mode);

        }
    }

    /** Creates a black and white image displaying the foreground. */
    protected BufferedImage getDisplayForeground(ScreenMatrix screen) {
        BufferedImage image =
                new BufferedImage(screen.width, screen.height, BufferedImage.TYPE_INT_RGB);
        
        if (foreground == null)
            return image;
        else {
            for (int x = 0; x < foreground.width; x++)
                for (int y = 0; y < foreground.height; y++) {
                    Color c;
                    // Unlabelled foreground
                    if (foreground.matrix[x][y] == 1)
                        // @dbg AAAI - display as black c = Color.WHITE;
                        c = Color.BLACK;
                    // Background
                    else if (labelledForeground.matrix[x][y] == 0)
                        c = Color.BLACK;
                    else // Determine color based on class
                        c = classColor(labelledForeground.matrix[x][y] - 1);

                    image.setRGB(x, y, c.getRGB());
                }
        }

        return image;
    }

    protected BufferedImage getDisplayObjects(ScreenMatrix screen) {
        BufferedImage image = converter.convert(screen);

        for (int c = 0; c < numClasses(); c++) {
            Color classColor = classColor(c);
            for (BlobObject object : currentClassInstances.getInstancesOf(c)) {
                int x = object.i_center_x;
                int y = object.i_center_y;

                // draw a single pixel on the object center
                image.setRGB(x, y, classColor.getRGB());
            }
        }

        return image;
    }

    protected final Color[] classColors =
            new Color[] {Color.RED, Color.GREEN, Color.BLUE,
        Color.CYAN, Color.YELLOW, Color.ORANGE, Color.MAGENTA, Color.PINK,
        Color.LIGHT_GRAY, Color.DARK_GRAY};

    protected Color classColor(int classIndex) {
        int numColors = classColors.length;
        return classColors[classIndex % numColors];
    }
    
    public static DISCOFeatureMap dynamicLoad(ParameterHolder params, FeatureColors colors) {
        return new DISCOFeatureMap(params);
    }
}
