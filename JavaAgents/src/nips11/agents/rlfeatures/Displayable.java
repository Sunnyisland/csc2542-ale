/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.rlfeatures;

import atariagents.screen.ScreenMatrix;
import java.awt.image.BufferedImage;

/** A FeatureMap which has displayable information.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public interface Displayable {
    public int numDisplayModes();
    public BufferedImage getDisplay(ScreenMatrix screen, int mode);
}
