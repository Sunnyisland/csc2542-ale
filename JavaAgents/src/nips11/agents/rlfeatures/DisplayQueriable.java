/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.rlfeatures;

/** Used for feature maps where we can query information about the features via
 *   pixel coordinates.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public interface DisplayQueriable {
    public int getFeatureIndex(int x, int y, int color);
}
