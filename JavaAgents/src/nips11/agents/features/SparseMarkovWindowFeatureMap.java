/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.features;

import atariagents.screen.ScreenMatrix;
import java.util.ListIterator;
import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.features.colors.SECAMColors;
import nips11.agents.models.FeatureList;
import nips11.parameters.WindowFeatureMapParameters;
import rlVizLib.general.ParameterHolder;

/** Creates a feature set composed of a series of windows looking back in time.
 *  For example, with a historyLength of 3 and a radius of 1, with an increment of
 *  1, the window sizes are 3x3, 5x5, 7x7 (for times t, t-1, t-2 respectively).
 *
 *  Uses SECAM encoding, which reduces the colors to 0-7.
 * 
 * @author marc
 */
public abstract class SparseMarkovWindowFeatureMap extends AbstractSparseFeatureMap {

    /** The length of history considered */
    protected int historyLength;
    /** The radius of the window used for obtaining features */
    protected int radius;
    protected int radiusIncrement;

    protected boolean hasInit = false;

    public static void addParameters(ParameterHolder params) {
        AbstractSparseFeatureMap.addParameters(params);
        WindowFeatureMapParameters.addParameters(params);
    }
    
    public SparseMarkovWindowFeatureMap(ParameterHolder params) {
        super(params, new SECAMColors(true));

        WindowFeatureMapParameters.parseParameters(params);

        this.historyLength = WindowFeatureMapParameters.historyLength;
        this.radius = WindowFeatureMapParameters.windowRadius;
        this.radiusIncrement = WindowFeatureMapParameters.windowRadiusIncrement;
    }

    public SparseMarkovWindowFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params, colors);

        WindowFeatureMapParameters.parseParameters(params);

        this.historyLength = WindowFeatureMapParameters.historyLength;
        this.radius = WindowFeatureMapParameters.windowRadius;
        this.radiusIncrement = WindowFeatureMapParameters.windowRadiusIncrement;
    }

    public abstract void encodeValue(FeatureList features, int blockIndex, int value);
    public abstract int encodeIndex(int blockIndex, int value);
    public abstract int numFeaturesPerValue();
    
    public int numFeatures() {
        int l = historyLength;
        int r = radius;
        int d = radiusIncrement;
        int b = numFeaturesPerValue();

        // Do the math
        return b * (l * ((1 + 4 * r * (1 + r)) + 2 * d * (l - 1) * (1 + 2 * r)) +
                4 * d * d * l * (l - 1) * (2 * l - 1) / 6);
    }

    public int historyLength() {
        return this.historyLength;
    }

    public void init() {
        hasInit = true;
    }

    @Override
    public FeatureList featuresAt(FrameHistory history, int x, int y) {
        FeatureList features = new FeatureList();
        featuresAt(features, history, x, y);

        return features;
    }

    /** Rather than re-creating FeatureList.Pair's all the time, we will overwrite
     *   the indices of old ones.
     * 
     * @param features
     * @param history
     * @param x
     * @param y
     */
    protected void featuresAt(FeatureList features, FrameHistory history, int x, int y) {
        if (!hasInit) init();

        // @dbg disabled for NIPS boolean doneReplacing = false;

        // @dbg for NIPS
        if (features.active.isEmpty()) {
            int n = numFeatures();
            int b = numFeaturesPerValue();
            int activeFeatures = n/b;

            for (int i = 0; i < activeFeatures; i++)
                features.addFeature(0, 1.0);
        }

        ListIterator<FeatureList.Pair> existingIt = features.active.listIterator();

        // Feature index in the feature vector
        int index = 0;

        // The (increasing) window radius
        int r = radius;
        int b = numFeaturesPerValue();

        // We will add that many features
        for (int t = 0; t < historyLength; t++) {            
            ScreenMatrix screen = history.getLastFrame(t);
            int w = screen.width;
            int h = screen.height;

            for (int xx = x - r; xx <= x + r; xx++) {
                boolean xOutside = (xx < 0 || xx >= w);

                for (int yy = y - r; yy <= y + r; yy++) {
                    // Set value to 0 if missing
                    int value;
                    
                    if (xOutside || yy < 0 || yy >= h) {
                        value = colors.getOutsideColor();
                    }
                    else
                        // Better already would be to use grayscale (first palette convert)
                        value = colors.encode(screen.matrix[xx][yy]);

                    int featureIndex = encodeIndex(index, value);

                    // Replace an existing feature
                    /* @dbg disabled for NIPS
                       if (!doneReplacing && existingIt.hasNext()) { */
                        FeatureList.Pair f = existingIt.next();
                        f.index = featureIndex;
                    /* @dbg disabled for NIPS
                    }
                    else {
                        features.addFeature(featureIndex, 1.0);
                        doneReplacing = true;
                    } */

                    index += b;
                }
            }

            // Grow the window radius
            r += radiusIncrement;
        }
    }

    // @dbg-ish
    protected int[] featuresArray;

    public int[] arrayFeaturesAt(FrameHistory history, int x, int y) {
        if (!hasInit) init();

        // @dbg disabled for NIPS boolean doneReplacing = false;

        // @dbg for NIPS
        if (featuresArray == null) {
            int n = numFeatures();
            int b = numFeaturesPerValue();
            int activeFeatures = n/b;

            featuresArray = new int[activeFeatures];
        }

        // Feature index in the feature vector
        int index = 0;
        int arrayIndex = 0;

        // The (increasing) window radius
        int r = radius;
        int b = numFeaturesPerValue();

        // We will add that many features
        for (int t = 0; t < historyLength; t++) {
            ScreenMatrix screen = history.getLastFrame(t);
            int w = screen.width;
            int h = screen.height;

            for (int xx = x - r; xx <= x + r; xx++) {
                boolean xOutside = (xx < 0 || xx >= w);

                for (int yy = y - r; yy <= y + r; yy++) {
                    // Set value to 0 if missing
                    int value;

                    if (xOutside || yy < 0 || yy >= h) {
                        value = colors.getOutsideColor();
                    }
                    else
                        // Better already would be to use grayscale (first palette convert)
                        value = colors.encode(screen.matrix[xx][yy]);

                    int featureIndex = encodeIndex(index, value);

                    featuresArray[arrayIndex++] = featureIndex;

                    index += b;
                }
            }

            // Grow the window radius
            r += radiusIncrement;
        }

        if (featuresArray.length != arrayIndex)
            throw new RuntimeException("Eep.");
        
        return featuresArray;
    }
}
