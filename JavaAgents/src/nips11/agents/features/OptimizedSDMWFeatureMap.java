/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.features;

import atariagents.screen.ScreenMatrix;
import java.util.ArrayList;
import java.util.ListIterator;
import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.features.colors.NTSCColors;
import nips11.agents.features.colors.SECAMColors;
import nips11.agents.models.FeatureList;
import rlVizLib.general.ParameterHolder;

/** An optimized version of the superclass.
 *
 * @author marc
 */
public class OptimizedSDMWFeatureMap extends SparseDiscretizedMarkovWindowFeatureMap {
    protected FeatureList features;
    protected int imageWidth;
    protected int imageHeight;

    protected int lastX, lastY;

    protected final boolean usesNTSCColors;

    public OptimizedSDMWFeatureMap(ParameterHolder params) {
        super(params);

        usesNTSCColors = false;
    }

    public OptimizedSDMWFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params, colors);

        if (colors instanceof NTSCColors)
            usesNTSCColors = true;
        else if (colors instanceof SECAMColors)
            usesNTSCColors = false;
        else
            throw new UnsupportedOperationException("Optimized for either NTSC or SECAM colors - "
                    +"no other color support available.");
    }

    protected void init(FrameHistory history) {
        ScreenMatrix screen = history.getLastFrame(0);
        imageWidth = screen.width;
        imageHeight = screen.height;
    }

    @Override
    public FeatureList featuresAt(FrameHistory history, int x, int y) {
        if (imageWidth == 0) init(history);

        // Optimize if shifting
        if (features != null && y == lastY && x == lastX + 1) {
            shiftFeaturesX(history);
        }
        // Replace if possible
        else if (features != null) {
            featuresAt(features, history, x, y);
        }
        // Finally create a new set if no choice
        else {
            // Use array list - not so great if shifting, but for now we are
            //  not shifting because we use BlockROC (MGB)
            // @todo fix this - parameter?
            features = new FeatureList(false);
            featuresAt(features, history, x, y);
        }

        lastX = x;
        lastY = y;
        
        return features;
    }

    // @dbg
    public void compareFeatures(FeatureList fullSet) {
        if (fullSet.active.size() != features.active.size()) {
            System.err.println ("Sizes differ "+fullSet.active.size()+" "+features.active.size());
            System.exit(-1);
        }
        ListIterator<FeatureList.Pair> it1 = fullSet.active.listIterator();
        ListIterator<FeatureList.Pair> it2 = features.active.listIterator();

        boolean differs = false;
        
        // Walk both lists and compare
        while (it1.hasNext()) {
            FeatureList.Pair p1 = it1.next();
            FeatureList.Pair p2 = it2.next();

            if (p1.index != p2.index) {
                differs = true;
                System.err.println ("Features differ");
            }
        }

        if (differs)
            System.exit(-1);
    }

   /** Shifts the current feature set by 1 pixel in x. Assumes one active feature per
     *    pixel.
     * 
     */
    public void shiftFeaturesX(FrameHistory history) {
        int r = radius;
        int b = numFeaturesPerValue();

        ListIterator<FeatureList.Pair> it = features.active.listIterator();

        int index = 0;
        
        for (int t = 0; t < historyLength; t++) {
            int h = 2 * r + 1;
            ScreenMatrix screen = history.getLastFrame(t);
            
            // Assume the pixels were computed in *column* order, so that features
            //  0 to h are to be removed
            for (int i = 0; i < h; i++) {
                it.next();
                it.remove();
            }

            // There are h x (h-1) remaining pixels for this time step
            int pixelsInBlock = h * (h - 1);
            int shift = h * b;

            // Now shift all the features for this time step by h * b (a column)
            for (int i = 0; i < pixelsInBlock; i++) {
                FeatureList.Pair fp = it.next();
                fp.index -= shift;
            }

            index += pixelsInBlock * b;
            
            // Finally add the rightmost column
            int xx = lastX + r + 1;
            boolean xOutOfBounds = (xx >= imageWidth);
            
            for (int yy = lastY - r; yy <= lastY + r; yy++) {
                int value;
                
                if (xOutOfBounds || yy < 0 || yy >= imageHeight) {
                    value = colors.getOutsideColor();
                }
                else
                    value = colors.encode(screen.matrix[xx][yy]);

                // Add this feature
                FeatureList.Pair pair = features.new Pair(index + value, 1.0);
                it.add(pair);

                index += b;
            }

            r += radiusIncrement;
        }

        lastX++;
    }

   /** Shifts the current feature set by 'delta' pixels in x. Assumes one active feature per
     *    pixel.
     *
     */
    public void shiftFeaturesX(FrameHistory history, int delta) {
        int r = radius;
        int b = numFeaturesPerValue();

        ListIterator<FeatureList.Pair> it = features.active.listIterator();

        int index = 0;

        /** Upper bound on how many features we shift for every frame in the history */
        int removedPixelCount = (2 * (r + historyLength * radiusIncrement) + 1);

        ArrayList<FeatureList.Pair> removed = new ArrayList<FeatureList.Pair>(removedPixelCount);
        
        for (int t = 0; t < historyLength; t++) {
            int h = 2 * r + 1;
            ScreenMatrix screen = history.getLastFrame(t);

            if (t != 0) removed.clear();
            
            // Assume the pixels were computed in *column* order, so that features
            //  0 to h are to be removed
            for (int i = 0; i < h * delta; i++) {
                removed.add(it.next());
                it.remove();
            }

            // There are h x (h-delta) remaining pixels for this time step
            int pixelsInBlock = h * (h - delta);
            int shift = h * b * delta;

            // Now shift all the features for this time step by h * b (a column)
            for (int i = 0; i < pixelsInBlock; i++) {
                FeatureList.Pair fp = it.next();
                fp.index -= shift;
            }

            index += pixelsInBlock * b;

            ListIterator<FeatureList.Pair> reuseIt = removed.listIterator();
            
            // Finally add the rightmost columns, reusing the feature classes
            for (int xx = lastX + r + 1; xx <= lastX + r + delta; xx++) {
                boolean xOutOfBounds = (xx >= imageWidth);

                for (int yy = lastY - r; yy <= lastY + r; yy++) {
                    int value;

                    if (xOutOfBounds || yy < 0 || yy >= imageHeight) {
                        value = colors.getOutsideColor();
                    }
                    else
                        value = colors.encode(screen.matrix[xx][yy]);

                    // Assume we have a next - otherwise, bad
                    FeatureList.Pair pair = reuseIt.next();
                    pair.index = index + value;

                    // Add this feature
                    it.add(pair);

                    index += b;
                }
            }

            r += radiusIncrement;
        }

        lastX += delta;
    }

    /** Optimized version for NIPS paper.
     *   @todo cleanup
     * 
     * @param features
     * @param history
     * @param x
     * @param y
     */
    @Override
    protected void featuresAt(FeatureList features, FrameHistory history, int x, int y) {
        if (!hasInit) init();

        if (features.active.isEmpty()) {
            int n = numFeatures();
            int b = numFeaturesPerValue();
            int activeFeatures = n/b;

            for (int i = 0; i < activeFeatures; i++)
                features.addFeature(0, 1.0);
        }

        ListIterator<FeatureList.Pair> existingIt = features.active.listIterator();

        // Feature index in the feature vector
        int index = 0;

        // The (increasing) window radius
        int r = radius;
        int b = numFeaturesPerValue();

        int outsideColor = colors.getOutsideColor();

        int colorMask = (usesNTSCColors? 0xFF : 0x0F);
        
        // We will add that many features
        for (int t = 0; t < historyLength; t++) {
            ScreenMatrix screen = history.getLastFrame(t);
            // A hack to ensure we can run this with a history length smaller than
            //  the required number
            if (screen == null) continue;
            
            int w = screen.width;
            int h = screen.height;

            boolean hasBounds = (x - r < 0) || (y - r < 0) || 
                        (x + r >= w) || (y + r >= h);
            
            for (int xx = x - r; xx <= x + r; xx++) {
                boolean xOutside = hasBounds && (xx < 0 || xx >= w);

                for (int yy = y - r; yy <= y + r; yy++) {
                    // Set value to 0 if missing
                    int value;

                    if (hasBounds && (xOutside || yy < 0 || yy >= h)) {
                        value = outsideColor;
                    }
                    else
                        value = (screen.matrix[xx][yy] & colorMask) >> 1;

                    // NIPS: Raw encoding
                    int featureIndex = index + value;

                    // Replace an existing feature
                    FeatureList.Pair f = existingIt.next();
                    f.index = featureIndex;

                    index += b;
                }
            }

            // Grow the window radius
            r += radiusIncrement;
        }
    }

    @Override
    public int[] arrayFeaturesAt(FrameHistory history, int x, int y) {
        if (!hasInit) init();

        // @dbg disabled for NIPS boolean doneReplacing = false;

        // @dbg for NIPS
        if (featuresArray == null) {
            int n = numFeatures();
            int b = numFeaturesPerValue();
            int activeFeatures = n/b;

            featuresArray = new int[activeFeatures];
        }

        // Feature index in the feature vector
        int index = 0;
        int arrayIndex = 0;

        // The (increasing) window radius
        int r = radius;
        int b = numFeaturesPerValue();

        int outsideColor = colors.getOutsideColor();

        int colorMask = (usesNTSCColors? 0xFF : 0x0F);

        // We will add that many features
        for (int t = 0; t < historyLength; t++) {
            ScreenMatrix screen = history.getLastFrame(t);
            // A hack to ensure we can run this with a history length smaller than
            //  the required number
            if (screen == null) continue;

            int w = screen.width;
            int h = screen.height;

            boolean hasBounds = (x - r < 0) || (y - r < 0) ||
                        (x + r >= w) || (y + r >= h);

            for (int xx = x - r; xx <= x + r; xx++) {
                boolean xOutside = hasBounds && (xx < 0 || xx >= w);

                for (int yy = y - r; yy <= y + r; yy++) {
                    // Set value to 0 if missing
                    int value;

                    if (hasBounds && (xOutside || yy < 0 || yy >= h)) {
                        value = outsideColor;
                    }
                    else
                        value = (screen.matrix[xx][yy] & colorMask) >> 1;

                    // NIPS: Raw encoding
                    int featureIndex = index + value;

                    // Replace an existing feature
                    featuresArray[arrayIndex++] = featureIndex;

                    index += b;
                }
            }

            // Grow the window radius
            r += radiusIncrement;
        }

        if (featuresArray.length != arrayIndex)
            throw new RuntimeException("Eep.");

        return featuresArray;
    }
}
