/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.features;

import nips11.agents.data.FrameHistory;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import rlVizLib.general.ParameterHolder;

/**
 * Maps a given point (x,y) to a double[] feature set.
 * 
 * @author marc
 */
public abstract class AbstractSparseFeatureMap {
    protected final FeatureColors colors;

    public static void addParameters(ParameterHolder params) {
    }

    public AbstractSparseFeatureMap(ParameterHolder params, FeatureColors colors) {
        this(colors);
    }
    
    public AbstractSparseFeatureMap(FeatureColors colors) {
        this.colors = colors;
    }

    /** Returns a feature vector for this location. No guarantee is made that
     *   the returned vector can be safely kept (it may be reused).
     * @param sample
     * @param x
     * @param y
     * @return
     */
    public abstract FeatureList featuresAt(FrameHistory sample, int x, int y);
    public abstract int numFeatures();

    /** Returns the history length required by the feature set. Any value below
     *   0 is interpreted as 1 (current time step only).
     * 
     * @return
     */
    public abstract int historyLength();

    public abstract int[] arrayFeaturesAt(FrameHistory sample, int x, int y);
}
