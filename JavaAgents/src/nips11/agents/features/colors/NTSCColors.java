/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.features.colors;

/**
 * A class that maps the 0-255 interval to 0-127 (NTSC palette).
 * 
 *
 * @author marc
 */
public class NTSCColors implements FeatureColors {
    protected final boolean useOutsideColor;

    public NTSCColors(boolean useOutsideColor) {
        this.useOutsideColor = useOutsideColor;
    }

    public int encode(int index) {
        return index >> 1;
    }

    public int numColors() {
        return (useOutsideColor ? 129 : 128);
    }

    public int getOutsideColor() {
        if (useOutsideColor) return 128;
        // Black is the outside color when not using a special index
        else return 0;
    }

}
