/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.features.colors;

import atariagents.screen.*;
import java.awt.Color;

/**
 * A simple class mapping a NTSC index to a SECAM index between 0-7.
 * @author marc
 */
public class SECAMColors implements FeatureColors {
    protected final boolean useOutsideColor;

    public SECAMColors(boolean useOutsideColor) {
        this.useOutsideColor = useOutsideColor;
    }

    public int encode(int colorIndex) {
        // Map to a 4-bit interval, divide by 2
        return (colorIndex & 0xF) >> 1;
    }

    public int numColors() {
        return (useOutsideColor ? 9 : 8);
    }
    
    public int getOutsideColor() {
        // Special outside color
        if (useOutsideColor) return 8;
        // Or black
        else return 0;
    }
}
