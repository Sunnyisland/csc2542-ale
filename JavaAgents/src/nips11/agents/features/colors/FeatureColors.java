/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.features.colors;

/** The set of colors used to generate features.
 *
 * @author marc
 */
public interface FeatureColors {
    /** Encode a 0-255 index into a lower-range color index */
    public int encode(int index);

    public int numColors();

    /** Returns the color used to represent outside pixels, e.g. pixels outside
     *    of the image.
     * 
     * @return
     */
    public int getOutsideColor();
}
