/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.features;

import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import rlVizLib.general.ParameterHolder;

/** Creates a feature set composed of a series of windows looking back in time.
 *  For example, with a historyLength of 3 and a radius of 1, with an increment of
 *  1, the window sizes are 3x3, 5x5, 7x7 (for times t, t-1, t-2 respectively).
 *
 * @author marc
 */
public class SparseDiscretizedMarkovWindowFeatureMap extends SparseMarkovWindowFeatureMap {
    public SparseDiscretizedMarkovWindowFeatureMap(ParameterHolder params) {
        super(params);
    }

    public SparseDiscretizedMarkovWindowFeatureMap(ParameterHolder params, FeatureColors colors) {
        super(params, colors);
    }

    /** Set exactly one bit to 1 */
    public void encodeValue(FeatureList features, int blockIndex, int value) {
        // Trivial encoding: set the 'value'-th bit to 1 in the block
        features.addFeature(blockIndex + value);
    }

    /** Returns the index of the feature to be encoded, assuming binary features.
     * 
     * @param blockIndex
     * @param value
     * @return
     */
    public int encodeIndex(int blockIndex, int value) {
        return blockIndex + value;
    }

    public int numFeaturesPerValue() {
        return colors.numColors();
    }

}
