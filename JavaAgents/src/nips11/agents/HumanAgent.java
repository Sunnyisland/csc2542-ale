/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents;

import atariagents.io.ALEPipes;
import atariagents.io.Actions;
import atariagents.io.RLData;
import atariagents.screen.ScreenMatrix;
import java.io.File;
import java.text.NumberFormat;
import marcgb.tools.config.Parameters;
import nips11.agents.data.Trajectory;
import nips11.agents.data.TrajectorySet;
import nips11.parameters.NIPSParameters;
import rlVizLib.general.ParameterHolder;

/** An 'agent' meant to be controlled by a human. Used to collect expert
 *   trajectories and play the game.
 *
 * @author marc
 */
public class HumanAgent extends AbstractAgent {
    /** Time when reset was last pressed */
    protected long startTime;

    protected Trajectory currentTrajectory;
    protected TrajectorySet trajectories;

    protected String trajectoriesFilename;

    protected int frameNumber;
    
    protected boolean terminalObserved;

    protected int framesPerStep = 5;
    protected int frameCount = 0;
    protected int currentAction = -1;

    protected double epsilon = 0.0;

    protected double score = 0;
    
    protected static final boolean resetOnTerminal = true;

    public HumanAgent(ParameterHolder params) {
        super(params);

        NIPSParameters.parseParameters(params);

        // Sanity check
        if (!NIPSParameters.useGui) {
            throw new IllegalArgumentException("Humans need graphics - set use-display=true.");
        }

        trajectoriesFilename = NIPSParameters.trajectoriesFilename;

        trajectories = new TrajectorySet();
        newTrajectory();
    }

    public static void addParameters(ParameterHolder params) {
        NIPSParameters.addParameters(params);
    }

    public boolean wantsRamData() {
        return true;
    }

    public boolean wantsRLData() {
        return true;
    }

    public boolean wantsStandardSize() {
        return true;
    }

    @Override
    public boolean wantsRealtime() {
        // we want real-time play if a human is running the show
        return true;
    }
    
    public boolean shouldTerminate() {
        // @todo add key?
        return false;
    }

    @Override
    public long getPauseLength() {
        return 0;
    }

    @Override
    public int selectAction() {
        frameCount++;

        if (currentAction >= 0 && frameCount <= framesPerStep) {
            currentTrajectory.addAction(currentAction);
            return currentAction;
        }

        frameCount = 1;
        int action;

        // Epsilon-greedy!
        if (Math.random() < epsilon)
            action = (int)(Math.random() * Actions.numPlayerActions);
        else
            action = ui.getKeyboardAction();

        // When reset is pressed, start a new trajectory
        if ((resetOnTerminal && terminalObserved)|| action == Actions.map("reset")) {
            System.err.println ("EPISODE "+score);
            newTrajectory();
            // Perform a hard reset instead of a software reset
            action = Actions.map("system_reset");
            currentAction = -1;
        }
        // Otherwise, add this action to our trajectory
        else if (currentTrajectory != null) {
            currentTrajectory.addAction(action);
            currentAction = action;
        }

        return action;
    }

    protected void newTrajectory() {
        terminalObserved = false;
        score = 0;
        
        // Because reset is sensitive, we may end up creating 0-length trajectories;
        //   ignore those
        if (currentTrajectory != null && currentTrajectory.length() == 0) {
            trajectories.data().remove(currentTrajectory);
        }
        
        currentTrajectory = new Trajectory();
        trajectories.data().add(currentTrajectory);

        startTime = System.currentTimeMillis();
    }
    
    @Override
    public void observeImage(ScreenMatrix image) {
        frameNumber++;

        if (currentTrajectory != null)
            displayTrajectoryLength();

        observeRLData();
    }

    protected void observeRLData() {
        RLData data = comm.getRLData();

        score += data.reward;
        
        // Print out RL information as text
        if (data.reward != 0)
            ui.addMessage("Reward: "+data.reward);
        if (data.isTerminal) {
            if (!terminalObserved)
                ui.addMessage("GAME OVER");
            terminalObserved = true;
        }

    }

    protected void displayTrajectoryLength() {
        // Display the time in the right corner
        long time = System.currentTimeMillis();
        long deltaTime = (time - startTime) / 1000;

        long seconds = deltaTime % 60;
        long minutes = (deltaTime / 60) % 60;
        long hours = (deltaTime / (60*60)) % 24;
        long days = (deltaTime / (60*60*24));

        String timeStr = "";
        if (days > 0)
            timeStr += days+":";
        if (days > 0 || hours > 0)
            timeStr += hours+":";

        NumberFormat format = NumberFormat.getIntegerInstance();
        format.setMinimumIntegerDigits(2);
        timeStr += format.format(minutes) + ":" + format.format(seconds);

        String countStr = trajectories.data().size() + " trajectories";

        ui.setRightString(countStr + ", " + timeStr);
    }
    
    @Override
    public int specialAction() {
        return -1;
    }

    @Override
    protected void keyboardEvents() {
        super.keyboardEvents();
        if (ui.getSaveWeights()) {
            trajectories.saveToFile(trajectoriesFilename);
            ui.setCenterString("Saved as "+trajectoriesFilename);
        }
    }

    public static void main(String[] args) {
        ParameterHolder params = new ParameterHolder();
        HumanAgent.addParameters(params);

        if (args.length > 0) {
            String configFilename = args[0];
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }

        HumanAgent agent = new HumanAgent(params);

        agent.run();
    }
}
