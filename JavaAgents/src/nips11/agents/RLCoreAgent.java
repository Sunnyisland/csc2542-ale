/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents;

import aij.agents.rl.features.LSHFeatureMap;
import aij.agents.rl.features.RAMBitsFeatureMap;
import atariagents.io.Actions;
import atariagents.gui.ImageSequence;
import atariagents.gui.NullUI;
import atariagents.movie.MovieGenerator;
import atariagents.screen.ScreenMatrix;
import explore.rl.NeedsHistory;
import imagetools.ImageTools;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import marcgb.tools.config.Parameters;
import marcgb.tools.loader.DynamicLoader;
import marcgb.tools.print.Numbers;
import nips11.agents.data.FrameHistory;
import nips11.agents.data.Trajectory;
import nips11.agents.features.AbstractSparseFeatureMap;
import nips11.agents.features.OptimizedSDMWFeatureMap;
import nips11.agents.features.colors.FeatureColors;
import nips11.agents.models.FeatureList;
import nips11.agents.rlfeatures.BASSFeatureMap;
import nips11.agents.rlfeatures.DISCOFeatureMap;
import nips11.agents.rlfeatures.Displayable;
import nips11.agents.rlfeatures.RLFeatureMap;
import nips11.agents.rlfeatures.reflect.RLFeatureMapProperties;
import nips11.parameters.MovieParameters;
import nips11.parameters.NIPSParameters;
import nips11.parameters.RLAgentParameters;
import nips11.rl.EnvironmentProvider;
import nips11.rl.RLLearner;
import nips11.rl.SarsaLearner;
import rlVizLib.general.ParameterHolder;

/** A RL environment + agent, all wrapped up as one.
 *
 * @author marc
 */
public class RLCoreAgent extends AbstractAgent {
    public static final int NUM_RIGHT_DISPLAY_MODES = 1;

    public static final int RD_BLANK = 0;

    /** Make a movie out of whatever is being displayed (default mode) */
    public static final int MOVIE_WYSIWYG = 0;
    /** The actual game image */
    public static final int MOVIE_RAW_IMAGE = 1;
    public static final int MOVIE_VALUE_OVERLAY = 2;
    
    protected final int numActions = 18;

    /** Parameters */
    protected String valueFunctionFilename;
    /** The classname for the features map */
    protected String featureMapClassname;
    /** The classname for converting indices to colors */
    protected String featureColorsClassname;
    /** The classname for converting indices to colors, for RL */
    protected String rlFeatureColorsClassname;
    protected String rlLearnerClassname;
    
    /** How often to save the value function */
    protected int autosaveFrequency;
    /** Whether to save the value function at periodic time intervals
     * (independent of autosaveFrequency) */
    protected boolean autosaveOnTime;
    protected boolean evaluationMode;
    /** Whether to generate a movie while in evaluation mode */
    protected boolean evaluationGenerateMovie;
    /** The type of frames we want to record for movie purposes */
    protected int movieMode;
    /** Macro-action length */
    protected int actionStepSize;

    protected EnvironmentProvider environment;
    protected RLFeatureMap featureMap;
    protected RLLearner learner;
    
    protected int learnerAction;
    /** A frame by frame history */
    protected FrameHistory history;
    /** A history of the last frames seen by the learner (skips frames based
     *   on actionStepSize) */
    protected FrameHistory rlHistory;
    
    protected boolean firstStep;
    protected boolean firstROCStep;
    protected boolean requestReset;
    
    protected int frameNumber;
    protected int trainingEpisodeNumber;
    protected int evaluationEpisodeNumber;
    protected int episodeStartFrame;
    protected int macroActionCounter;
    protected boolean inEvaluationPhase;

    protected int rocTrackingCounter = 0;
    /** How often to re-compute the roc center */
    protected int rocTrackingInterval = 5;

    /** The number of training episodes */
    protected int numTrainingEpisodes;
    /** How frequently to evaluate the agent */
    protected int evaluationInterval;
    /** How many episodes to use when evaluating the agent */
    protected int numEvalEpisodes;
    /** How frequently we want to record the episode's trajectory */
    protected int recordTrajectoryInterval;

    protected Trajectory currentTrajectory;

    protected int rocCenterX;
    protected int rocCenterY;

    protected boolean useCenterTracking;

    protected MovieGenerator movieGenerator;

    protected long episodeStartTime;
    protected long lastSaveTime;
    
    protected final boolean displayBackgroundMatrix = false;
    protected final boolean displayROCFeatures = false;

    protected final boolean safeNoFileOverwriting = true;

    protected ImageSequence leftImage;
    protected ImageSequence rightImage;

    protected double startValueEstimate;

    protected boolean featureMapNeedsRAM;
    protected boolean featureMapNeedsScreen;

    /** Evaluation phase variables */
    double evalSumScores;
    double evalDeltaTime;
    double evalDeltaFrames;
    double evalFPS;
    double evalStartValue;

    public static boolean aaaiPosterPlot;
    public static String PARAM_AAAI_POSTER = "aaai-poster-plot";
    
    // Save 12 hours after the last save
    public static long autosaveTimeInterval = 12 * 3600 * 1000;

    public RLCoreAgent(ParameterHolder params) {
        super(params);

        aaaiPosterPlot = params.getBooleanParam(PARAM_AAAI_POSTER);
        
        NIPSParameters.parseParameters(params);
        MovieParameters.parseParameters(params);
        
        valueFunctionFilename = NIPSParameters.valueFunctionFilename;
        featureMapClassname = NIPSParameters.featureMapClassname;
        featureColorsClassname = NIPSParameters.featureColorsClassname;
        rlFeatureColorsClassname = NIPSParameters.rlFeatureColorsClassname;
        rlLearnerClassname = NIPSParameters.rlLearnerClassname;

        autosaveFrequency = NIPSParameters.autosaveFrequency;
        autosaveOnTime = NIPSParameters.autosaveOnTime;
        evaluationMode = NIPSParameters.evalMode;
        evaluationGenerateMovie = NIPSParameters.evalMakeMovie;
        movieMode = MovieParameters.movieMode;
        actionStepSize = NIPSParameters.actionStepSize;
        
        environment = new EnvironmentProvider(params);
        learner = loadRLLearner(params);

        determineDynamicProperties();
        
        int requiredHistoryLength = 0;
        int requiredRLHistoryLength = featureMap.historyLength();
        
        history = new FrameHistory(requiredHistoryLength);
        rlHistory = new FrameHistory(requiredRLHistoryLength);

        numTrainingEpisodes = NIPSParameters.maxNumEpisodes;
        numEvalEpisodes = NIPSParameters.numEvalEpisodes;
        evaluationInterval = NIPSParameters.evaluationInterval;
        recordTrajectoryInterval = NIPSParameters.recordTrajectoryInterval;
        
        if (evaluationMode && evaluationInterval != 0) {
            System.err.println ("WARNING: Setting evaluation interval to 0 because evaluation mode is on.");
            evaluationInterval = 0;
        }
        
        int resumeEpisodeNumber = NIPSParameters.resumeAt;

        /* If __resume_episode is > 0, we reload the learner from when learning
         *     was stopped. This somewhat assumes that we are not in evaluation mode.
         */
        if (resumeEpisodeNumber > 0) {
            trainingEpisodeNumber = resumeEpisodeNumber;
            loadExperiment();
        }
        else if (safeNoFileOverwriting && (autosaveFrequency > 0 || autosaveOnTime)) {
            File fp = new File(valueFunctionFilename);
            if (fp.exists()) {
                ui.die();
                throw new IllegalArgumentException("File "+valueFunctionFilename+" exists. "+
                        "Disable autosave or delete file manually.");
            }

            trainingEpisodeNumber = 0;
        }
        else {
            trainingEpisodeNumber = 0;
        }

        setEvaluationMode(evaluationMode);
        
        if (evaluationMode) {
            loadExperiment();
        }
        // If necessary, start in an evaluation phase
        else if(wantsEvaluationPhase())
            beginEvaluationPhase();
        else
            inEvaluationPhase = false;

        lastSaveTime = System.currentTimeMillis();
        
        requestReset = true;

        if (evaluationGenerateMovie)
            movieGenerator = new MovieGenerator(params);
    }

    public static void addParameters(ParameterHolder params) {
        params.addBooleanParam(PARAM_AAAI_POSTER, false);
        
        AbstractAgent.addParameters(params);
        
        NIPSParameters.addParameters(params);
        // @todo load dynamically
        BASSFeatureMap.addParameters(params);
        DISCOFeatureMap.addParameters(params);
        LSHFeatureMap.addParameters(params);
        RAMBitsFeatureMap.addParameters(params);

        // @todo load dynamically
        SarsaLearner.addParameters(params);
        EnvironmentProvider.addParameters(params);
        // @todo load dynamically
        OptimizedSDMWFeatureMap.addParameters(params);
        MovieGenerator.addParameters(params);

        params.addBooleanParam("dummy-param-for-bug", true);
    }

    protected boolean needsGraphics() {
        return true;
    }

    /** Set a bunch of parameters to evaluate the agent.
     *   - evaluation mode
     *   - epsilon
    *    - no autosave
     */
    public static void applyEvaluationParameters(ParameterHolder params) {
        params.setDoubleParam(RLAgentParameters.PARAM_EPSILON, 0.0);
        params.setIntegerParam(NIPSParameters.PARAM_AUTOSAVE_FREQUENCY, 0);
        params.setBooleanParam(NIPSParameters.PARAM_EVAL_MODE, true);
    }

    public static void applyResumeParameters(ParameterHolder params, int episode) {
        params.setIntegerParam(NIPSParameters.PARAM_RESUME_AT, episode);
    }

    public static void applyNumEpisodesParameters(ParameterHolder params, int numEpisodes) {
        params.setIntegerParam(NIPSParameters.PARAM_MAX_EPISODES, numEpisodes);
    }

    protected final FeatureColors createFeatureColors(boolean outsideColor) {
        try {
            return (FeatureColors) DynamicLoader.constructor(
                    featureColorsClassname,
                    new Class[]{Boolean.TYPE},
                    new Object[]{outsideColor});
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    protected final FeatureColors createRLFeatureColors(boolean outsideColor) {
        try {
            return (FeatureColors) DynamicLoader.constructor(
                    rlFeatureColorsClassname,
                    new Class[]{Boolean.TYPE},
                    new Object[]{outsideColor});
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public final RLFeatureMap createFeatureMap(ParameterHolder params) {
        try {
            // Create a color map with no special outside color
            FeatureColors colors = createRLFeatureColors(false);
            
            RLFeatureMap map = (RLFeatureMap)DynamicLoader.dynamicLoader(
                    featureMapClassname,
                    new String[] {"rlVizLib.general.ParameterHolder",
                        "nips11.agents.features.colors.FeatureColors" },
                    new Object[] {params, colors});
            return map;
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    
    public final RLLearner loadRLLearner(ParameterHolder params) {
        featureMap = createFeatureMap(params);

        try {
            RLLearner l = (RLLearner)DynamicLoader.constructor(
                    rlLearnerClassname,
                    new Class[] {ParameterHolder.class, RLFeatureMap.class, Integer.TYPE},
                    new Object[] {params, featureMap, numActions});
            return l;
        }
        catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    protected void determineDynamicProperties() {
        featureMapNeedsRAM = RLFeatureMapProperties.wantsRAM(featureMapClassname);
        featureMapNeedsScreen = RLFeatureMapProperties.wantsScreen(featureMapClassname);
    }

    public boolean shouldTerminate() {
        // Terminate when we are told to do so by the outside world; or when we
        //  reach the max number of episodes
        boolean hasMaxEpisodes = (numTrainingEpisodes > 0 && 
                trainingEpisodeNumber >= numTrainingEpisodes &&
                !inEvaluationPhase);

        // The isOutOfTime flag indicates that we ran out of experiment time.
        //  This flag is set only after we complete an episode
        boolean shouldTerminate = (isOutOfTime || comm.wantsTerminate() || hasMaxEpisodes);

        // If we return true, we should also end the last episode, if ongoing
        if (shouldTerminate && macroActionCounter > 0 && rlHistory.getLastFrame(0) != null) {
            rlStep();
        }

        return shouldTerminate;
    }

    @Override
    public long getPauseLength() {
        return 0;
    }

    public void setEvaluationMode(boolean mode) {
        learner.setEvalMode(mode);
        evaluationMode = mode;
    }
    
    @Override
    public int selectAction() {
        // Record trajectory if so desired
        if (wantsRecordTrajectory())
            recordTrajectoryStep(learnerAction);

        return learnerAction;
    }

    public void stepBookkeeping(ScreenMatrix image) {
        ScreenMatrix storedImage = (ScreenMatrix)image.clone();

        // Convert the image history to a feature vector
        // @todo potentially reuse memory
        history.addFrame(storedImage);

        frameNumber++;

        // The environment gets to see every ALE step
        environment.observe(image, comm.getRAM(), comm.getRLData());
    }

    @Override
    public void observeImage(ScreenMatrix image) {
        stepBookkeeping(image);

        metaAgent(image);
    }

    /** Handles meta-agent business, such as taking macro-actions as the base
     *    RL action.
     * 
     * @param image
     */
    protected void metaAgent(ScreenMatrix image) {
        macroActionCounter++;
        // Every so often, choose a new action (perform a RL step)
        if (macroActionCounter >= actionStepSize) {
            if (featureMapNeedsScreen)
                rlHistory.addFrame(image);
            if (featureMapNeedsRAM)
                rlHistory.addRAM(comm.getRAM());
            
            rlStep();
            rlHistory.addAction(learnerAction);

            macroActionCounter = 0;

            makeRightImage();
            
            if (evaluationGenerateMovie)
                recordMovieFrame(image);
        }
    }

    public void recordMovieFrame(ScreenMatrix image) {
        switch (movieMode) {
            case MOVIE_WYSIWYG:
                movieGenerator.record(new ImageSequence(converter.convert(image)), rightImage);
                break;
            case MOVIE_RAW_IMAGE:
                movieGenerator.record(new ImageSequence(converter.convert(image)), true);
                break;
            case MOVIE_VALUE_OVERLAY:
                recordFrameWithValue(image);
                break;
        }
    }

    /** Records a frame with the Q-value estimate written in the bottom right corner */
    protected void recordFrameWithValue(ScreenMatrix image) {
        BufferedImage img = converter.convert(image);

        Graphics g = img.getGraphics();

        double chosenValue = Numbers.crop(learner.getLastValue(), 3);
        double downFireValue = Numbers.crop(learner.getValues()[13], 3);

        String vt = chosenValue+"";
        int stringLength = g.getFontMetrics().stringWidth(vt);
        int height = g.getFontMetrics().getHeight();

        int textOffset = height/2;

        // chosen value
        g.setColor(Color.yellow);
        g.drawString(vt, img.getWidth() - stringLength - 2, img.getHeight() - height - textOffset);

        // down fire value
        vt = downFireValue+"";
        stringLength = g.getFontMetrics().stringWidth(vt);
        g.drawString(vt, img.getWidth() - stringLength - 2, img.getHeight() - textOffset);

        movieGenerator.record(new ImageSequence(img), true);

    }

    /** Whether we want to record a trajectory this episode. */
    protected boolean wantsRecordTrajectory() {
        return recordTrajectoryInterval > 0 &&
                ((trainingEpisodeNumber + 1) % recordTrajectoryInterval == 0);
    }

    /** Starts a new trajectory to be recorded. */
    protected void newTrajectory() {
        currentTrajectory = new Trajectory();
    }

    /** Ends the trajectory; output it. */
    protected void endTrajectory() {
        System.err.println ("TRAJ "+currentTrajectory.fullString());
    }

    protected void recordTrajectoryStep(int action) {
        currentTrajectory.addAction(action);
    }
    
    @Override
    public void makeRightImage() {
        // @todo MGB a hack to speed things up; should be refactored
        if (ui instanceof NullUI) {
            rightImage = null;
            return;
        }

        if (rightDisplayMode < NUM_RIGHT_DISPLAY_MODES) {
            switch (rightDisplayMode) {
                case RD_BLANK:
                    rightImage = null;
                    break;
            }
        }
        else if (featureMap instanceof Displayable) { // Displayable feature map
            int mode = rightDisplayMode - NUM_RIGHT_DISPLAY_MODES;
            Displayable d = (Displayable)featureMap;

            rightImage = new ImageSequence(d.getDisplay(currentScreen, mode));
        }
    }

    protected void setPredictedCRImage(ScreenMatrix prediction) {
        int overlayColor = Color.WHITE.getRGB();
        ScreenMatrix currentFrame = history.getLastFrame(0);
        BufferedImage image = converter.convert(currentFrame);

        // Add some translucency to the image
        for (int x = 0; x < currentFrame.width; x++) {
            for (int y = 0; y < currentFrame.height; y++) {
                double value;
                value = prediction.matrix[x][y] / 255.0;

                int imageColor = image.getRGB(x, y);

                // Interpolate between imageColor and overlayColor, with
                //  a factor alpha depending on our prediction strength,
                //  up to 50% for control = 1
                double alpha = value / 2;

                // Interpolate the two colors, channel by channel
                int r = (int) (alpha * (overlayColor & 0xFF0000)
                        + (1.0 - alpha) * (imageColor & 0xFF0000)) & 0xFF0000;
                int g = (int) (alpha * (overlayColor & 0x00FF00)
                        + (1.0 - alpha) * (imageColor & 0x00FF00)) & 0x00FF00;
                int b = (int) (alpha * (overlayColor & 0x0000FF)
                        + (1.0 - alpha) * (imageColor & 0x0000FF)) & 0x0000FF;

                image.setRGB(x, y, r | g | b);
            }
        }

        // Set it in the right panel
        rightImage = new ImageSequence(image);
    }

    // @dbg
    boolean[] coverage;
    int numHits = 0;

    // @dbg
    private void countLoadFactor(FeatureList features) {
        if (coverage == null) coverage = new boolean[featureMap.numFeatures()];

        for (FeatureList.Pair p : features.active) {
            int index = p.index;

            if (!coverage[index]) {
                coverage[index] = true;
                numHits++;
            }
        }
    }
    
    /** The interface between the RL environment and agent.
     * 
     * @param image
     * @param ram
     * @param features
     */
    public void rlStep() {
        FeatureList features = featureMap.getFeatures(rlHistory);
        if (learner instanceof NeedsHistory)
            ((NeedsHistory)learner).observeHistory(rlHistory);
        // @dbg @loadfactor
        countLoadFactor(features);

        if (firstStep) {
            // On the first step, no reward is computed
            learnerAction = learner.agent_start(features);
            startValueEstimate = learner.getLastValue();
            
            firstStep = false;
        }
        else {
            boolean terminal = environment.isTerminal();
            double reward = environment.getReward();
            
            if (!terminal)
                learnerAction = learner.agent_step(reward, features);
            else
                endEpisode(reward);
        }

        environment.newStep();
    }

    protected void endEpisode(double reward) {
        // @dbg
        // MemoryWatch.report("episode-end");

        boolean wasRecording = wantsRecordTrajectory();

        learner.agent_end(reward);
        learnerAction = Actions.map("player_a_noop");

        long deltaTime = System.currentTimeMillis() - episodeStartTime;
        int deltaFrames = frameNumber - episodeStartFrame;
        double fps = Numbers.crop((deltaFrames) / (deltaTime / 1000.0), 4);

        if (inEvaluationPhase)
            evaluationEpisodeEnd(deltaTime, deltaFrames, fps);
        else
            trainingEpisodeEnd(deltaTime, deltaFrames, fps);

        // subsequently reset the environment
        environment.reset();
        requestReset = true;

        episodeStartFrame = frameNumber;

        // @dbg @loadfactor
        /* final int loadFactorReportInterval = 1;

        if (trainingEpisodeNumber % loadFactorReportInterval == 0) {
            double loadFactor = (numHits + 0.0) / coverage.length;
            System.err.println ("LOAD-FACTOR "+loadFactor);
        } */

        if (wasRecording)
            endTrajectory();
    }

    protected boolean wantsEvaluationPhase() {
        return (trainingEpisodeNumber != 0 && evaluationInterval > 0 &&
                numEvalEpisodes > 0 && trainingEpisodeNumber % evaluationInterval == 0);
    }

    protected void trainingEpisodeEnd(long deltaTime, int deltaFrames, double fps) {
        trainingEpisodeNumber++;

        System.err.println("EPISODE " + trainingEpisodeNumber + " " + (int)(environment.getScore()) + " " +
                Numbers.crop(startValueEstimate,4)+" "+(deltaTime / 1000.0)+" "+deltaFrames+" "+fps);

        testOutOfTime();
        
        if (wantsAutosave())
            saveExperiment();

        if (wantsEvaluationPhase())
            beginEvaluationPhase();        
    }

    protected void evaluationEpisodeEnd(long deltaTime, int deltaFrames, double fps) {
        evalSumScores += environment.getScore();
        evalDeltaTime += deltaTime;
        evalDeltaFrames += deltaFrames;
        evalFPS += fps;
        evalStartValue += startValueEstimate;

        if (evaluationEpisodeNumber >= numEvalEpisodes)
            endEvaluationPhase();
        evaluationEpisodeNumber++;
    }

    protected void beginEvaluationPhase() {
        evaluationEpisodeNumber = 1;
        inEvaluationPhase = true;
        // Re-initialize statistics
        evalSumScores = 0;
        evalDeltaTime = 0;
        evalDeltaFrames = 0;
        evalFPS = 0;
        evalStartValue = 0;
        
        setEvaluationMode(true);
    }

    protected void endEvaluationPhase() {
        inEvaluationPhase = false;
        // We can safely set it back to false because: if we were using mode == true
        //  to start with, we would also use evaluationInterval == 0
        setEvaluationMode(false);

        // Now report the average statistics
        double avgScore = evalSumScores / numEvalEpisodes;
        double avgFrames = evalDeltaFrames / numEvalEpisodes;
        double avgFPS = evalFPS / numEvalEpisodes;
        // Note: this should always be the same number
        double avgStartValue = evalStartValue / numEvalEpisodes;
        
        int phaseNumber = 1 + (trainingEpisodeNumber - 1) / evaluationInterval;

        System.err.println ("EVAL "+phaseNumber+" "+avgScore+" "+Numbers.crop(avgStartValue,4)+" "+
                (evalDeltaTime / 1000.0)+" "+avgFrames+" "+avgFPS);
    }
    
    protected boolean wantsAutosave() {
        long t = System.currentTimeMillis();

        return (autosaveFrequency > 0 && trainingEpisodeNumber % autosaveFrequency == 0) ||
            (autosaveOnTime && (t - lastSaveTime >= autosaveTimeInterval || isOutOfTime));
    }
    
    protected void saveExperiment() {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(valueFunctionFilename)));
            learner.saveLearner(oos);
            environment.saveEnvironment(oos);
            
            oos.close();
            
            lastSaveTime = System.currentTimeMillis();
            // Display how many episodes are completed
            System.err.println("SAVE "+trainingEpisodeNumber);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected void loadExperiment() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new GZIPInputStream(new FileInputStream(valueFunctionFilename)));
            learner.loadLearner(ois);
            environment.loadEnvironment(ois);

            ois.close();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void newEpisode() {
        firstStep = true;
        firstROCStep = true;

        // @dbg
        // MemoryWatch.report("episode-start");
        episodeStartTime = System.currentTimeMillis();

        macroActionCounter = 0;
        rocTrackingCounter = 0;
        history.clear();
        rlHistory.clear();
        // Clear any remaining ugliness in the environment
        environment.newStep();

        if (wantsRecordTrajectory())
            newTrajectory();
    }

    @Override
    public int specialAction() {
        if (requestReset) {
            newEpisode();
            requestReset = false;
            return Actions.map("system_reset");
        }
        else
            return -1;
    }

    public boolean wantsRamData() {
        return featureMapNeedsRAM;
    }

    @Override
    public boolean wantsScreenData() {
        return super.wantsScreenData() || featureMapNeedsScreen;
    }

    public boolean wantsRLData() {
        return true;
    }

    public boolean wantsStandardSize() {
        return true;
    }

    @Override
    protected void keyboardEvents() {
        super.keyboardEvents();
    }

    @Override
    public int numRightDisplayModes() {
        int totalModes = NUM_RIGHT_DISPLAY_MODES;

        if (featureMap instanceof Displayable) {
            Displayable d = (Displayable)featureMap;
            totalModes += d.numDisplayModes();
        }

        return totalModes;
    }

    @Override
    public ImageSequence getRightImage() {
        return rightImage;
    }

    static boolean mainEvaluationFlag = false;
    static int mainResumeEpisode = 0;
    static int mainNumEpisodes = -1;

    protected static ParameterHolder parseCommandLine(String[] args, ParameterHolder params) {
        // Parse arguments first - somewhat awkward
        int argIndex = 0;
        String configFilename = null;

        while (argIndex < args.length) {
            String a = args[argIndex];

            // Go into evaluation mode
            if (a.equals("-e") || a.equals("--evaluate"))
                mainEvaluationFlag = true;
            else if(a.startsWith("--resume")) {
                String[] tokens = a.split("=");
                mainResumeEpisode = Integer.parseInt(tokens[1]);
            }
            else if (a.startsWith("--episodes")) {
                String[] tokens = a.split("=");
                mainNumEpisodes = Integer.parseInt(tokens[1]);
            }
            else if (!a.startsWith("-"))
                configFilename = a;

            argIndex++;
        }

        if (configFilename != null) {
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return null;
            }

            Parameters.loadFromConfig(params, configFilename, false);
        }

        if (mainEvaluationFlag) {
            applyEvaluationParameters(params);
        }
        if (mainResumeEpisode > 0) {
            applyResumeParameters(params, mainResumeEpisode);
        }
        if (mainNumEpisodes >= 0) {
            applyNumEpisodesParameters(params, mainNumEpisodes);
        }

        return params;
    }

    public static void main(String[] args) {
        ParameterHolder params = new ParameterHolder();
        RLCoreAgent.addParameters(params);
        
        parseCommandLine(args, params);

        // @dbg
        // MemoryWatch.report("before-constructor");
        RLCoreAgent agent = new RLCoreAgent(params);
        // MemoryWatch.report("after-constructor");

        try {
            agent.run();
        }
        catch (Exception e) {
            // make sure the UI dies, or we will wait forever
            throw new RuntimeException(e);
        }
    }
}
