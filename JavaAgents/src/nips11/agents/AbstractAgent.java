/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents;

import atariagents.io.ALEPipes;
import atariagents.io.Actions;
import atariagents.gui.AbstractUI;
import atariagents.gui.AgentGUI;
import atariagents.gui.ImageSequence;
import atariagents.gui.NullUI;
import atariagents.io.ALEPipes0_3;
import atariagents.screen.ColorPalette;
import atariagents.screen.NTSCPalette;
import atariagents.screen.NTSCPalette32;
import atariagents.screen.SECAMPalette;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import nips11.AgentALELoader;
import nips11.parameters.NIPSParameters;
import rlVizLib.general.ParameterHolder;

/**
 * 
 * 
 * @author marc
 */
public abstract class AbstractAgent {
    protected ParameterHolder params;
    
    public static final int numInitialDelaySteps = 50;
    
    protected int leftDisplayMode;
    protected int rightDisplayMode;
    protected ScreenMatrix currentScreen;

    protected boolean simulationPaused = false;
    
    protected boolean defaultLeftDisplay = true;
    protected final static int LD_FRAME = 0;
    protected final static int LD_BLANK = 1;

    protected final static int NUM_LD_MODES = 2;
    
    protected final ScreenConverter converter;

    protected AbstractUI ui;
    protected ALEPipes comm;

    /** Parameters */
    protected boolean useGUI;
    protected String namedPipesBasename;
    protected int aleProtocol;
    
    /** Variables to enforce real-time in human control mode */
    protected long lastFrameTime;
    protected long lastTargetDelta;
    protected final int framesPerSecond = 60;
    protected long millisFraction = 0;

    protected long frameNumber;

    /** Variables to enforce a maximum run time for experiments. Note! The
      *  subclass must still call testOutOfTime() to set isOutOfTime. This is
      *  to allow agents to terminate only at episode boundaries, etc.
    /** Max run time in seconds */
    protected int maximumRunTime;
    protected long agentStartTime;
    protected boolean isOutOfTime;

    public AbstractAgent(ParameterHolder params) {
        this.params = params;
        
        NIPSParameters.parseParameters(params);
        // Fix the stderr output stream
        String outputFile = NIPSParameters.outputFilename;
        aleProtocol = NIPSParameters.aleProtocol;
        
        try {
            if (outputFile != null)
                System.setErr(new PrintStream(new FileOutputStream(outputFile, true)));
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

        maximumRunTime = NIPSParameters.maxRunTime * 3600;
        useGUI = NIPSParameters.useGui;
        namedPipesBasename = NIPSParameters.namedPipesBasename;

        agentStartTime = System.currentTimeMillis();
        
        // Create the palette
        String paletteName = NIPSParameters.colorPalette;

        if (needsGraphics()) {
            ColorPalette palette = makePalette(paletteName);
            converter = new ScreenConverter(palette);
        }
        else
            converter = null;
        
        init();

        if (NIPSParameters.leftDisplayMode != -1) leftDisplayMode = NIPSParameters.leftDisplayMode;
        if (NIPSParameters.rightDisplayMode != -1) rightDisplayMode = NIPSParameters.rightDisplayMode;
    }

    protected ColorPalette makePalette(String paletteName) {
        if (paletteName.equals("NTSC"))
            return new NTSCPalette();
        if (paletteName.equals("NTSC32"))
            return new NTSCPalette32();
        else if (paletteName.equals("SECAM"))
            return new SECAMPalette();
        else if (paletteName.equals("RANDOM"))
            return new ColorPalette();
        else
            throw new IllegalArgumentException("Invalid palette: "+paletteName);
    }

    protected boolean needsGraphics() {
        return (useGUI);
    }

    public static void addParameters(ParameterHolder params) {
        NIPSParameters.addParameters(params);
        AgentALELoader.addParameters(params);
    }

    public void setParameters(ParameterHolder params) {
    }

    public void init() {
        if (useGUI) {
            // Create the GUI and the communication channel
            ui = new AgentGUI();
            leftDisplayMode = LD_FRAME;
        }
        else {
            ui = new NullUI();
            leftDisplayMode = LD_BLANK;
        }
    }

    protected ALEPipes makePipes() throws IOException {
        // Initialize the pipes; use named pipes if requested
        if (namedPipesBasename != null) {
            switch (aleProtocol) {
                case 2:
                    return new ALEPipes(namedPipesBasename + "out", namedPipesBasename + "in");
                case 3:
                    return new ALEPipes0_3(namedPipesBasename + "out", namedPipesBasename + "in");
                default:
                    throw new IllegalArgumentException("Invalid ALE version: "+aleProtocol);
            }
        } else {
            switch (aleProtocol) {
                case 2:
                    return new ALEPipes();
                case 3:
                    return new ALEPipes0_3();
                default:
                    throw new IllegalArgumentException("Invalid ALE version: "+aleProtocol);
            }
        }
    }

    protected void initComm() {
        comm = null;

        try {
            comm = makePipes();

            comm.setUpdateScreen(wantsScreenData());
            comm.setUpdateRam(wantsRamData());
            comm.setUpdateRL(wantsRLData());
            comm.setStandardSize(wantsStandardSize());
            comm.initPipes();

            // Additional setup
            switch (aleProtocol) {
                case 3:
                    boolean useRLE = params.getBooleanParam(AgentALELoader.PARAM_ALE_RLE);
                    ((ALEPipes0_3)comm).setUseRLE(useRLE);
                    break;
            }
        }
        catch (IOException e) {
            System.err.println ("Could not initialize pipes: "+e.getMessage());
            System.exit(-1);
        }
    }

    public boolean applyReset(boolean hasFirstObservation) {
        boolean done = false;
        boolean mustObserve = !hasFirstObservation;

        if (mustObserve) comm.observe();
        done = comm.act(Actions.map("system_reset"));

        mustObserve = true;

        return done;
    }

    public boolean isPaused() {
        return simulationPaused;
    }
    
    public void run() {
        // Start ALE
        initComm();

        boolean done = false;

        applyReset(false);
        
        // Initial reset
        int sa = specialAction();
        if (sa == Actions.map("system_reset")) {
            done = applyReset(false);
        }

        // The communication channel will return true once the pipes are closed
        while (!done) {
            if (isPaused()) {
                keyboardEvents();
                pause(100);
                continue;
            }

            done = comm.observe();
            if (done) break;
            
            // Obtain the screen matrix
            currentScreen = comm.getScreen();
            updateImages(currentScreen);
            observeImage(currentScreen);

            // After observing, we may want to reset, OR take an action
            sa = specialAction();
            if (sa != -1) {
                // Don't increment the frame count on save/load state
                if (sa != Actions.map("save_state") && sa != Actions.map("load_state"))
                    frameNumber++;

                comm.act(sa);
            }
            else {
                frameNumber++;
                int action = selectAction();
                done = comm.act(action);
            }
            
            // Update the color map
            ui.updateFrameCount();
            keyboardEvents();
            mouseEvents();

            // How long to pause for, in millis
            long pauseLength;

            // Two modes of operation: real-time (60fps) or agent-specific
            if (wantsRealtime()) 
                pauseLength = realtimePauseLength();
            else
                pauseLength = getPauseLength();

            if (pauseLength > 0) {
                pause(pauseLength);
            }

            done |= shouldTerminate();
        }

        // Clean up the GUI
        ui.die();
    }

    /** Handles UI keyboard events. Classes that override this should call
     *   super.keyboardEvents().
     * 
     */
    protected void keyboardEvents() {
        updateDisplayModes();

        // Toggle pause
        if (ui.getPauseSimulation()) {
            simulationPaused ^= true;

            if (simulationPaused) ui.setCenterString("Paused - Frame "+frameNumber);
            else ui.setCenterString("Resumed");
        }
    }

    protected void mouseEvents() {
        int[] pt = ui.getLeftClick();
        if (pt != null) {
            int color = 0;

            // Sample color at point -- for now assume we care only about the left display
            switch (pt[2]) {
                case 0: // Left image
                    color = comm.getScreen().matrix[pt[0]][pt[1]];
                    break;
                case 1:
                    color = -1;
                    break;
            }

            ui.addMessage("("+pt[0]+","+pt[1]+","+color+")");
        }
    }

    protected void updateDisplayModes() {
        // Cycle through display modes on key presses
        if (ui.getLeftDisplayChange()) {
            leftDisplayMode++;
            if (leftDisplayMode >= numLeftDisplayModes())
                leftDisplayMode = 0;

            ui.setCenterString ("Left display mode: "+leftDisplayMode);
        }
        if (ui.getRightDisplayChange() && numRightDisplayModes() > 0) {
            rightDisplayMode++;
            if (rightDisplayMode >= numRightDisplayModes())
                rightDisplayMode = 0;

            ui.setCenterString ("Right display mode: "+rightDisplayMode);
            makeRightImage();
        }
    }

    protected void updateImages(ScreenMatrix currentScreen) {
        // By default, display the current frame
        if (defaultLeftDisplay) {
            switch (leftDisplayMode) {
                case LD_FRAME:
                    BufferedImage img = converter.convert(currentScreen);
                    ui.setLeftImage(new ImageSequence(img));
                    break;
                case LD_BLANK:
                    ui.setLeftImage(null);
                    break;
            }
        }
        else {
            ImageSequence img = getLeftImage();
            ui.setLeftImage(img);
        }

        if (numRightDisplayModes() > 0) {
            ImageSequence img = getRightImage();
            ui.setRightImage(img);
        }

        ui.refresh();
    }

    protected long realtimePauseLength() {
        long targetDelta = 1000 / framesPerSecond;
        long deltaRemainder = 1000 % framesPerSecond;
        millisFraction += deltaRemainder;

        // Correct for fractional deltas
        while (millisFraction > framesPerSecond) {
            targetDelta += 1;
            millisFraction -= framesPerSecond;
        }

        long time = System.currentTimeMillis();
        if (lastFrameTime == 0) {
            lastFrameTime = time;
            lastTargetDelta = targetDelta;

            return targetDelta;
        }
        else {
            long deltaTime = time - lastFrameTime;
            lastFrameTime = time;
            lastTargetDelta = lastTargetDelta - deltaTime + targetDelta;

            // Correct the timing by how much time actually elapsed;
            return lastTargetDelta;
        }
    }

    /** Sets isOutOfTime if the maximum run time has elapsed. This is NOT called
     *   by the abstract class; subclasses must call it as needed.
     */
    protected void testOutOfTime() {
        long secondsSinceStart = (System.currentTimeMillis() - agentStartTime) / 1000;
        isOutOfTime = (maximumRunTime > 0 && secondsSinceStart >= maximumRunTime);
    }

    protected void pause(long waitTime) {
        try {
            Thread.sleep(waitTime);
        }
        catch (Exception e) {
        }
    }

    /** Cleans up other threads so that we can terminate. */
    public void cleanup() {
        ui.die();
        comm.close();
    }

    /** Methods to handle the interaction with the simulator */
    public abstract long getPauseLength();
    public abstract int selectAction();
    public abstract void observeImage(ScreenMatrix image);
    /** Returns an action to be executed, or -1 (no action). If an action is to
     *    be executed, no further processing is done (image updating, etc). Used
     *    for reset, save state, load state, etc.
     * @return
     */
    public abstract int specialAction();
    public abstract boolean shouldTerminate();
    public abstract boolean wantsRamData();
    public abstract boolean wantsRLData();
    public abstract boolean wantsStandardSize();

    /** Any agent overriding this should call super.wantsScreenData() and OR the
     *   result with their own desire for screen data.
     * 
     * @return
     */
    public boolean wantsScreenData() {
        return !(ui instanceof NullUI);
    }

    public boolean wantsRealtime() {
        return false;
    }
    /** Methods dealing with image display. To be overriden by the agent. */
    public int numLeftDisplayModes() {
        return 2;
    }
    public int numRightDisplayModes() {
        return 0;
    }
    public ImageSequence getLeftImage() {
        throw new UnsupportedOperationException();
    }
    public ImageSequence getRightImage() {
        throw new UnsupportedOperationException();
    }
    /** Override this to generate a new image as needed */
    public void makeLeftImage() {
    }
    /** Override this to generate a new image as needed */
    public void makeRightImage() {
        throw new UnsupportedOperationException();
    }
}
