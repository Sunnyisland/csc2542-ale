/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.data;

import atariagents.screen.ScreenMatrix;

/** Marc G. Bellemare
 *
 * @author marc
 */
public class ControlFrameSample {
    protected FrameHistory history;
    protected ScreenMatrix controlRegion;

    public ControlFrameSample(FrameHistory history, ScreenMatrix controlRegion) {
        this.history = history;
        this.controlRegion = controlRegion;
    }

    /** Returns true if the point (x,y) is within the control region.
     * 
     * @param x
     * @param y
     * @return
     */
    public boolean valueAt(int x, int y) {
        return(controlRegion.matrix[x][y] == 1);
    }

    public boolean hasControlRegion() {
        return (controlRegion != null);
    }

    public FrameHistory getHistory() {
        return history;
    }
}
