/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

/** Contains a trajectory / action sequence.
 *
 * @author Marc G. Bellemare
 */
public class Trajectory implements Serializable {
    protected ArrayList<Integer> actions;
    protected transient ListIterator<Integer> iterator;
    protected transient final Random rnd;

    public Trajectory() {
        this(new Random());
    }

    public Trajectory(Random rnd) {
        actions = new ArrayList<Integer>();
        this.rnd = rnd;
    }

    /** Create a trajectory from string data */
    public Trajectory(String data) {
        this(new Random());

        String[] tokens = data.split(" ");
        for (int i = 0; i < tokens.length; i++) {
            actions.add(Integer.parseInt(tokens[i]));
        }
    }

    public int length() {
        return actions.size();
    }

    public void addAction(int a) {
        actions.add(a);
    }

    public int getAction(int i) {
        return actions.get(i);
    }
    
    public void initIteration() {
        iterator = actions.listIterator();
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public int next() {
        return iterator.next();
    }

    public void clear() {
        actions.clear();
    }

    public void setRandom(int minLength, int maxLength, int stepSize, int numActions) {
        clear();

        int range = maxLength - minLength;
        int length = rnd.nextInt(range) + minLength;

        // Ensure that the trajectory length gives all action blocks the same length
        int mod = length % stepSize;
        if (length - mod < minLength)
            length += (stepSize - mod);
        else
            length -= mod;

        while (actions.size() < length) {
            // Pick action at random
            int action = rnd.nextInt(numActions);
            // Add it for 'stepSize' steps
            for (int i = 0; i < stepSize; i++)
                actions.add(action);
        }
    }

    /** Returns a String describing this trajectory.
     * 
     * @return
     */
    public String fullString() {
        String s = "";

        boolean first = true;

        // @dbg
        long startTime = System.currentTimeMillis();

        for (int a : actions) {
            s += (first?"":" ") + a;
            first = false;
        }

        // @dbg
        long endTime = System.currentTimeMillis();
        System.err.println ("string concatenation took "+(endTime - startTime) +" ms");
        return s;
    }
}
