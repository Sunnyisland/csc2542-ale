/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.data;

import atariagents.screen.ScreenMatrix;
import java.util.ArrayList;

/** A set of images, one per action.
 *
 * @author marc
 */
public class FrameSet {

    protected ArrayList<ScreenMatrix> images;

    public FrameSet() {
        images = new ArrayList<ScreenMatrix>();
    }

    public boolean isEmpty() {
        return images.isEmpty();
    }

    public int size() {
        return images.size();
    }
    
    public ScreenMatrix get(int i) {
        return images.get(i);
    }
    
    public void addImage(ScreenMatrix img) {
        try {
            images.add((ScreenMatrix)img.clone());
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /** Returns an image where each pixel takes on a 0 or 1 value. A value of
     *   0 indicates that over all images, the pixel value is constant. A value
     *   of 1 indicates otherwise.
     *
     * @return
     */
    public ScreenMatrix getDifferenceImage() {
        if (images.isEmpty()) {
            throw new UnsupportedOperationException("Cannot obtain difference of a null image set.");
        }

        // Assume all images have the same width/height
        ScreenMatrix I0 = images.get(0);
        int w = I0.width;
        int h = I0.height;

        ScreenMatrix diff = new ScreenMatrix(w, h);

        // By transitivity, we only need to compare all images to I0
        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                // Test all other images
                for (int index = 1; index < images.size(); index++) {
                    ScreenMatrix Ip = images.get(index);

                    // As soon as one image differs, set pixel to 1
                    if (Ip.matrix[x][y] != I0.matrix[x][y]) {
                        diff.matrix[x][y] = 1;
                        break;
                    }
                }
            }
        }

        return diff;
    }
}
