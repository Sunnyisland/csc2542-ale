/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.data;

import nips11.parameters.NIPSParameters;
import rlVizLib.general.ParameterHolder;

/** A method that provides trajectories for agents to follow. 
 *
 * @author marc
 */
public class TrajectoryProvider {
    protected TrajectorySet trajectories;

    protected int minRandomLength;
    protected int maxRandomLength;
    protected int macroActionLength;
    protected int numActions;

    /** Which trajectory we are currently running */
    protected int trajectoryIndex;

    protected boolean generateRandom;
    protected boolean expertRandom;
    protected boolean sampleOrdered;

    /** The maximum length of the expert part, when generating expert-random
     *   trajectories
     */
    protected int maxExpertLength;
    /** Whether to align expert trajectory lengths to the macro-action size */
    public static boolean alignExpertLength = true;

    public TrajectoryProvider(int macroActionLength, int numActions, 
            ParameterHolder params) {
        this.macroActionLength = macroActionLength;
        this.numActions = numActions;
        
        NIPSParameters.parseParameters(params);

        minRandomLength = NIPSParameters.minTrajectoryLength;
        maxRandomLength = NIPSParameters.maxTrajectoryLength;
        sampleOrdered = NIPSParameters.allTrajectoriesOnce;

        maxExpertLength = NIPSParameters.maxExpertTrajectoryLength;
        
        // If no filename is specified, we will generate random trajectories
        if (NIPSParameters.trajectoriesFilename == null ||
            NIPSParameters.trajectoriesFilename.equals("")) {
            generateRandom = true;
            expertRandom = false;
        }
        else {
            generateRandom = false;
            trajectories = new TrajectorySet();
            trajectories.loadFromFile(NIPSParameters.trajectoriesFilename);

            expertRandom = NIPSParameters.expertRandomTrajectories;
        }
    }

    public static void addParameters(ParameterHolder params) {
        NIPSParameters.addParameters(params);
    }

    public boolean hasExpertTrajectories() {
        return !generateRandom;
    }
    
    public int getMinRandomLength() { return minRandomLength; }
    public int getMaxRandomLength() { return maxRandomLength; }

    public void addTrajectory(Trajectory t) {
        // We will be sampling from our pool of trajectories from now
        generateRandom = false;
        if (trajectories == null) {
            trajectories = new TrajectorySet();
        }

        trajectories.data().add(t);
    }
    
    public Trajectory getTrajectory() {
        // Randomly make a trajectory, or randomly sample from our pool
        if (generateRandom)
            return newRandomTrajectory();
        else if (sampleOrdered) { // sample each trajectory in turn
            if (trajectoryIndex >= trajectories.data().size())
                return null;
            else {
                return trajectories.data().get(trajectoryIndex++);
            }
        }
        // Generate a hybrid expert + random trajectory
        else if (expertRandom) {
            return newExpertRandomTrajectory();
        }
        else {
            int r = (int)(Math.random() * trajectories.data().size());

            return trajectories.data().get(r);
        }
    }

    public Trajectory newExpertRandomTrajectory() {
        int r = (int)(Math.random() * trajectories.data().size());

        Trajectory expert = trajectories.data().get(r);

        int maxLength;
        
        // Pick an expert trajectory at random
        if (maxExpertLength == 0)
            maxLength = expert.length();
        else
            maxLength = Math.min(maxExpertLength, expert.length());

        // Sample part of the expert trajectory
        int expertSublength = (int)(Math.random() * maxLength);

        // Align the expert trajectory length to the macro action size, if
        //  desired
        if (alignExpertLength) {
            int mod = expertSublength % macroActionLength;
            if (expertSublength - mod < 0)
                expertSublength += (macroActionLength - mod);
            else
                expertSublength -= mod;
        }

        Trajectory t = new Trajectory();

        expert.initIteration();
        for (int a = 0; a < expertSublength; a++) {
            t.addAction(expert.next());
        }

        Trajectory random = newRandomTrajectory();
        random.initIteration();
        // Concatenate the random trajectory
        while (random.hasNext()) {
            t.addAction(random.next());
        }

        return t;
    }

    public Trajectory newRandomTrajectory() {
        Trajectory t = new Trajectory();
        t.setRandom(minRandomLength, maxRandomLength, macroActionLength, numActions);

        return t;
    }
}
