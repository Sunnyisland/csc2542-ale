/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;

/** As the name implies, a set of trajectories.
 *
 * @author marc
 */
public class TrajectorySet {
    protected ArrayList<Trajectory> trajectories;

    public TrajectorySet() {
        trajectories = new ArrayList<Trajectory>();
    }

    /** Load a set of trajectories from a file.
     * 
     * @param filename
     */
    public TrajectorySet(String filename) {

    }

    public ArrayList<Trajectory> data() {
        return trajectories;
    }

    /** Saves the trajectories as a set of newline separated strings.
     * 
     * @param filename
     */
    public void saveToFile(String filename) {
        try {
            PrintStream out = new PrintStream(new FileOutputStream(filename));

            for (Trajectory t : trajectories) {
                out.print(t.fullString()+"\n");
            }
        }
        catch (IOException e) {
            throw new RuntimeException (e);
        }
    }

    public void loadFromFile(String filename) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

            trajectories.clear();
            String line;

            // Each trajectory on its own line
            while ((line = in.readLine()) != null) {
                // Create a new trajectory
                trajectories.add(new Trajectory(line));
            }
        }
        catch (IOException e) {
            throw new RuntimeException (e);
        }
    }

}
