/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.data;

import atariagents.io.ConsoleRAM;
import atariagents.screen.ScreenMatrix;
import java.util.LinkedList;

/** A time-ordered list of frames.
 *
 * @author marc
 */
public class FrameHistory implements Cloneable {
    protected LinkedList<ScreenMatrix> frames;
    protected LinkedList<ConsoleRAM> ramHistory;
    protected LinkedList<Integer> actions;
    
    protected int maxLength;

    public FrameHistory(int maxLength) {
        this.maxLength = maxLength;
        frames = new LinkedList<ScreenMatrix>();
        ramHistory = new LinkedList<ConsoleRAM>();
        actions = new LinkedList<Integer>();
    }

    public boolean isFull() {
        return size() == maxLength;
    }

    public void addFrame(ScreenMatrix frame) {
        frames.addLast(frame);
        while (frames.size() > maxLength)
            frames.removeFirst();
    }

    public void addRAM(ConsoleRAM ram) {
        ramHistory.addLast(ram);
        while (ramHistory.size() > maxLength)
            ramHistory.removeFirst();
    }

    public void addAction(int action) {
        actions.addLast(action);
        while (actions.size() > maxLength - 1)
            actions.removeFirst();
    }
    
    public int size() {
        return frames.size();
    }
    
    public void clear() {
        frames.clear();
        actions.clear();
        ramHistory.clear();
    }
    
    /** Removes the t-to-last frame. */
    public void removeLast(int t) {
        // This is really bad - because I'm overloading this class to store either
        //  *just* a sequence of frames, or frames + RAM + action, I need to have
        //  these if statements
        if (frames.size() > 0)
            frames.remove(frames.size() - t - 1);
        if (ramHistory.size() > 0)
            ramHistory.remove(ramHistory.size() - t - 1);
        if (actions.size() > 0)
            actions.remove(actions.size() - t - 1);
    }

    public int maxHistoryLength() {
        return maxLength;
    }

    /** Return the t-to-last frame. */
    public ScreenMatrix getLastFrame(int t) {
        if (t >= frames.size()) return null;
        else return frames.get(frames.size() - t - 1);
    }

    public ConsoleRAM getLastRAM(int t) {
        if (t >= ramHistory.size()) return null;
        else return ramHistory.get(ramHistory.size() - t - 1);
    }

    public int getLastAction(int t) {
        if (t >= actions.size()) return -1;
        else return actions.get(actions.size() - t - 1);
    }

    public Object clone() {
        try {
            FrameHistory obj = (FrameHistory)super.clone();

            obj.frames = new LinkedList<ScreenMatrix>();
            // Copy over the frames; we do not clone them
            for (ScreenMatrix screen : this.frames) {
                obj.frames.add(screen);
            }

            obj.ramHistory = new LinkedList<ConsoleRAM>();
            for (ConsoleRAM ram : this.ramHistory) {
                obj.ramHistory.add(ram);
            }

            obj.actions = new LinkedList<Integer>();
            for (int a : this.actions) {
                obj.actions.add(a);
            }
            
            return obj;
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
