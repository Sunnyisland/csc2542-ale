/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents;

import atariagents.io.Actions;
import atariagents.gui.ImageSequence;
import atariagents.screen.ScreenConverter;
import atariagents.screen.ScreenMatrix;
import java.io.File;
import javax.imageio.ImageIO;
import marcgb.tools.config.Parameters;
import nips11.agents.bgdetect.ScreenHistogram;
import nips11.agents.data.Trajectory;
import nips11.agents.data.TrajectoryProvider;
import nips11.parameters.BackgroundDetectionParameters;
import nips11.parameters.NIPSParameters;
import rlVizLib.general.ParameterHolder;

/**
 * This agents detects an Atari game's background via the histogram method (Naddaf 2010).
 * a state machine.
 *
 * @author Marc G. Bellemare
 */
public class BackgroundDetectionAgent extends AbstractAgent {
    public enum StateType {Start, GenerateTrajectory, Reset, RunTrajectoryHead, RunTrajectory};

    public static final int RD_BACKGROUND = 0;
    public static final int RD_BLANK = 1;
    public static final int NUM_RIGHT_DISPLAY_MODES = 2;
    
    protected StateType state;
    
    protected int numActions = 18;

    protected String backgroundBasename;
    
    protected Trajectory trajectory;
    protected TrajectoryProvider trajectoryProvider;
    /** How many steps to run in a trajectory before sampling */
    protected int samplingFrequency;
    /** How many frames to first skip when running a trajectory. Useful for evaluating. */
    protected int trajectoryHeadIgnoreLength;
    protected int trajectoryCounter;

    protected int maximumSampleCount;
    
    protected boolean sampleNextFrame;
    protected ScreenMatrix currentFrame;

    protected ScreenHistogram histogram;
    
    /** How many samples we have processed / acquired so far */
    protected int numSamples;
    protected boolean noMoreTrajectories;
    
    /** Parameters */
    protected int actionStepSize;

    /** The image we want to display on the left side, if not using default display */
    protected ImageSequence leftImage;
    /** The image we want to display on the right side */
    protected ImageSequence rightImage;
    
    protected int frameNumber = 0;
    protected long cycleStartTime;
    
    public static final int sampleReportInterval = 10000;
    public static final boolean safeNoFileOverwriting = true;
    public static final boolean reportResults = true;
    
    public BackgroundDetectionAgent(ParameterHolder params) {
        super(params);

        // Parse parameters
        NIPSParameters.parseParameters(params);
        BackgroundDetectionParameters.parseParameters(params);
        
        backgroundBasename = BackgroundDetectionParameters.backgroundBasename;
        
        samplingFrequency = NIPSParameters.trajectorySamplingInterval;
        actionStepSize = NIPSParameters.actionStepSize;
        trajectoryHeadIgnoreLength = NIPSParameters.trajectoryHeadIgnoreLength;
        maximumSampleCount = NIPSParameters.maximumSampleCount;

        histogram = new ScreenHistogram();

        trajectory = new Trajectory();
        trajectoryProvider = new TrajectoryProvider(actionStepSize, numActions, params);

        // Safety check - don't overwrite files
        if (safeNoFileOverwriting) {
            String filename = getBackgroundDataFilename();
            File fp = new File(filename);
            if (fp.exists())
                throw new IllegalArgumentException("File "+filename+" exists. "+
                        "Disable autosave or delete file manually.");
        }
        if (maximumSampleCount == 0) {
            throw new IllegalArgumentException("Sample count cannot be 0.");
        }

        state = StateType.Start;
        setState(StateType.GenerateTrajectory);
    }
    
    public static void addParameters(ParameterHolder params) {
        // Parent parameters
        AbstractAgent.addParameters(params);
        // Our parameters
        NIPSParameters.addParameters(params);
        BackgroundDetectionParameters.addParameters(params);
    }

    public String getBackgroundDataFilename() {
        return backgroundBasename+".bg";
    }

    public String getBackgroundPNGFilename() {
        return backgroundBasename+".png";
    }
    
    public boolean wantsScreenData() {
        return true;
    }

    public boolean wantsRamData() {
        return false;
    }

    public boolean wantsRLData() {
        return false;
    }

    public boolean wantsStandardSize() {
        return true;
    }

    public boolean shouldTerminate() {        
        // Terminate if we have the required number of samples
        return (noMoreTrajectories || numSamples >= maximumSampleCount);
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    protected void keyboardEvents() {
        super.keyboardEvents();
    }
    
    @Override
    public long getPauseLength() {
        return 0;
    }

    /** Helper function for selectAction.
     *
     * @return
     */
    private boolean wantsControlSample() {
        // always true at the end of trajectories
        if (!trajectory.hasNext()) return true;
        return (samplingFrequency > 0 && trajectoryCounter >= samplingFrequency);
    }

    @Override
    public int selectAction() {
        int action = 0;

        switch (state) {
            case RunTrajectoryHead:
                action = trajectory.next();
                trajectoryCounter++;

                // We need some length left in the trajectory once done the
                //  initial run
                if (!trajectory.hasNext()) {
                    System.err.println ("WARNING trajectory too short, skipped");
                    setState(StateType.GenerateTrajectory);
                }
                else if (trajectoryCounter >= trajectoryHeadIgnoreLength)
                    setState(StateType.RunTrajectory);
                break;
            case RunTrajectory:
                action = trajectory.next();
                trajectoryCounter++;

                boolean wantsSample = wantsControlSample();

                // Switch to the control action once done with the trajectory,
                //  or every so often (based on 'samplingFrequency')
                if (wantsSample)
                    sampleNextFrame = true;

                if (!trajectory.hasNext())
                    setState(StateType.GenerateTrajectory);
                
                break;
                
            default:
                System.err.println ("NOOP State "+state);
                action = Actions.map("player_a_noop");
                break;
        }

        return action;
    }

    @Override
    public void observeImage(ScreenMatrix image) {
        if (cycleStartTime == 0)
            cycleStartTime = System.currentTimeMillis();
        
        currentFrame = image;

        frameNumber++;

        // If so desired, use the current frame to compute the background
        if (sampleNextFrame) {
            sampleNextFrame = false;
            sampleFrame(currentFrame);
        }
    }

    @Override
    public int specialAction() {
        switch(state) {
            case Reset:
                setState(StateType.RunTrajectoryHead);
                return Actions.map("system_reset");
            default:
                return -1;
        }
    }

    public void setState(StateType newState) {
        state = newState;
        
        switch (state) {
            case GenerateTrajectory:
                newTrajectory();

                setState(StateType.Reset);
                break;
            case RunTrajectoryHead:
                if (trajectoryHeadIgnoreLength <= 0)
                    setState(StateType.RunTrajectory);
                else {
                    // Assume a non-empty trajectory
                    trajectoryCounter = 0;
                }
                break;
            case RunTrajectory:
                // If we have more trajectory to run, re-initialize the sampling
                //  process; otherwise, first get a new trajectory
                if (trajectory.hasNext())
                    trajectoryCounter = 0;
                else
                    setState(StateType.GenerateTrajectory);
                break;
            default:
                break;
        }
    }

    public void saveBackgroundPNG(ScreenMatrix background, String filename) {
        ScreenConverter sc = new ScreenConverter(ScreenConverter.NTSC_PALETTE);

        try {
            ImageIO.write(sc.convert(background), "png", new File(filename));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveBackgroundData(ScreenMatrix background, String filename) {
        try {
            background.saveData(filename);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /** Save background information (data and png).
     * 
     */
    protected void saveBackground() {
        ScreenMatrix background = extractBackground();

        saveBackgroundPNG(background, getBackgroundPNGFilename());
        saveBackgroundData(background, getBackgroundDataFilename());
    }

    /** Sample one frame to compute our background.
     * 
     * @param frame
     */
    protected void sampleFrame(ScreenMatrix frame) {
        histogram.addScreen(frame);

        numSamples++;

        if (numSamples >= maximumSampleCount) {
            saveBackground();
        }

        report();
        makeRightImage();
    }

    protected void report() {
        long cycleTime = System.currentTimeMillis() - cycleStartTime;

        if (reportResults && sampleReportInterval > 0 && numSamples % sampleReportInterval == 0) {
            System.err.println ("SAMPLES "+numSamples+" "+(cycleTime/1000.0));
            cycleTime = System.currentTimeMillis();
        }
    }

    public ScreenMatrix extractBackground() {
        return histogram.getMostFrequent();
    }

    /** Generates a new, random trajectory */
    public void newTrajectory() {
        trajectory = trajectoryProvider.getTrajectory();

        if (trajectory != null) {
            trajectory.initIteration();

            // We don't want empty trajectories, they will cause badness in the state machine
            if (!trajectory.hasNext())
                throw new UnsupportedOperationException("Empty trajectory");
        }
        else {
            // End prematurely; we are done collecting data
            saveBackground();
            noMoreTrajectories = true;
        }
    }

    @Override
    public void makeRightImage() {
        if (!useGUI || rightDisplayMode == RD_BLANK) {
            rightImage = null;
            return;
        }
        else switch (rightDisplayMode) {
            case RD_BACKGROUND:
                ScreenConverter sc = new ScreenConverter(ScreenConverter.NTSC_PALETTE);
                ScreenMatrix background = extractBackground();
                rightImage = new ImageSequence(sc.convert(background));
                break;
            default:
                break;
        }
    }

    @Override
    public int numRightDisplayModes() { return NUM_RIGHT_DISPLAY_MODES; }

    @Override
    public ImageSequence getRightImage() {
        return rightImage;
    }
    
    public static void main(String[] args) {
        ParameterHolder params = new ParameterHolder();
        BackgroundDetectionAgent.addParameters(params);

        if (args.length > 0) {
            String configFilename = args[0];
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }
        
        BackgroundDetectionAgent agent = new BackgroundDetectionAgent(params);

        agent.run();
    }
}
