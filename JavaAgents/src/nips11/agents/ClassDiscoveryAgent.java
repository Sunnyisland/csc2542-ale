/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents;

import aij.agents.disco.ClassDiscovery;
import atariagents.gui.ImageSequence;
import atariagents.io.Actions;
import atariagents.screen.ScreenMatrix;
import java.io.File;
import marcgb.tools.config.Parameters;
import nips11.agents.data.Trajectory;
import nips11.agents.data.TrajectoryProvider;
import nips11.parameters.NIPSParameters;
import rlVizLib.general.ParameterHolder;

/**
 * This agents detects an Atari game's background via the histogram method (Naddaf 2010).
 * a state machine.
 *
 * @author Marc G. Bellemare
 */
public class ClassDiscoveryAgent extends AbstractAgent {
    public enum StateType {Start, GenerateTrajectory, Reset, RunTrajectoryHead, RunTrajectory};
    
    protected StateType state;
    
    protected int numActions = 18;

    protected Trajectory trajectory;
    protected TrajectoryProvider trajectoryProvider;
    /** How many steps to run in a trajectory before sampling */
    protected int samplingFrequency;
    /** How many frames to first skip when running a trajectory. Useful for evaluating. */
    protected int trajectoryHeadIgnoreLength;
    protected int trajectoryCounter;

    protected boolean sampleNextFrame;
    protected ScreenMatrix currentFrame;

    protected ClassDiscovery classDiscoveryModule;
    
    /** How many samples we have processed / acquired so far */
    protected int numSamples;
    
    /** Parameters */
    protected int actionStepSize;

    protected int frameNumber = 0;
    protected long cycleStartTime;
    
    public static final int sampleReportInterval = 10000;
    public static final boolean safeNoFileOverwriting = true;
    public static final boolean reportResults = true;
    
    public ClassDiscoveryAgent(ParameterHolder params) {
        super(params);

        // Parse parameters
        NIPSParameters.parseParameters(params);

        samplingFrequency = NIPSParameters.trajectorySamplingInterval;
        actionStepSize = NIPSParameters.actionStepSize;
        trajectoryHeadIgnoreLength = NIPSParameters.trajectoryHeadIgnoreLength;

        trajectory = new Trajectory();
        trajectoryProvider = new TrajectoryProvider(actionStepSize, numActions, params);

        classDiscoveryModule = new ClassDiscovery(params);

        state = StateType.Start;
        setState(StateType.GenerateTrajectory);
    }
    
    public static void addParameters(ParameterHolder params) {
        // Parent parameters
        AbstractAgent.addParameters(params);
        // Our parameters
        NIPSParameters.addParameters(params);
        // Children parameters
        ClassDiscovery.addParameters(params);
    }

    @Override
    public boolean wantsScreenData() {
        return true;
    }

    public boolean wantsRamData() {
        return false;
    }

    public boolean wantsRLData() {
        return false;
    }

    public boolean wantsStandardSize() {
        return true;
    }

    public boolean shouldTerminate() {        
        // Terminate if we have the required number of samples
        return (classDiscoveryModule.is_class_discovery_complete());
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    protected void keyboardEvents() {
        super.keyboardEvents();
    }
    
    @Override
    public long getPauseLength() {
        return 0;
    }

    /** Helper function for selectAction.
     *
     * @return
     */
    private boolean wantsControlSample() {
        // always true at the end of trajectories
        if (!trajectory.hasNext()) return true;
        return (samplingFrequency > 0 && trajectoryCounter >= samplingFrequency);
    }

    @Override
    public int selectAction() {
        int action = 0;

        switch (state) {
            case RunTrajectoryHead:
                action = trajectory.next();
                trajectoryCounter++;

                // We need some length left in the trajectory once done the
                //  initial run
                if (!trajectory.hasNext()) {
                    System.err.println ("WARNING trajectory too short, skipped");
                    setState(StateType.GenerateTrajectory);
                }
                else if (trajectoryCounter >= trajectoryHeadIgnoreLength)
                    setState(StateType.RunTrajectory);
                break;
            case RunTrajectory:
                action = trajectory.next();
                trajectoryCounter++;

                boolean wantsSample = wantsControlSample();

                // Switch to the control action once done with the trajectory,
                //  or every so often (based on 'samplingFrequency')
                if (wantsSample)
                    sampleNextFrame = true;

                if (!trajectory.hasNext())
                    setState(StateType.GenerateTrajectory);
                
                break;
                
            default:
                System.err.println ("NOOP State "+state);
                action = Actions.map("player_a_noop");
                break;
        }

        return action;
    }

    @Override
    public void observeImage(ScreenMatrix image) {
        if (cycleStartTime == 0)
            cycleStartTime = System.currentTimeMillis();
        
        currentFrame = image;

        frameNumber++;

        // If so desired, use the current frame to compute the background
        if (sampleNextFrame) {
            sampleNextFrame = false;
            sampleFrame(currentFrame);
        }
    }

    @Override
    public int specialAction() {
        switch(state) {
            case Reset:
                setState(StateType.RunTrajectoryHead);
                return Actions.map("system_reset");
            default:
                return -1;
        }
    }

    public void setState(StateType newState) {
        state = newState;
        
        switch (state) {
            case GenerateTrajectory:
                newTrajectory();

                setState(StateType.Reset);
                break;
            case RunTrajectoryHead:
                if (trajectoryHeadIgnoreLength <= 0)
                    setState(StateType.RunTrajectory);
                else {
                    // Assume a non-empty trajectory
                    trajectoryCounter = 0;
                }
                break;
            case RunTrajectory:
                // If we have more trajectory to run, re-initialize the sampling
                //  process; otherwise, first get a new trajectory
                if (trajectory.hasNext())
                    trajectoryCounter = 0;
                else
                    setState(StateType.GenerateTrajectory);
                break;
            default:
                break;
        }
    }

    /** Sample one frame to compute our background.
     * 
     * @param frame
     */
    protected void sampleFrame(ScreenMatrix frame) {
        classDiscoveryModule.get_new_screen(frame, frameNumber);

        numSamples++;
        
        report();
    }

    protected void report() {
        long cycleTime = System.currentTimeMillis() - cycleStartTime;

        if (reportResults && sampleReportInterval > 0 && numSamples % sampleReportInterval == 0) {
            System.err.println ("SAMPLES "+numSamples+" "+(cycleTime/1000.0));
            cycleTime = System.currentTimeMillis();
        }
    }

    /** Generates a new, random trajectory */
    public void newTrajectory() {
        trajectory = trajectoryProvider.getTrajectory();

        if (trajectory != null) {
            trajectory.initIteration();

            // We don't want empty trajectories, they will cause badness in the state machine
            if (!trajectory.hasNext())
                throw new UnsupportedOperationException("Empty trajectory");
        }
        else
            classDiscoveryModule.terminate();
    }

    public static void main(String[] args) {
        ParameterHolder params = new ParameterHolder();
        ClassDiscoveryAgent.addParameters(params);

        if (args.length > 0) {
            String configFilename = args[0];
            File fp = new File(configFilename);
            if (!fp.exists()) {
                System.err.println("Configuration file "+configFilename+" does not exist.");
                return;
            }
            Parameters.loadFromConfig(params, configFilename);
        }
        
        ClassDiscoveryAgent agent = new ClassDiscoveryAgent(params);

        agent.run();
    }
}
