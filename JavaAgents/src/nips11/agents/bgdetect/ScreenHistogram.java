/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.bgdetect;

import atariagents.screen.ScreenMatrix;

/** A histogram of screen data.
 *
 * @author marc
 */
public class ScreenHistogram {
    public static final int numColors = 256;
    
    protected ScreenMatrix[] colorCounts;

    public ScreenHistogram() {
    }

    protected void init(ScreenMatrix sampleScreen) {
        // Create one screen per color to keep track of counts
        colorCounts = new ScreenMatrix[numColors];

        for (int i = 0; i < colorCounts.length; i++) {
            colorCounts[i] = new ScreenMatrix(sampleScreen.width, sampleScreen.height);
        }
    }

    /** Update the color counts with the given screen.
     * 
     * @param screen
     */
    public void addScreen(ScreenMatrix screen) {
        if (colorCounts == null)
            init(screen);

        // Update color counts for all pixels
        for (int x = 0; x < screen.width; x++)
            for (int y = 0; y < screen.height; y++) {
                int c = screen.matrix[x][y];

                if (c < 0 || c >= numColors)
                    throw new UnsupportedOperationException("Color "+c+" out of range.");
                else
                    colorCounts[c].matrix[x][y]++;
            }
    }

    /** Extract a background based on the most frequently occuring colors.
     * 
     * @return
     */
    public ScreenMatrix getMostFrequent() {
        if (colorCounts == null)
            throw new UnsupportedOperationException("Background samples are needed to extract background.");

        int width = colorCounts[0].width;
        int height = colorCounts[0].height;

        ScreenMatrix background = new ScreenMatrix(width, height);

        boolean hasTies = false;

        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++) {
                int maxCount = 0;
                int maxColor = 0;

                // For each pixel, find its most frequent color
                for (int c = 0; c < numColors; c++) {
                    int count = colorCounts[c].matrix[x][y];

                    if (count > maxCount) {
                        maxColor = c;
                        maxCount = count;
                    }
                    else if (count == maxCount)
                        hasTies = true;
                }

                // Set this pixel's color to the most frequent
                background.matrix[x][y] = maxColor;
            }

        if (hasTies)
            System.err.println ("WARNING: Background has tied pixel colors.");

        return background;
    }

}
