/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nips11.agents.models;

//import Jama.Matrix;
import java.io.Serializable;
import java.util.Arrays;
import nips11.agents.models.parallel.ParallelPredictor;

/**
 *
 * Defines a logistic regression model. Uses full vectors.
 *
 * @author Marc G. Bellemare
 */
public abstract class AbstractSparseLinearFamilyModelSO implements Serializable, Cloneable {
    public static final long serialVersionUID = -1465575451772999533L;
    
    public static transient boolean useExpectedNorm = true;
    public static transient boolean useMaximumNorm = false;
    public static transient boolean binaryFeaturesOptimization = true;
    
    /** Whether we should use the bias weights as well */
    protected boolean aUseBias = false;
    /** Learning rate for modifying weights */
    protected double aAlpha = 0.1;

    protected int aNumWeights;
    protected double aPrediction;
    protected double[] aWeights;
    protected double aBias;

    protected double[] aSavedWeights;
    protected double aSavedBias;

    protected double aNormAverage;

    protected final double aAveragingFactor = 0.999;
    
    /** For parallel predictions */
    protected transient ParallelPredictor parallel;
    
    /**
     * Create a new LogisticModel.
     *
     * @param pStateLength The length of the state vector used by this model.
     * @param pOutputLength The length of the output observation vector.
     */
    public AbstractSparseLinearFamilyModelSO(int pStateLength) {
        this(pStateLength, false);
    }

    public AbstractSparseLinearFamilyModelSO(int pStateLength, boolean pUseBias) {
        aUseBias = pUseBias;
        aNumWeights = pStateLength;

        aWeights = new double[aNumWeights];
    }

    public static void setUseMaximumNorm(boolean doUse) {
        if (doUse) {
            useExpectedNorm = false;
            useMaximumNorm = true;
        }
    }

    public void setAlpha(double pAlpha) {
        aAlpha = pAlpha;
    }

    public double getAlpha() {
        return aAlpha;
    }

    /** Makes this predictor predict in parallel */
    public void parallelize(int numThreads) {
        if (numThreads > 1)
            parallel = new ParallelPredictor(aWeights, binaryFeaturesOptimization, numThreads);
        else if (numThreads == 1)
            parallel = null;
        else
            throw new IllegalArgumentException("Unsupported number of threads: "+numThreads);
    }

    public int getNumInputs() {
        return aNumWeights;
    }
    
    public double[] getWeights() {
        return aWeights;
    }

    public boolean getUseBias() {
        return aUseBias;
    }

    public double getPrediction() {
        return aPrediction;
    }

    public abstract double function(double x);
    /** Returns the partial derivative of y (a function as above) wrt to the
     *   weight corresponding to x. The full derivative is obtained as
     *   derivative(y) * x. This works for linear and logistic models.
     */
    public abstract double derivative(double y);
    
    public double predict(FeatureList pState) {
        if (parallel != null) {
            aPrediction = function(parallel.sum(pState) + aBias);
            return aPrediction;
        }
        else if(binaryFeaturesOptimization)
            return predictBinaryFeatures(pState);
        else
            return predictFull(pState);
    }

    public double predictFull(FeatureList pState) {
        aPrediction = 0.0;

        for (FeatureList.Pair p : pState.active) {
            int index = p.index;
            double value = p.value;
            
            aPrediction += aWeights[index] * value;
        }

        // Add bias
        aPrediction += aBias;
        // Squash by function
        aPrediction = function(aPrediction);

        return aPrediction;
    }

    private double predictBinaryFeatures(FeatureList pState) {
        aPrediction = 0.0;

        for (FeatureList.Pair p : pState.active) {
            int index = p.index;

            aPrediction += aWeights[index];
        }

        // Add bias
        aPrediction += aBias;
        // Squash by function
        aPrediction = function(aPrediction);

        return aPrediction;
    }

    public double predict(StorageBinaryFeatureList pState) {
        return predict(pState.active);
    }

    public double predict(int[] features) {
        aPrediction = 0.0;

        for (int i = 0; i < features.length; i++) {
            int index = features[i];

            aPrediction += aWeights[index];
        }

        // Add bias
        aPrediction += aBias;
        // Squash by function
        aPrediction = function(aPrediction);

        return aPrediction;
    }

    protected double getModifiedAlpha(FeatureList pLastState) {
        double alpha = 0;

        if (useExpectedNorm) {
            double vecNorm = pLastState.sum();

            if (aNormAverage == 0.0) {
                aNormAverage = vecNorm;
            }
            else
                aNormAverage = vecNorm + aAveragingFactor * (aNormAverage - vecNorm);

            if (aNormAverage > 0.0) alpha = aAlpha / aNormAverage;
            else alpha = aAlpha;
        }
        else if (useMaximumNorm) {
            double vecNorm = pLastState.sum();

            if (vecNorm > aNormAverage)
                aNormAverage = vecNorm;

            if (aNormAverage > 0.0) alpha = aAlpha / aNormAverage;
            else alpha = aAlpha;
        }
        else {
            alpha = aAlpha;
        }

        return alpha;
    }

    protected double getModifiedAlpha(StorageBinaryFeatureList pLastState) {
        double alpha = 0;

        if (useExpectedNorm) {
            double vecNorm = pLastState.sum();

            if (aNormAverage == 0.0) {
                aNormAverage = vecNorm;
            }
            else
                aNormAverage = vecNorm + aAveragingFactor * (aNormAverage - vecNorm);

            if (aNormAverage > 0.0) alpha = aAlpha / aNormAverage;
            else alpha = aAlpha;
        }
        else if (useMaximumNorm) {
            double vecNorm = pLastState.sum();

            if (vecNorm > aNormAverage)
                aNormAverage = vecNorm;

            if (aNormAverage > 0.0) alpha = aAlpha / aNormAverage;
            else alpha = aAlpha;
        }
        else {
            alpha = aAlpha;
        }

        return alpha;
    }

    /** Updates the weights matrix via a simple gradient descent update such that
     *   W * I(s,a,o) = t, where I(s,a,o) is the input vector built from the state,
     *   action and observations and t is the next state. W is the matrix of weights
     *   to be optimized.
     *
     *  A call to predict() MUST be made prior to calling this function!
     * 
     * @param pState
     * @param pTarget
     */
    public void updateWeights(FeatureList pLastState, double pTarget) {
        double alpha = getModifiedAlpha(pLastState);

        // Compute the error vector, update the bias
        double error;

        error = pTarget - aPrediction;

        double alphaError = alpha * error;

        if (aUseBias) {
            aBias += alphaError;
        }

        double der = derivative(aPrediction);

        double alphaErrorDer = alphaError * der;

        // Standard gradient descent
        for (FeatureList.Pair p : pLastState.active) {
            int index = p.index;
            double value = p.value;

            aWeights[index] += alphaErrorDer * value;
        }
    }

    /** Updates the weights by a 'delta' gradient-ish quantity (e.g., TD
     *   error). This update function does not (and should not) depend on the
     *   last prediction made by the model.
     * 
     * @param pLastState
     * @param pTarget
     */
    public void updateWeightsDelta(FeatureList pLastState, double pDelta) {
        // Early out for the case where alpha is set to 0
        if (aAlpha == 0.0 || pDelta == 0.0) return;
        
        double alpha = getModifiedAlpha(pLastState);

        double alphaDelta = alpha * pDelta;
        
        // Update the bias
        aBias += alphaDelta;

        // Update other weights
        // @todo handle sigmoid
        for (FeatureList.Pair p : pLastState.active) {
            int index = p.index;
            double value = p.value;

            aWeights[index] += alphaDelta * value;
        }
    }

    public void updateWeightsDelta(StorageBinaryFeatureList pState, double pDelta) {
        // Early out for the case where alpha is set to 0
        if (aAlpha == 0.0) return;

        double alpha = getModifiedAlpha(pState);

        double alphaDelta = alpha * pDelta;

        // Update the bias
        aBias += alphaDelta;

        // Update other weights
        // @todo handle sigmoid
        for (int i = 0; i < pState.active.length; i++) {
            int index = pState.active[i];

            aWeights[index] += alphaDelta;
        }
    }

    /** Updates weights using delta; the bias is updated with its own special term. */
    public void updateWeightsDelta(FeatureList pLastState, double pDelta, double pBiasDelta) {
        // Early out for the case where alpha is set to 0
        if (aAlpha == 0.0) return;

        double alpha = getModifiedAlpha(pLastState);

        double alphaDelta = alpha * pDelta;

        // Update the bias
        aBias += pBiasDelta * alpha;

        // Update other weights
        // @todo handle sigmoid
        for (FeatureList.Pair p : pLastState.active) {
            int index = p.index;
            double value = p.value;

            aWeights[index] += alphaDelta * value;
        }
    }

    /** Set all weights, and the bias, to the given value.
     * 
     * @param w
     */
    public void setWeights(double w) {
        Arrays.fill(aWeights, w);
        aBias = w;

        aSavedWeights = null;
        aSavedBias = 0.0;
    }

    public void setBias(double v) {
        aBias = v;
        aSavedBias = 0.0;
    }

    public void setWeights(int min, int max, double v) {
        Arrays.fill(aWeights, min, max, v);
        
        aSavedWeights = null;
        aSavedBias = 0.0;
    }

    /* @todo public void saveState() {
        if (aSavedWeights == null) {
            aSavedWeights = new double[aNumWeights][aNumOutputs];
            aSavedBias = new double[aNumOutputs];
        }
        for (int i = 0; i < aWeights.length; i++) {
            System.arraycopy(aWeights[i], 0, aSavedWeights[i], 0, aWeights[i].length);
        }

        System.arraycopy(aBias, 0, aSavedBias, 0, aBias.length);
    } */

    /** Reset the weights to the last saved state; clears the gradients.
     *
     */
    /* @todo
    public void loadState() {
        if (aSavedWeights == null) return;

        for (int i = 0; i < aWeights.length; i++) {
            System.arraycopy(aSavedWeights[i], 0, aWeights[i], 0, aSavedWeights[i].length);
        }

        System.arraycopy(aSavedBias, 0, aBias, 0, aSavedBias.length);
    } */

    @Override
    public Object clone() {
        AbstractSparseLinearFamilyModelSO model;
        try {
            model = (AbstractSparseLinearFamilyModelSO) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }

        model.aUseBias = aUseBias;
        model.aAlpha = aAlpha;
        model.aBias = aBias;
        model.aNumWeights = aNumWeights;

        model.aPrediction = aPrediction;
        model.aWeights = (double[]) aWeights.clone();

        return model;
    }
}
