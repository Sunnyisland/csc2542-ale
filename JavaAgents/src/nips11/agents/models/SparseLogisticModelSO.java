/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.models;

import marcgb.tools.Functions;

/**
 * A linear model with a sigmoid squash.
 *
 * @author marc
 */
public class SparseLogisticModelSO extends AbstractSparseLinearFamilyModelSO {
    public SparseLogisticModelSO(int pStateLength) {
        super(pStateLength);
    }

    public SparseLogisticModelSO(int pStateLength, boolean pUseBias) {
        super(pStateLength, pUseBias);
    }

    @Override
    public double function(double x) {
        return Functions.sigmoid(x);
    }

    @Override
    public double derivative(double y) {
        return y * (1 - y);
    }

    public void updateWeightsDelta(double[] pLastState, double[] pDelta) {
        throw new UnsupportedOperationException("Delta updates not supported in LogisticModel.");
    }

    @Override
    public Object clone() {
        return super.clone();
    }
}
