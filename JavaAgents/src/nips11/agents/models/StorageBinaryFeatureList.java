/*
 * (c) 2009 Marc G. Bellemare.
 */

package nips11.agents.models;

import java.io.Serializable;

/** Encapsulates a list of active features with a small memory footprint; for
 *   storage purposes.
 *
 * @author marc
 */
public class StorageBinaryFeatureList implements Serializable {
    public final int[] active;
    
    public StorageBinaryFeatureList(FeatureList fv) {
        active = new int[fv.active.size()];

        int index = 0;

        for (FeatureList.Pair pair : fv.active) {
            if (pair.value != 1.0)
                throw new UnsupportedOperationException("Cannot clone FeatureList with non-1 elements.");
            this.active[index++] = pair.index;
        }
    }

    public StorageBinaryFeatureList(int[] active) {
        this.active = active;
    }
    
    @Override
    public String toString() {
        String s = "";

        for (int item : active) {
            s += (item+" ");
        }

        return s;
    }

    public String indicesString() {
        return toString();
    }

    public double sumSquares() {
        double s = 0;

        return active.length;
    }

    public double sum() {
        return active.length;
    }

    @Override
    public Object clone() {
        StorageBinaryFeatureList newList = new StorageBinaryFeatureList(this.active.clone());

        return newList;
    }
}
