/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nips11.agents.models;

//import Jama.Matrix;
import java.io.Serializable;
import java.util.Arrays;
import marcgb.tools.Functions;
import marcgb.tools.Matrices;

/**
 *
 * Defines a logistic regression model. Uses full vectors.
 *
 * @author Marc G. Bellemare
 */
public abstract class AbstractLinearFamilyModel implements Serializable, Cloneable {
    public static boolean useExpectedNorm = true;

    /** Whether we should use the bias weights as well */
    protected boolean aUseBias = false;
    /** Learning rate for modifying weights */
    protected double aAlpha = 0.1;

    protected int aNumWeights;
    protected int aNumOutputs;
    protected double[] aPrediction;
    protected double[][] aWeights;
    protected double[] aBias;

    protected double[][] aSavedWeights;
    protected double[] aSavedBias;

    protected double aNormAverage;

    protected final double aAveragingFactor = 0.999;
    

    /**
     * Create a new LogisticModel.
     *
     * @param pStateLength The length of the state vector used by this model.
     * @param pOutputLength The length of the output observation vector.
     */
    public AbstractLinearFamilyModel(int pStateLength, int pOutputLength) {
        this(pStateLength, pOutputLength, false);
    }

    public AbstractLinearFamilyModel(int pStateLength, int pOutputLength, boolean pUseBias) {
        aUseBias = pUseBias;
        aNumWeights = pStateLength;
        aNumOutputs = pOutputLength;

        aWeights = new double[aNumWeights][aNumOutputs];
        aBias = new double[aNumOutputs];
        aPrediction = new double[aNumOutputs];

    }

    public void setAlpha(double pAlpha) {
        aAlpha = pAlpha;
    }

    public double getAlpha() {
        return aAlpha;
    }

    public double[][] getWeights() {
        return aWeights;
    }

    public boolean getUseBias() {
        return aUseBias;
    }

    public double[] getPrediction() {
        return aPrediction;
    }

    public abstract double function(double x);
    /** Returns the partial derivative of y (a function as above) wrt to the
     *   weight corresponding to x. Basically a wrapper for sigmoid and linear functions.
     */
    public abstract double derivative(double y, double x);
    
    public double[] predict(double[] pState) {
        for (int j = 0; j < aNumOutputs; j++) {
            aPrediction[j] = 0.0;
        }

        for (int i = 0; i < pState.length; i++) {
            for (int j = 0; j < aNumOutputs; j++)
                aPrediction[j] += aWeights[i][j] * pState[i];
        }

        for (int j = 0; j < aNumOutputs; j++) {
            // Add bias
            aPrediction[j] += aBias[j];
            // Squash by function
            aPrediction[j] = function(aPrediction[j]);
        }

        return aPrediction;
    }

    protected double getModifiedAlpha(double[] pLastState) {
        double alpha = 0;

        if (useExpectedNorm) {
            double vecNorm = Matrices.getSum(pLastState);

            if (aNormAverage == 0.0) {
                aNormAverage = vecNorm;
            }
            else
                aNormAverage = vecNorm + aAveragingFactor * (aNormAverage - vecNorm);

            if (aNormAverage > 0.0) alpha = aAlpha / aNormAverage;
            else alpha = aAlpha;
        }
        else {
            alpha = aAlpha;
        }

        return alpha;
    }

    /** Updates the weights matrix via a simple gradient descent update such that
     *   W * I(s,a,o) = t, where I(s,a,o) is the input vector built from the state,
     *   action and observations and t is the next state. W is the matrix of weights
     *   to be optimized.
     *
     *  A call to predict() MUST be made prior to calling this function!
     * 
     * @param pState
     * @param pTarget
     */
    public void updateWeights(double[] pLastState, double[] pTarget) {
        double alpha = getModifiedAlpha(pLastState);

        // Compute the error vector, update the bias
        double[] errorVec = new double[pTarget.length];
        for (int j = 0; j < aNumOutputs; j++) {
            errorVec[j] = pTarget[j] - aPrediction[j];

            if (aUseBias) {
                aBias[j] += alpha * errorVec[j];
            }
        }

        // Standard gradient descent
        // @todo add sigmoid
        for (int index = 0; index < pLastState.length; index++) {
            double value = pLastState[index];

            for (int j = 0; j < aNumOutputs; j++) {
                aWeights[index][j] += alpha * (errorVec[j] * derivative(aPrediction[j], value));
            }
        }
    }

    /** Updates the weights by a 'delta' gradient-ish quantity (e.g., TD
     *   error). This update function does not (and should not) depend on the
     *   last prediction made by the model.
     * 
     * @param pLastState
     * @param pTarget
     */
    public void updateWeightsDelta(double[] pLastState, double[] pDelta) {
        double alpha = getModifiedAlpha(pLastState);

        // Update the bias
        for (int j = 0; j < aNumOutputs; j++) {
            if (aUseBias) {
                aBias[j] += alpha * pDelta[j];
            }
        }

        // Update other weights
        // @todo handle sigmoid
        for (int index = 0; index < pLastState.length; index++) {
            double value = pLastState[index];

            for (int j = 0; j < aNumOutputs; j++) {
                aWeights[index][j] += alpha * (pDelta[j] * value);
            }
        }
    }

    /** Set all weights, and the bias, to the given value.
     * 
     * @param w
     */
    public void setWeights(double w) {
        for (int i = 0; i < aWeights.length; i++) {
            Arrays.fill(aWeights[i], w);
        }

        Arrays.fill(aBias, w);

        aSavedWeights = null;
        aSavedBias = null;
    }

    public void saveState() {
        if (aSavedWeights == null) {
            aSavedWeights = new double[aNumWeights][aNumOutputs];
            aSavedBias = new double[aNumOutputs];
        }
        for (int i = 0; i < aWeights.length; i++) {
            System.arraycopy(aWeights[i], 0, aSavedWeights[i], 0, aWeights[i].length);
        }

        System.arraycopy(aBias, 0, aSavedBias, 0, aBias.length);
    }

    /** Reset the weights to the last saved state; clears the gradients.
     *
     */
    public void loadState() {
        if (aSavedWeights == null) return;

        for (int i = 0; i < aWeights.length; i++) {
            System.arraycopy(aSavedWeights[i], 0, aWeights[i], 0, aSavedWeights[i].length);
        }

        System.arraycopy(aSavedBias, 0, aBias, 0, aSavedBias.length);
    }

    @Override
    public Object clone() {
        AbstractLinearFamilyModel model;
        try {
            model = (AbstractLinearFamilyModel) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }

        model.aUseBias = aUseBias;
        model.aAlpha = aAlpha;
        model.aBias = aBias;
        model.aNumWeights = aNumWeights;
        model.aNumOutputs = aNumOutputs;

        model.aPrediction = (double[]) aPrediction.clone();
        model.aWeights = (double[][]) aWeights.clone();

        return model;
    }
}
