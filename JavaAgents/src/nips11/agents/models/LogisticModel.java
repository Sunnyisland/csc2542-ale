/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.models;

import marcgb.tools.Functions;

/**
 * A linear model with a sigmoid squash.
 *
 * @author marc
 */
public class LogisticModel extends AbstractLinearFamilyModel {
    public LogisticModel(int pStateLength, int pOutputLength) {
        super(pStateLength, pOutputLength);
    }

    public LogisticModel(int pStateLength, int pOutputLength, boolean pUseBias) {
        super(pStateLength, pOutputLength, pUseBias);
    }

    @Override
    public double function(double x) {
        return Functions.sigmoid(x);
    }

    @Override
    public double derivative(double y, double x) {
        return y * (1 - y) * x;
    }

    public void updateWeightsDelta(double[] pLastState, double[] pDelta) {
        throw new UnsupportedOperationException("Delta updates not supported in LogisticModel.");
    }

    @Override
    public Object clone() {
        return super.clone();
    }
}
