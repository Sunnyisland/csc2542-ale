/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.models;

import marcgb.tools.Functions;

/**
 * A linear model with a sigmoid squash.
 *
 * @author marc
 */
public class SparseLinearModel extends AbstractSparseLinearFamilyModel {
    public SparseLinearModel(int pStateLength, int pOutputLength) {
        super(pStateLength, pOutputLength);
    }

    public SparseLinearModel(int pStateLength, int pOutputLength, boolean pUseBias) {
        super(pStateLength, pOutputLength, pUseBias);
    }

    @Override
    public double function(double x) {
        return x;
    }

    @Override
    public double derivative(double y, double x) {
        return x;
    }

    @Override
    public Object clone() {
        return super.clone();
    }
}
