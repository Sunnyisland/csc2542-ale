/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.models;

import marcgb.tools.Functions;

/**
 * A linear model with a sigmoid squash.
 *
 * @author marc
 */
public class LogLikelihoodSparseLogisticModelSO extends AbstractSparseLinearFamilyModelSO {
    public LogLikelihoodSparseLogisticModelSO(int pStateLength) {
        super(pStateLength);
    }

    public LogLikelihoodSparseLogisticModelSO(int pStateLength, boolean pUseBias) {
        super(pStateLength, pUseBias);
    }

    @Override
    public double function(double x) {
        return Functions.sigmoid(x);
    }

    @Override
    public double derivative(double y) {
        // The derivative proper is y * (1 - y), but we pretend here that it is 1
        //  in order to perform the log-likelihood update
        return 1.0;
    }

    public void updateWeightsDelta(double[] pLastState, double[] pDelta) {
        throw new UnsupportedOperationException("Delta updates not supported in LogisticModel.");
    }

    @Override
    public Object clone() {
        return super.clone();
    }
}
