/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.models.parallel;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import nips11.agents.models.FeatureList;

/** A class that parallelizes prediction (matrix x vector multiplication).
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ParallelPredictor {
    protected ParallelPredictorThread[] threads;

    protected int numThreads;

    protected boolean started = false;
    
    public ParallelPredictor(double[] weights, boolean binaryFeatures, int numThreads) {
        this.numThreads = numThreads;

        // Create and start the thread objects
        threads = new ParallelPredictorThread[numThreads];
        for (int t = 0; t < threads.length; t++) {
            threads[t] = new ParallelPredictorThread(weights, binaryFeatures);
        }
    }

    private void start() {
        for (int t = 0; t < threads.length; t++)
            // The thread will now wait for request to compute
            threads[t].start();

        started = true;
    }

    public double sum(FeatureList features) {
        // Start the threads if necessary
        if (!started)
            start();

        int numFeatures = features.active.size();
        int featuresPerThread = numFeatures / numThreads;

        CountDownLatch latch = new CountDownLatch(numThreads);

        int index = 0;

        // Request that each thread compute a bit of this data
        for (int t = 0; t < numThreads; t++) {
            List<FeatureList.Pair> section;

            if (t < numThreads - 1)
                section = features.active.subList(index, index + featuresPerThread);
            else // Last thread gets the remainder
                section = features.active.subList(index, features.active.size());

            index += section.size();

            // Pass that bit of computation to the thread
            threads[t].requestCompute(latch, section);
        }

        try {
            latch.await();
        }
        catch (InterruptedException e) {
            System.err.println ("Processing interrupted!");
        }

        // Sum up the result
        double sum = 0;
        for (int t = 0; t < numThreads; t++) {
            sum += threads[t].getSum();
        }

        return sum;
    }
}
