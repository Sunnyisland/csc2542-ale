/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.models.parallel;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import nips11.agents.models.FeatureList;

/** A single computation thread for ParallelPredictor.
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class ParallelPredictorThread extends Thread {
    /** Mutex on this object */
    protected final Object synchObj = new Object();

    protected CountDownLatch latch;
    protected double accumulator;
    protected List<FeatureList.Pair> section;

    protected boolean computeRequested = false;
    protected boolean closeRequested = false;

    protected final double[] weights;
    protected final boolean binaryFeatures;

    public ParallelPredictorThread(double[] weights, boolean binary) {
        this.weights = weights;
        this.binaryFeatures = binary;
    }

    public void requestCompute(CountDownLatch latch, List<FeatureList.Pair> section) {
        synchronized(synchObj) {
            this.latch = latch;
            this.section = section;
            computeRequested = true;
            synchObj.notify();
        }
    }

    public void close() {
        synchronized(synchObj) {
            closeRequested = true;
        }
    }

    public double getSum() {
        return accumulator;
    }

    @Override
    public void run() {
        // Sit there until we are closed...
        while (!closeRequested) {
            // Wait for the computeRequested flag
            while (!closeRequested && !computeRequested) {
                synchronized(synchObj) {
                    try {
                        synchObj.wait();
                    }
                    catch (InterruptedException e) {
                        closeRequested = true;
                        System.err.println ("Interrupted!");
                    }
                }
            }

            // Now perform computation
            if (computeRequested) {
                synchronized (synchObj) {
                    try {
                        //System.err.println("Computing! " +section.get(0).index);
                    compute();
                    } finally {
                        latch.countDown();
                        computeRequested = false;
                    }
                }
            }
        }
    }

    private void compute() {
        if (binaryFeatures)
            computeBinary();
        else
            computeFull();
    }

    private void computeBinary() {
        this.accumulator = 0;

        for (FeatureList.Pair p : section) {
            this.accumulator += weights[p.index];
        }
    }

    private void computeFull() {
        this.accumulator = 0;

        for (FeatureList.Pair p : section) {
            this.accumulator += weights[p.index] * p.value;
        }
    }
}
