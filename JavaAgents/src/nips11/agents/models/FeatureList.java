/*
 * (c) 2009 Marc G. Bellemare.
 */

package nips11.agents.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/** Encapsulates a list of active features.
 *
 * @author marc
 */
public class FeatureList {
    public class Pair implements Comparable {
        public int index;
        public float value;

        public Pair(int _index, double _value) {
            this (_index, (float)_value);
        }
        
        public Pair(int _index, float _value) {
            index = _index;
            value = _value;
        }

        /** Compare by index.
         * 
         * @param arg
         * @return
         */
        public int compareTo(Object arg) {
            Pair p = (Pair)arg;

            return this.index - p.index;
        }
    }

    public List<Pair> active;
    
    public FeatureList() {
        this(false);
    }

    public FeatureList(boolean pLinkedListBacked) {
        if (pLinkedListBacked) {
            active = new LinkedList<Pair>();
        }
        else {
            active = new ArrayList<Pair>();
        }
    }

    /** Add a feature (via its index in the full vector).
     * 
     * @param pIndex
     */
    public void addFeature(int pIndex) {
        addFeature(pIndex, 1.0);
    }

    public void addFeature(int pIndex, double pValue) {
        active.add(new Pair(pIndex, pValue));
    }

    /** Adds a feature; if the feature already exists, the two values are
     *   combined.
     * 
     * @param pIndex
     * @param pValue
     */
    public void addUniqueFeature(int pIndex, double pValue) {
        for (Pair pair : active) {
            if (pair.index == pIndex) {
                pair.value += pValue;
                return;
            }
        }

        addFeature(pIndex, pValue);
    }

    public void addUnique(FeatureList pList) {
        for (Pair pair : pList.active) {
            addUniqueFeature(pair.index, pair.value);
        }
    }

    public void subtractUnique(FeatureList pList) {
        for (Pair pair : pList.active) {
            addUniqueFeature(pair.index, -pair.value);
        }
    }


    /** Normalizes the feature values in this FeatureList so that they sum to
     *   1.0.
     */
    public void normalize() {
        double valueSum = this.sum();

        // In general this won't be true, but it doesn't hurt to check
        if (valueSum != 1.0) {
            for (Pair feature : active) {
                feature.value /= valueSum;
            }
        }
    }

    /** Remove all duplicate (index-wise) elements.
     * 
     */
    public void removeDuplicates() {
        Object[] pairs = active.toArray();
        Arrays.sort(pairs);

        for (int index = 0; index < pairs.length-1; index++) {
            Pair p1 = (Pair)(pairs[index]);
            Pair p2 = (Pair)(pairs[index+1]);

            if (p1.index == p2.index) {
                // Remove the duplicate element
                for (ListIterator<Pair> it = active.listIterator(); it.hasNext(); ) {
                    Pair p = it.next();

                    if (p.index == p2.index) {
                        it.remove();
                        break;
                    }
                }
                
            }
        }
    }

    public void clearList() {
        active.clear();
    }

    /** Handles this FeatureList as an eligibility trace vector. Decays the
     *   value of all existing elements, adds new ones as needed and removes
     *   values below a given threshold.
     * @param pLambda
     */
    public void updateTrace(FeatureList pState, double pLambda, double pThreshold) {
        // Assume the two lists are sorted
        ListIterator<Pair> myIt = active.listIterator();
        ListIterator<Pair> itsIt = pState.active.listIterator();

        boolean pointerOn;

        // Test for sortedness - in case I mess up again
        if (Math.random() < 0.001) checkSorted();

        while (myIt.hasNext()) {
            // Peek at the next element in this list
            boolean added = false;
            Pair myPair = myIt.next();
            pointerOn = true;

            int currentItem = myPair.index;
            double currentValue = myPair.value;
            
            // Add all elements up to myIt; if we find our match, handle it
            //  properly
            while (itsIt.hasNext()) {
                Pair itsPair = itsIt.next();

                int newItem = itsPair.index;
                double newValue = itsPair.value;

                // Process this one and move on to the next element in the
                //   outer list
                if (newItem == currentItem) {
                    // Accumulating traces? no
                    // double val = pLambda * currentValue + newValue;
                    double val = Math.min(pLambda * currentValue + newValue, 1.0);

                    // Update the trace value
                    myPair.value = (float)val;

                    added = true;
                    break;
                }
                else if (newItem > currentItem) {
                    itsIt.previous();
                    break;
                }
                else {// Not found in our list, add it
                    // Go one element back if necessary
                    if (pointerOn) {
                        myIt.previous();
                        pointerOn = false;
                    }
                    // @todo can I avoid creating a new pair here?
                    myIt.add(new Pair(newItem, newValue));
                }
            }

            if (!added) {
                double decayedValue = pLambda * currentValue;

                if (decayedValue > pThreshold) {
                    // Keep the feature but decay its value
                    myPair.value = (float)decayedValue;
                }
                else {
                    // Remove element
                    if (!pointerOn) {
                        myIt.next();
                        pointerOn = true;
                    }
                    
                    myIt.remove();
                }
            }

            if (!pointerOn) {
                // Move the iterator so that the next call to next() returns
                //  the next pair
                myIt.next();
            }
        }

        // Add remainder
        while (itsIt.hasNext()) {
            Pair itsPair = itsIt.next();
            int newItem = itsPair.index;
            double newValue = itsPair.value;

            // @todo add pairs directly rather than clone?
            active.add(new Pair(newItem, newValue));
        }
    }

    /** Updates traces based on a binary feature vector input. */
    public void updateTrace(StorageBinaryFeatureList pState, double pLambda, double pThreshold) {
        // Assume the two lists are sorted
        ListIterator<Pair> myIt = active.listIterator();
        int itsIt = 0;

        boolean pointerOn;

        // Test for sortedness - in case I mess up again
        if (Math.random() < 0.001) checkSorted();

        while (myIt.hasNext()) {
            // Peek at the next element in this list
            boolean added = false;
            Pair myPair = myIt.next();
            pointerOn = true;

            int currentItem = myPair.index;
            double currentValue = myPair.value;

            // Add all elements up to myIt; if we find our match, handle it
            //  properly
            for (; itsIt < pState.active.length; ) {
                int newItem = pState.active[itsIt++];
                double newValue = 1.0;

                // Process this one and move on to the next element in the
                //   outer list
                if (newItem == currentItem) {
                    // Accumulating traces? no
                    // double val = pLambda * currentValue + newValue;
                    double val = Math.min(pLambda * currentValue + newValue, 1.0);

                    // Update the trace value
                    myPair.value = (float)val;

                    added = true;
                    break;
                }
                else if (newItem > currentItem) {
                    itsIt--;
                    break;
                }
                else {// Not found in our list, add it
                    // Go one element back if necessary
                    if (pointerOn) {
                        myIt.previous();
                        pointerOn = false;
                    }
                    // @todo can I avoid creating a new pair here?
                    myIt.add(new Pair(newItem, newValue));
                }
            }

            if (!added) {
                double decayedValue = pLambda * currentValue;

                if (decayedValue > pThreshold) {
                    // Keep the feature but decay its value
                    myPair.value = (float)decayedValue;
                }
                else {
                    // Remove element
                    if (!pointerOn) {
                        myIt.next();
                        pointerOn = true;
                    }

                    myIt.remove();
                }
            }

            if (!pointerOn) {
                // Move the iterator so that the next call to next() returns
                //  the next pair
                myIt.next();
            }
        }

        // Add remainder
        while (itsIt < pState.active.length) {
            int newItem = pState.active[itsIt++];

            active.add(new Pair(newItem, 1.0));
        }
    }

    /** Similar to updateTraces; but adds pState to this vector instead, weighted
      * by pWeight. Used by PolicyGradientSarsa. Also, traces don't accumulate. */
    public void addFeatures(FeatureList pState, double pWeight) {
        // Assume the two lists are sorted
        ListIterator<Pair> myIt = active.listIterator();
        ListIterator<Pair> itsIt = pState.active.listIterator();

        boolean pointerOn;

        // Test for sortedness - in case I mess up again
        if (Math.random() < 0.001) checkSorted();

        while (myIt.hasNext()) {
            // Peek at the next element in this list
            boolean added = false;
            Pair myPair = myIt.next();
            pointerOn = true;

            int currentItem = myPair.index;
            double currentValue = myPair.value;

            // Add all elements up to myIt; if we find our match, handle it
            //  properly
            while (itsIt.hasNext()) {
                Pair itsPair = itsIt.next();

                int newItem = itsPair.index;
                double newValue = itsPair.value;

                // Process this one and move on to the next element in the
                //   outer list
                if (newItem == currentItem) {
                    double val = pWeight * newValue + currentValue;

                    // Update the trace value
                    myPair.value = (float)val;

                    added = true;
                    break;
                }
                else if (newItem > currentItem) {
                    itsIt.previous();
                    break;
                }
                else {// Not found in our list, add it
                    // Go one element back if necessary
                    if (pointerOn) {
                        myIt.previous();
                        pointerOn = false;
                    }
                    // @todo can I avoid creating a new pair here?
                    myIt.add(new Pair(newItem, pWeight * newValue));
                }
            }

            if (!added) {
                // Do nothing
            }

            if (!pointerOn) {
                // Move the iterator so that the next call to next() returns
                //  the next pair
                myIt.next();
            }
        }

        // Add remainder
        while (itsIt.hasNext()) {
            Pair itsPair = itsIt.next();
            int newItem = itsPair.index;
            double newValue = itsPair.value;

            // @todo add pairs directly rather than clone?
            active.add(new Pair(newItem, pWeight * newValue));
        }
    }

    /** Computes the hamming distance between the two vectors. */
    public int hammingDistance(FeatureList pState) {
        // Assume the two lists are sorted
        ListIterator<Pair> myIt = active.listIterator();
        ListIterator<Pair> itsIt = pState.active.listIterator();

        boolean pointerOn;

        // Test for sortedness - in case I mess up again
        if (Math.random() < 0.001) checkSorted();

        int numDifferent = 0;
        
        while (myIt.hasNext()) {
            // Peek at the next element in this list
            Pair myPair = myIt.next();
            pointerOn = true;

            int currentItem = myPair.index;

            // Count all elements up to myIt
            while (itsIt.hasNext()) {
                Pair itsPair = itsIt.next();

                int newItem = itsPair.index;

                // Stop when currentItem is reached or passed
                if (newItem == currentItem)
                    break;
                else if (newItem > currentItem) {
                    // This item doesn't exist in the other list; count it
                    numDifferent++;
                    itsIt.previous();
                    break;
                }
                else {
                    // Count
                    numDifferent++;
                    if (pointerOn) {
                        myIt.previous();
                        pointerOn = false;
                    }
                }
            }
            if (!pointerOn) {
                // Move the iterator so that the next call to next() returns
                //  the next pair
                myIt.next();
            }
        }

        // Count remainder
        while (itsIt.hasNext()) {
            Pair itsPair = itsIt.next();
            numDifferent++;
        }

        return numDifferent;
    }

    private void checkSorted() {
        int lastIndex = 0;
        boolean first = true;

        for (FeatureList.Pair p : active) {
            if (!first) {
                if (lastIndex > p.index) {
                    // @dbg
                    /* System.err.println ("P "+copy.indicesString());
                    System.err.println ("A "+add.indicesString());
                    System.err.println ("R "+this.indicesString()); */
                    throw new RuntimeException("Unsorted feature set used with updateTrace(). Indices: "+lastIndex+" "+p.index);
                }
                else if (lastIndex == p.index)
                    throw new RuntimeException("Feature set with duplicates used with updateTrace(). Index: "+lastIndex);
            }

            first = false;
            lastIndex = p.index;
        }
    }

    public void addToAverage(FeatureList pState, double pThisWeight, double pStateWeight) {
        this.addToAverage(pState, pThisWeight, pStateWeight, 0.0);
    }
    
    /** Somewhat of a hack function; averages this feature vector with a new
     *    one, using the given weights. If A is the current vector and B the
     *    new ones, with weights u and v, then the result is (u*A + v*B)/(u+v).
     *
     * @param pState
     * @param pThisWeight The weight to be given to the existing feature set.
     * @param pStateWeight The weight given to pState.
     * @param pThreshold Elements below pThreshold are deleted.
     */
    public void addToAverage(FeatureList pState, double pThisWeight, 
            double pStateWeight, double pThreshold) {
        double totalSupport = pThisWeight + pStateWeight;
        
        // Assume the two lists are sorted
        ListIterator<Pair> myIt = active.listIterator();
        ListIterator<Pair> itsIt = pState.active.listIterator();

        while (myIt.hasNext()) {
            boolean added = false;

            Pair currentPair = myIt.next();

            // Add all elements up to myIt; if we find our match, handle it
            //  properly
            while (itsIt.hasNext()) {
                Pair newPair = itsIt.next();

                // Process this one and move on to the next element in the
                //   outer list
                if (currentPair.index == newPair.index) {
                    double val = (pThisWeight * currentPair.value +
                            pStateWeight * newPair.value) / totalSupport;

                    // Modify the current feature
                    currentPair.value = (float)val;

                    added = true;
                    break;
                }
                else if (newPair.index > currentPair.index) {
                    itsIt.previous();
                    break;
                }
                else {// Not found in our list, add it before the current item
                    myIt.previous();
                    myIt.add(new Pair(newPair.index, newPair.value * pStateWeight / totalSupport));
                    myIt.next();
                }
            }

            if (!added) {
                // Keep the feature but re-weight its value
                double reweightedValue = currentPair.value * pThisWeight / totalSupport;

                // To clean up the feature vector, we get rid of old small elements
                // @todo - bad: support should be updated to reflect this?
                if (reweightedValue > pThreshold) {
                    currentPair.value = (float)reweightedValue;
                }
            }
        }

        // Add remainder
        while (itsIt.hasNext()) {
            Pair newPair = itsIt.next();

            myIt.add(new Pair(newPair.index, newPair.value * pStateWeight / totalSupport));
        }
    }

    @Override
    public String toString() {
        String s = "";
        int index = 0;

        for (Pair item : active) {
            s += (item.index+":"+item.value+" ");
            index++;
        }

        return s;
    }

    public String indicesString() {
        String s = "";
        int index = 0;

        for (Pair item : active) {
            s += (item.index+" ");
            index++;
        }

        return s;
    }

    public double sumSquares() {
        double s = 0;

        for (Pair v : active) {
            double dv = v.value;
            s += dv*dv;
        }

        return s;
    }

    public double sum() {
        double s = 0;

        for (Pair v : active) {
            double dv = v.value;
            s += dv;
        }

        return s;
    }

    public static FeatureList fromArray(double[] array) {
        FeatureList features = new FeatureList();

        for (int i = 0; i < array.length; i++) {
            features.addFeature(i, array[i]);
        }

        return features;
    }

    @Override
    public Object clone() {
        FeatureList newList = new FeatureList();

        for (Pair pair : this.active) {
            newList.active.add(new Pair(pair.index, pair.value));
        }

        return newList;
    }
}
