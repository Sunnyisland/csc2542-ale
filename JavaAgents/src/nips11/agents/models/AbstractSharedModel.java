/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nips11.agents.models;

//import Jama.Matrix;
import java.io.Serializable;
import java.util.Arrays;
import marcgb.tools.Functions;
import marcgb.tools.Matrices;

/**
 *
 * Defines a logistic regression model. Based on AbstractSparseLinearFamilyModelSO.
 *  Only the non-shared part is regularized.
 *
 * @author Marc G. Bellemare
 */
public abstract class AbstractSharedModel implements Serializable, Cloneable {
    public static boolean useExpectedNorm = true;

    /** Whether we should use the bias weights as well */
    protected boolean aUseBias = false;
    /** Learning rate for modifying weights */
    protected double aAlpha = 0.1;

    protected int aNumWeights;
    protected double aPrediction;
    protected double[] aWeights;
    protected double[] aSharedWeights;
    protected double[] aSharedBias;
    protected double aBias;

    protected double aNormAverage;

    protected double aL2Regularization;

    protected final double aAveragingFactor = 0.999;
    

    public AbstractSharedModel(int pStateLength, boolean pUseBias) {
        this(pStateLength, new double[pStateLength], new double[1], pUseBias);
    }

    /**
     * Create a new LogisticModel.
     *
     * @param pStateLength The length of the state vector used by this model.
     * @param pOutputLength The length of the output observation vector.
     */
    public AbstractSharedModel(int pStateLength, double[] pSharedWeights, double[] pSharedBias) {
        this(pStateLength, pSharedWeights, pSharedBias, false);
    }

    public AbstractSharedModel(int pStateLength, double[] pSharedWeights, double[] pSharedBias, boolean pUseBias) {
        aUseBias = pUseBias;
        aNumWeights = pStateLength;
        aSharedWeights = pSharedWeights;
        aSharedBias = pSharedBias;

        aWeights = new double[aNumWeights];
    }

    public void setAlpha(double pAlpha) {
        aAlpha = pAlpha;
    }

    public double getAlpha() {
        return aAlpha;
    }

    public void setL2Regularization(double pCoefficient) {
        aL2Regularization = pCoefficient;
    }

    public void setShared(double[] pWeights, double[] pBias) {
        aSharedWeights = pWeights;
        aSharedBias = pBias;
    }

    public double[] getSharedWeights() { return aSharedWeights; }
    public double[] getSharedBias() { return aSharedBias; }
    
    public int getNumInputs() {
        return aNumWeights;
    }
    
    public double[] getWeights() {
        return aWeights;
    }

    public boolean getUseBias() {
        return aUseBias;
    }

    public double getPrediction() {
        return aPrediction;
    }

    public abstract double function(double x);
    /** Returns the partial derivative of y (a function as above) wrt to the
     *   weight corresponding to x. The full derivative is obtained as
     *   derivative(y) * x. This works for linear and logistic models.
     */
    public abstract double derivative(double y);
    
    public double predict(FeatureList pState) {
        aPrediction = 0.0;

        for (FeatureList.Pair p : pState.active) {
            int index = p.index;
            double value = p.value;
            
            aPrediction += aWeights[index] * value;
            aPrediction += aSharedWeights[index] * value;
        }

        // Add bias
        aPrediction += aBias;
        aPrediction += aSharedBias[0];
        
        // Squash by function
        aPrediction = function(aPrediction);

        return aPrediction;
    }

    protected double getModifiedAlpha(FeatureList pLastState) {
        double alpha = 0;

        if (useExpectedNorm) {
            double vecNorm = pLastState.sum();

            if (aNormAverage == 0.0) {
                aNormAverage = vecNorm;
            }
            else
                aNormAverage = vecNorm + aAveragingFactor * (aNormAverage - vecNorm);

            if (aNormAverage > 0.0) alpha = aAlpha / aNormAverage;
            else alpha = aAlpha;
        }
        else {
            alpha = aAlpha;
        }

        // Divide by two because we also have shared weights
        return alpha / 2;
    }
    /** Updates the weights of this model. The actual prediction is provided
     *    separately to allow for shared-weight models.
     * 
     * @param pLastState
     * @param pTarget
     */
    public void updateWeights(FeatureList pLastState, double pTarget) {
        double alpha = getModifiedAlpha(pLastState);

        // Compute the error vector, update the bias
        double error;

        error = pTarget - aPrediction;

        double alphaError = alpha * error;
        double alphaReg = alpha * aL2Regularization;

        if (aUseBias) {
            aBias += alphaError - alphaReg * aBias;
            // No regularization for the shared part
            aSharedBias[0] += alphaError;
        }

        double der = derivative(aPrediction);

        double alphaErrorDer = alphaError * der;

        // Standard gradient descent
        for (FeatureList.Pair p : pLastState.active) {
            int index = p.index;
            double value = p.value;

            aWeights[index] += alphaErrorDer * value - alphaReg * aWeights[index];
            // No regularization for the shared part
            aSharedWeights[index] += alphaErrorDer * value;
        }
    }

    /** Updates the weights by a 'delta' gradient-ish quantity (e.g., TD
     *   error). This update function does not (and should not) depend on the
     *   last prediction made by the model.
     * 
     * @param pLastState
     * @param pTarget
     */
    public void updateWeightsDelta(FeatureList pLastState, double pDelta) {
        // Early out for the case where alpha is set to 0
        if (aAlpha == 0.0) return;
        
        double alpha = getModifiedAlpha(pLastState);

        double alphaDelta = alpha * pDelta;
        
        // Update the bias
        // @todo Unregularized - is this what we want?
        aBias += alphaDelta;
        aSharedBias[0] += alphaDelta;

        // Update other weights
        // @todo handle sigmoid
        for (FeatureList.Pair p : pLastState.active) {
            int index = p.index;
            double value = p.value;

            // @todo Unregularized - is this what we want?
            aWeights[index] += alphaDelta * value;
            aSharedWeights[index] += alphaDelta * value;
        }
    }

    /** Set all weights, and the bias, to the given value.
     * 
     * @param w
     */
    public void setWeights(double w) {
        Arrays.fill(aWeights, w);
        aBias = w;
    }

    @Override
    public Object clone() {
        AbstractSharedModel model;
        try {
            model = (AbstractSharedModel) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }

        model.aUseBias = aUseBias;
        model.aAlpha = aAlpha;
        model.aBias = aBias;
        model.aNumWeights = aNumWeights;
        model.aL2Regularization = aL2Regularization;

        model.aPrediction = aPrediction;
        model.aWeights = (double[]) aWeights.clone();
        model.aSharedWeights = (double[])aSharedWeights.clone();
        model.aSharedBias = (double[])aSharedBias.clone();

        return model;
    }
}
