/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nips11.agents.models;

/**
 * A linear model with a sigmoid squash.
 *
 * @author marc
 */
public class SparseLinearModelSO extends AbstractSparseLinearFamilyModelSO {
    public SparseLinearModelSO(int pStateLength) {
        super(pStateLength);
    }

    public SparseLinearModelSO(int pStateLength, boolean pUseBias) {
        super(pStateLength, pUseBias);
    }

    @Override
    public double function(double x) {
        return x;
    }

    @Override
    public double derivative(double y) {
        return 1.0;
    }

    @Override
    public Object clone() {
        return super.clone();
    }
}
