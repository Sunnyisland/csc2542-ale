/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package features.parameters;

import rlVizLib.general.ParameterHolder;

/** General parameters for feature generation methods.
 *
 * @author marc
 */
public class FeatureParameters {
    public static final String PARAM_HISTORY_LENGTH = "features-history-length";
    public static final String PARAM_ENCODE_LAST_ACTION = "features-encode-last-action";
    public static final String PARAM_JOINT_ENCODING = "features-joint-encoding";

    public static int historyLength;
    public static boolean encodeLastAction;
    public static boolean jointEncoding;
    
    public static void addParameters(ParameterHolder params) {
        if (params.isParamSet(PARAM_HISTORY_LENGTH)) return;

        params.addIntegerParam(PARAM_HISTORY_LENGTH, 1);
        params.addBooleanParam(PARAM_ENCODE_LAST_ACTION, false);
        params.addBooleanParam(PARAM_JOINT_ENCODING, true);
    }

    public static void parseParameters(ParameterHolder params) {
        historyLength = params.getIntegerParam(PARAM_HISTORY_LENGTH);
        encodeLastAction = params.getBooleanParam(PARAM_ENCODE_LAST_ACTION);
        jointEncoding = params.getBooleanParam(PARAM_JOINT_ENCODING);
    }
}
