/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package features.lsh;

import aij.parameters.LSHParameters;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.Random;
import nips11.agents.models.FeatureList;
import rlVizLib.general.ParameterHolder;

/** An (optimized) class that maps a given bit vector to a smaller feature set
 *   using Locally Sensitive Hashing (LSH).
 *
 * @author Marc G. Bellemare <mgbellemare@ualberta.ca>
 */
public class LSHMap {
    protected int numRandomVectors; // l
    protected int randomVectorSize; // k
    protected int hashTableSize; // M
    protected int randomSeed; // for generating the same results everytime

    protected int[][] randomVectors; // l x k array
    protected int[][] hashFunctions; // l x k indices

    protected FeatureList features;

    public LSHMap(ParameterHolder params) {
        LSHParameters.parseParameters(params);
        numRandomVectors = LSHParameters.numRandomVectors;
        randomVectorSize = LSHParameters.randomVectorSize;
        hashTableSize = LSHParameters.hashTableSize;
        randomSeed = LSHParameters.randomSeed;

        features = new FeatureList();
        // One active feature per random vector
        for (int i = 0; i < numRandomVectors; i++)
            features.addFeature(-1, 1.0);
    }

    public static void addParameters(ParameterHolder params) {
        LSHParameters.addParameters(params);
    }

    public int numRandomVectors() {
        return numRandomVectors;
    }

    public int numFeatures() {
        return numRandomVectors * hashTableSize;
    }

    public int numActiveFeatures() {
        return numRandomVectors;
    }
    
    /** Create the random vectors and their hash functions using randomSeed
     *
     */
    public void initLSHStructures(int inputVectorSize) {
        randomVectors = new int[numRandomVectors][randomVectorSize];
        hashFunctions = new int[numRandomVectors][randomVectorSize];

        Random rnd = new Random(randomSeed);

        if (randomVectorSize > inputVectorSize)
            throw new IllegalArgumentException("Random vector larger than screen vector.");

        // For each random vector...
        for (int v = 0; v < numRandomVectors; v++) {
            // Sample k bits, without replacement
            for (int b = 0; b < randomVectorSize; b++) {
                int bitCoordinate = rnd.nextInt(inputVectorSize);

                // Ensure non-duplicates
                boolean bitExists = false;
                for (int i = 0; i < b; i++)
                    if (randomVectors[v][b] == bitCoordinate) {
                        bitExists = true;
                        break;
                    }

                // Re-sample this bit
                if (bitExists) {
                    b--;
                    continue;
                }
                else {
                    randomVectors[v][b] = bitCoordinate;
                    // Also choose the corresponding random hash factor
                    hashFunctions[v][b] = rnd.nextInt(hashTableSize);
                }
            }

            // Sort the random vector coordinates
            Arrays.sort(randomVectors[v]);
        }
    }

    /** Maps the screen bit vector to a much smaller bit set
     *   Go through each random vector in turn; first project the screen
     *   vector onto it (using colorBits); then hash it further down; add
     *   the resulting indices in the second hash to our feature vector, offset
     *   by hashTableSize x randomVectorIndex
     *
     * @param features
     */
    public FeatureList map(boolean[] bitVector, int[] bitList, int bitListSize) {
        ListIterator<FeatureList.Pair> it = features.active.listIterator();

        long maxHashCode = Long.MAX_VALUE - 2 * hashTableSize;

        int offset = 0;
        for (int v = 0; v < numRandomVectors; v++) {
            long hashCode = 0;

            for (int b = 0; b < randomVectorSize; b++) {
                int coordinate = randomVectors[v][b];

                // Project the screen vector onto our random vector
                boolean bitProjection = bitVector[coordinate];

                // Hash the vector!
                if (bitProjection) {
                    hashCode += hashFunctions[v][b];
                    // Avoid overflows
                    if (hashCode >= maxHashCode)
                        hashCode %= hashTableSize;
                }
            }

            hashCode %= hashTableSize;

            int featureIndex = (int)(hashCode + offset);

            // Replace the old feature in the list with this one
            FeatureList.Pair p = it.next();
            p.index = featureIndex;

            offset += hashTableSize;
        }

        return features;
    }

}
