/* *****************************************************************************
 * A.L.E (Arcade Learning Environment)
 * Copyright (c) 2009-2012 by Yavar Naddaf, Joel Veness, Marc G. Bellemare
 * Released under GNU General Public License www.gnu.org/licenses/gpl-3.0.txt
 *
 * Based on: Stella  --  "An Atari 2600 VCS Emulator"
 * Copyright (c) 1995-2007 by Bradford W. Mott and the Stella team
 *
 * *****************************************************************************
 *  SingleActionAgent.hpp
 *
 * The implementation of the SingleActionAgent class. With probability epsilon
 *  it takes a random action; otherwise it takes the action specified by the 
 *  configuration under 'agent_action'.
 **************************************************************************** */

#ifndef __SINGLE_ACTION_AGENT_HPP__
#define __SINGLE_ACTION_AGENT_HPP__

#include "Constants.h"
#include "PlayerAgent.hpp"
#include "OSystem.hxx"

class SingleActionAgent : public PlayerAgent {
    public:
        SingleActionAgent(OSystem * _osystem, RomSettings * _settings);
		
	protected:
        /* *********************************************************************
            Returns the best action from the set of possible actions
         ******************************************************************** */
        virtual Action act();

  protected:
      double epsilon;
      Action agent_action;
};

#endif // __SINGLE_ACTION_AGENT_HPP__
 

