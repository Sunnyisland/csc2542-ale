MODULE := src/agents

MODULE_OBJS := \
	src/agents/ALEState.o \
	src/agents/PlayerAgent.o \
	src/agents/SearchAgent.o \
	src/agents/SearchTree.o \
	src/agents/TreeNode.o \
	src/agents/FullSearchTree.o \
	src/agents/UCTSearchTree.o \
	src/agents/UCTTreeNode.o \
	src/agents/RandomAgent.o \
	src/agents/SingleActionAgent.o \

MODULE_DIRS += \
	src/agents

# Include common rules 
include $(srcdir)/common.rules
