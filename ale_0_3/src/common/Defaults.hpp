/* *****************************************************************************
 * A.L.E (Arcade Learning Environment)
 * Copyright (c) 2009-2012 by Yavar Naddaf, Joel Veness, Marc G. Bellemare
 * Released under GNU General Public License www.gnu.org/licenses/gpl-3.0.txt
 *
 * Based on: Stella  --  "An Atari 2600 VCS Emulator"
 * Copyright (c) 1995-2007 by Bradford W. Mott and the Stella team
 *
 * *****************************************************************************
 *  Defaults.hpp 
 *
 *  Defines methods for setting default parameters. 
 *
 **************************************************************************** */

#ifndef __DEFAULTS_HPP__
#define __DEFAULTS_HPP__

#include "Settings.hxx"

/** Sets all of the ALE-specific default settings */
void setDefaultSettings(Settings &settings);

#endif // __DEFAULTS_HPP__
