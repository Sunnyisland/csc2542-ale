/* *****************************************************************************
 * A.L.E (Arcade Learning Environment)
 * Copyright (c) 2009-2012 by Yavar Naddaf, Joel Veness, Marc G. Bellemare
 * Released under GNU General Public License www.gnu.org/licenses/gpl-3.0.txt
 *
 * Based on: Stella  --  "An Atari 2600 VCS Emulator"
 * Copyright (c) 1995-2007 by Bradford W. Mott and the Stella team
 *
 * *****************************************************************************
 *  Defaults.cpp 
 *
 *  Defines methods for setting default parameters. 
 *
 **************************************************************************** */
#include "Defaults.hpp"

void setDefaultSettings(Settings &settings) {
    // General settings
    settings.setString("random_seed", "time");

    // Controller settings
    settings.setString("game_controller", "internal");
    settings.setInt("max_num_episodes", 10);
    settings.setInt("max_num_frames", 0);
    settings.setInt("max_num_frames_per_episode", 0);

    // FIFO controller settings
    settings.setBool("run_length_encoding", true);

    // Agent settings
    settings.setString("player_agent", "search_agent");
    settings.setFloat("discount_factor", 1.0);

    // Search settings
    settings.setString("search_method", "uct");
    settings.setInt("sim_steps_per_node", 5);
    
    // UCT settings
    settings.setInt("uct_monte_carlo_steps", 20);
    settings.setFloat("uct_exploration_constant", 0.5);

    // Environment customization settings
    settings.setBool("record_trajectory", false);
    settings.setBool("restricted_action_set", false);
    settings.setBool("normalize_rewards", true);
}
