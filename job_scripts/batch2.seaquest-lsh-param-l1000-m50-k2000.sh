#!/bin/bash

#PBS -l walltime=72:00:00,mem=4gb,nodes=1:ppn=2
#PBS -j eo
#PBS -e /dev/null

cd $PBS_O_WORKDIR

jobid=$PBS_JOBID

for i in `seq 1 2`; do
	export PBS_JOBID=run${i}.$jobid
	bash job_scripts/seaquest-lsh-param-l1000-m50-k2000.sh &
done

wait
