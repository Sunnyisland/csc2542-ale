#!/bin/bash

#PBS -l walltime=24:00:00,mem=2gb,nodes=1:ppn=1
#PBS -j eo
#PBS -e /dev/null

cd ${PBS_O_WORKDIR}

maxFrames=0

trainScript=trainDirect.sh
evalScript=evalDirect.sh
baseDir=../AtariAgents
configDir=../shared_data/cfgs/jair/single-action-epsilon
gameName=battle_zone
baseConfig=battle_zone-single-action-epsilon
numEpisodes=1000
numEvalEpisodes=0
maxMemory=2
singleAction=16

sourceCfg=${configDir}/${baseConfig}.cfg
uniqueId=${PBS_JOBID}

targetCfg=${PBS_O_WORKDIR}/cfgs/${baseConfig}.$uniqueId.cfg

sed "s/\$UID/${uniqueId}/g" $sourceCfg >$targetCfg

resultsFile=${PBS_O_WORKDIR}/train/${baseConfig}.$uniqueId.train
evalFile=${PBS_O_WORKDIR}/eval/${baseConfig}.$uniqueId.eval

if [ -z "$maxMemory" ]; then
  maxMemory=2
fi

echo $uniqueId

bash $trainScript --mem $maxMemory $targetCfg --action=$singleAction --episodes=$numEpisodes 2>$resultsFile >/dev/null
