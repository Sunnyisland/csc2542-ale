#!/bin/bash

#PBS -l walltime=40:00:00,mem=2gb,nodes=1:ppn=1
#PBS -j eo
#PBS -e /dev/null

if [ -z "$PBS_O_WORKDIR" ]; then
	echo "Don't call this file directly: use qsub."
	exit -1
fi

cd ${PBS_O_WORKDIR}

maxFrames=18000

trainScript=trainDirect.sh
baseDir=../AtariAgents
resumeScriptsDir=resume_scripts
resumePBSTail=pbs/tail-resuming.pbs

#HEADER_END
configDir=../shared_data/cfgs/jair/ram
gameName=jamesbond
baseConfig=18f-jamesbond-ram
numEpisodes=5000
maxMemory=2
maxTime=40

sourceCfg=${configDir}/${baseConfig}.cfg
uniqueId=${PBS_JOBID}

targetCfg=${PBS_O_WORKDIR}/cfgs/${baseConfig}.$uniqueId.cfg

maxTimeForAgent=$(( $maxTime - 1 ))

# Generate a random seed using all the digits found in the job ID
randomSeed=`echo $PBS_JOBID | tr -c -d "[0-9]"`

sed "s/\$UID/${uniqueId}/g" $sourceCfg | sed "s/MAXRUNTIME/$maxTimeForAgent/g" | sed "s/RANDOMSEED/$randomSeed/g" >$targetCfg

errFile=${PBS_O_WORKDIR}/err/${baseConfig}.$uniqueId.err

if [ -z "$maxMemory" ]; then
  maxMemory=2
fi

thisFile=$0

resumeScript=$resumeScriptsDir/${baseConfig}.resume.$uniqueId.sh

# Create the resume job script
(
awk '{print} ($1 == "#HEADER_END") {exit}' $thisFile 
echo configFile=$targetCfg
echo maxMemory=$maxMemory
echo numEpisodes=$numEpisodes
echo baseConfig=$baseConfig
cat $resumePBSTail
) >$resumeScript

chmod 750 $resumeScript

bash $trainScript --mem $maxMemory $targetCfg --episodes=$numEpisodes 2>$errFile >/dev/null

