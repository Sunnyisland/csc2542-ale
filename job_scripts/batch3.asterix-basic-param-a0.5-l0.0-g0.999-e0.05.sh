#!/bin/bash

#PBS -l walltime=30:00:00,mem=6gb,nodes=1:ppn=3
#PBS -j eo
#PBS -e /dev/null

cd $PBS_O_WORKDIR

jobid=$PBS_JOBID

for i in `seq 1 3`; do
	export PBS_JOBID=run${i}.$jobid
	bash job_scripts/asterix-basic-param-a0.5-l0.0-g0.999-e0.05.sh &
done

wait
