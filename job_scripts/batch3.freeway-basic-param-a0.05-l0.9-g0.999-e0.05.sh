#!/bin/bash

#PBS -l walltime=22:00:00,mem=6gb,nodes=1:ppn=3
#PBS -j eo
#PBS -e /dev/null

cd $PBS_O_WORKDIR

jobid=$PBS_JOBID

for i in `seq 1 3`; do
	export PBS_JOBID=run${i}.$jobid
	bash job_scripts/freeway-basic-param-a0.05-l0.9-g0.999-e0.05.sh &
done

wait
