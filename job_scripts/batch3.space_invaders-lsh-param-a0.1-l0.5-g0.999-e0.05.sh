#!/bin/bash

#PBS -l walltime=36:00:00,mem=6gb,nodes=1:ppn=3
#PBS -j eo
#PBS -e /dev/null

cd $PBS_O_WORKDIR

jobid=$PBS_JOBID

for i in `seq 1 3`; do
	export PBS_JOBID=run${i}.$jobid
	bash job_scripts/space_invaders-lsh-param-a0.1-l0.5-g0.999-e0.05.sh &
done

wait
