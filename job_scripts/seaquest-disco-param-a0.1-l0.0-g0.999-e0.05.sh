#!/bin/bash

#PBS -l walltime=49:00:00,mem=2gb,nodes=1:ppn=1
#PBS -j eo
#PBS -e /dev/null

cd ${PBS_O_WORKDIR}

maxFrames=18000

trainScript=trainDirect.sh
evalScript=evalDirect.sh
baseDir=../AtariAgents
configDir=../shared_data/cfgs/jair/learning-params/disco
gameName=seaquest
baseConfig=seaquest-disco-param
numEpisodes=5000
numEvalEpisodes=0
maxMemory=2
alpha=0.1
lambda=0.0
gamma=0.999
epsilon=0.05
fullName=seaquest-disco-param-a0.1-l0.0-g0.999-e0.05

sourceCfg=${configDir}/${baseConfig}.cfg
uniqueId=${PBS_JOBID}

targetCfg=${PBS_O_WORKDIR}/cfgs/$fullName.$uniqueId.cfg

randomSeed=`echo $PBS_JOBID | tr -c -d "[0-9]"`

sed "s/\$UID/${uniqueId}/g" $sourceCfg | sed "s/\$ALPHA/${alpha}/g; s/\$GAMMA/${gamma}/g; s/\$LAMBDA/${lambda}/g; s/\$EPSILON/${epsilon}/g" | sed s/RANDOMSEED/$randomSeed/g >$targetCfg

resultsFile=${PBS_O_WORKDIR}/train/${fullName}.$uniqueId.train

if [ -z "$maxMemory" ]; then
  maxMemory=2
fi

echo $uniqueId
bash $trainScript --mem $maxMemory $targetCfg --episodes=$numEpisodes 2>$resultsFile >/dev/null
