#!/bin/bash

#PBS -l walltime=asterix:00:00,mem=2gb,nodes=1:ppn=1
#PBS -j eo
#PBS -e /dev/null

cd ${PBS_O_WORKDIR}

maxFrames=0

trainScript=trainDirect.sh
evalScript=evalDirect.sh
baseDir=../AtariAgents
configDir=
gameName=30
baseConfig=
numEpisodes=18000
numEvalEpisodes=0
maxMemory=2
